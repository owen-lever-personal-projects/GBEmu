#include "OPCodesCB.h"
#include "Instructions.h"

// 0x
int CB_00(Registers& registers, BUS& bus) {
	// RLC B
	int cycles = CB_RLC(registers, registers.b);
	//;
	return cycles;
}
int CB_01(Registers& registers, BUS& bus) {
	// RLC C
	int cycles = CB_RLC(registers, registers.c);
	//;
	return cycles;
}
int CB_02(Registers& registers, BUS& bus) {
	// RLC D
	int cycles = CB_RLC(registers, registers.d);
	//;
	return cycles;
}
int CB_03(Registers& registers, BUS& bus) {
	// RLC E
	int cycles = CB_RLC(registers, registers.e);
	//;
	return cycles;
}
int CB_04(Registers& registers, BUS& bus) {
	// RLC H
	int cycles = CB_RLC(registers, registers.h);
	//;
	return cycles;
}
int CB_05(Registers& registers, BUS& bus) {
	// RLC L
	int cycles = CB_RLC(registers, registers.l);
	//;
	return cycles;
}
int CB_06(Registers& registers, BUS& bus) {
	// RLC (HL)
	Reg8 hl = bus.read8(registers.hl);
	CB_RLC(registers, hl);
	bus.write8(registers.hl, hl);
	return 4;
}
int CB_07(Registers& registers, BUS& bus) {
	// RLC A
	int cycles = CB_RLC(registers, registers.a);
	//;
	return cycles;
}
int CB_08(Registers& registers, BUS& bus) {
	// RRC B
	int cycles = CB_RRC(registers, registers.b);
	//;
	return cycles;
}
int CB_09(Registers& registers, BUS& bus) {
	// RRC C
	int cycles = CB_RRC(registers, registers.c);
	//;
	return cycles;
}
int CB_0A(Registers& registers, BUS& bus) {
	// RRC D
	int cycles = CB_RRC(registers, registers.d);
	//;
	return cycles;
}
int CB_0B(Registers& registers, BUS& bus) {
	// RRC E
	int cycles = CB_RRC(registers, registers.e);
	//;
	return cycles;
}
int CB_0C(Registers& registers, BUS& bus) {
	// RRC H
	int cycles = CB_RRC(registers, registers.h);
	//;
	return cycles;
}
int CB_0D(Registers& registers, BUS& bus) {
	// RRC L
	int cycles = CB_RRC(registers, registers.l);
	//;
	return cycles;
}
int CB_0E(Registers& registers, BUS& bus) {
	// RRC (HL)
	Reg8 hl = bus.read8(registers.hl);
	CB_RRC(registers, hl);
	bus.write8(registers.hl, hl);
	return 4;
}
int CB_0F(Registers& registers, BUS& bus) {
	// RRC E
	int cycles = CB_RRC(registers, registers.a);
	//;
	return cycles;
}

//1x
int CB_10(Registers& registers, BUS& bus) {
	// RL B
	int cycles = CB_RL(registers, registers.b);
	//;
	return cycles;
}
int CB_11(Registers& registers, BUS& bus) {
	// RL C
	int cycles = CB_RL(registers, registers.c);
	//;
	return cycles;
}
int CB_12(Registers& registers, BUS& bus) {
	// RL D
	int cycles = CB_RL(registers, registers.d);
	//;
	return cycles;
}
int CB_13(Registers& registers, BUS& bus) {
	// RL E
	int cycles = CB_RL(registers, registers.e);
	//;
	return cycles;
}
int CB_14(Registers& registers, BUS& bus) {
	// RL H
	int cycles = CB_RL(registers, registers.h);
	//;
	return cycles;
}
int CB_15(Registers& registers, BUS& bus) {
	// RL L
	int cycles = CB_RL(registers, registers.l);
	//;
	return cycles;
}
int CB_16(Registers& registers, BUS& bus) {
	// RL (HL)
	Reg8 hl = bus.read8(registers.hl);
	CB_RL(registers, hl);
	bus.write8(registers.hl, hl);
	return 4;
}
int CB_17(Registers& registers, BUS& bus) {
	// RL A
	int cycles = CB_RL(registers, registers.a);
	//;
	return cycles;
}
int CB_18(Registers& registers, BUS& bus) {
	// RR B
	int cycles = CB_RR(registers, registers.b);
	//;
	return cycles;
}
int CB_19(Registers& registers, BUS& bus) {
	// RR C
	int cycles = CB_RR(registers, registers.c);
	//;
	return cycles;
}
int CB_1A(Registers& registers, BUS& bus) {
	// RR D
	int cycles = CB_RR(registers, registers.d);
	//;
	return cycles;
}
int CB_1B(Registers& registers, BUS& bus) {
	// RR E
	int cycles = CB_RR(registers, registers.e);
	//;
	return cycles;
}
int CB_1C(Registers& registers, BUS& bus) {
	// RR H
	int cycles = CB_RR(registers, registers.h);
	//;
	return cycles;
}
int CB_1D(Registers& registers, BUS& bus) {
	// RR L
	int cycles = CB_RR(registers, registers.l);
	//;
	return cycles;
}
int CB_1E(Registers& registers, BUS& bus) {
	// RR (HL)
	Reg8 hl = bus.read8(registers.hl);
	CB_RR(registers, hl);
	bus.write8(registers.hl, hl);
	return 4;
}
int CB_1F(Registers& registers, BUS& bus) {
	// RR A
	int cycles = CB_RR(registers, registers.a);
	//;
	return cycles;
}

//2x
int CB_20(Registers& registers, BUS& bus) {
	// SLA B
	int cycles = SLA(registers, registers.b);
	//;
	return cycles;
}
int CB_21(Registers& registers, BUS& bus) {
	// SLA C
	int cycles = SLA(registers, registers.c);
	//;
	return cycles;
}
int CB_22(Registers& registers, BUS& bus) {
	// SLA D
	int cycles = SLA(registers, registers.d);
	//;
	return cycles;
}
int CB_23(Registers& registers, BUS& bus) {
	// SLA E
	int cycles = SLA(registers, registers.e);
	//;
	return cycles;
}
int CB_24(Registers& registers, BUS& bus) {
	// SLA H
	int cycles = SLA(registers, registers.h);
	//;
	return cycles;
}
int CB_25(Registers& registers, BUS& bus) {
	// SLA L
	int cycles = SLA(registers, registers.l);
	//;
	return cycles;
}
int CB_26(Registers& registers, BUS& bus) {
	// SLA (HL)
	Reg8 data = bus.read8(registers.hl);
	SLA(registers, data);
	bus.write8(registers.hl, data);
	return 4;
}
int CB_27(Registers& registers, BUS& bus) {
	// SLA A
	int cycles = SLA(registers, registers.a);
	//;
	return cycles;
}
int CB_28(Registers& registers, BUS& bus) {
	// SRA B
	int cycles = SRA(registers, registers.b);
	//;
	return cycles;
}
int CB_29(Registers& registers, BUS& bus) {
	// SRA C
	int cycles = SRA(registers, registers.c);
	//;
	return cycles;
}
int CB_2A(Registers& registers, BUS& bus) {
	// SRA D
	int cycles = SRA(registers, registers.d);
	//;
	return cycles;
}
int CB_2B(Registers& registers, BUS& bus) {
	// SRA E
	int cycles = SRA(registers, registers.e);
	//;
	return cycles;
}
int CB_2C(Registers& registers, BUS& bus) {
	// SRA H
	int cycles = SRA(registers, registers.h);
	//;
	return cycles;
}
int CB_2D(Registers& registers, BUS& bus) {
	// SRA L
	int cycles = SRA(registers, registers.l);
	//;
	return cycles;
}
int CB_2E(Registers& registers, BUS& bus) {
	// SRA (HL)
	Reg8 data = bus.read8(registers.hl);
	SRA(registers, data);
	bus.write8(registers.hl, data);
	return 4;
}
int CB_2F(Registers& registers, BUS& bus) {
	// SRA A
	int cycles = SRA(registers, registers.a);
	//;
	return cycles;
}

//3x
int CB_30(Registers& registers, BUS& bus) {
	// SWAP B
	int cycles = SWAP(registers, registers.b);
	//;
	return cycles;
}
int CB_31(Registers& registers, BUS& bus) {
	// SWAP C
	int cycles = SWAP(registers, registers.c);
	//;
	return cycles;
}
int CB_32(Registers& registers, BUS& bus) {
	// SWAP D
	int cycles = SWAP(registers, registers.d);
	//;
	return cycles;
}
int CB_33(Registers& registers, BUS& bus) {
	// SWAP E
	int cycles = SWAP(registers, registers.e);
	//;
	return cycles;
}
int CB_34(Registers& registers, BUS& bus) {
	// SWAP H
	int cycles = SWAP(registers, registers.h);
	//;
	return cycles;
}
int CB_35(Registers& registers, BUS& bus) {
	// SWAP L
	int cycles = SWAP(registers, registers.l);
	//;
	return cycles;
}
int CB_36(Registers& registers, BUS& bus) {
	// SWAP (HL)
	Reg8 hl = bus.read8(registers.hl);
	SWAP(registers, hl);
	bus.write8(registers.hl, hl);
	return 4;
}
int CB_37(Registers& registers, BUS& bus) {
	// SWAP A
	int cycles = SWAP(registers, registers.a);
	//;
	return cycles;
}
int CB_38(Registers& registers, BUS& bus) {
	// SRL B
	int cycles = SRL(registers, registers.b);
	//;
	return cycles;
}
int CB_39(Registers& registers, BUS& bus) {
	// SRL C
	int cycles = SRL(registers, registers.c);
	//;
	return cycles;
}
int CB_3A(Registers& registers, BUS& bus) {
	// SRL D
	int cycles = SRL(registers, registers.d);
	//;
	return cycles;
}
int CB_3B(Registers& registers, BUS& bus) {
	// SRL E
	int cycles = SRL(registers, registers.e);
	//;
	return cycles;
}
int CB_3C(Registers& registers, BUS& bus) {
	// SRL H
	int cycles = SRL(registers, registers.h);
	//;
	return cycles;
}
int CB_3D(Registers& registers, BUS& bus) {
	// SRL L
	int cycles = SRL(registers, registers.l);
	//;
	return cycles;
}
int CB_3E(Registers& registers, BUS& bus) {
	// SRL (HL)
	int cycles = SRL_HL(registers, bus);
	//
	return cycles;
}
int CB_3F(Registers& registers, BUS& bus) {
	// SRL A
	int cycles = SRL(registers, registers.a);
	//;
	return cycles;
}

//4x
int CB_40(Registers& registers, BUS& bus) {
	// BIT 0 B
	int cycles = BIT(registers, 0, registers.b);
	//;
	return cycles;
}
int CB_41(Registers& registers, BUS& bus) {
	// BIT 0 C
	int cycles = BIT(registers, 0, registers.c);
	//;
	return cycles;
}
int CB_42(Registers& registers, BUS& bus) {
	// BIT 0 D
	int cycles = BIT(registers, 0, registers.d);
	//;
	return cycles;
}
int CB_43(Registers& registers, BUS& bus) {
	// BIT 0 E
	int cycles = BIT(registers, 0, registers.e);
	//;
	return cycles;
}
int CB_44(Registers& registers, BUS& bus) {
	// BIT 0 H
	int cycles = BIT(registers, 0, registers.h);
	//;
	return cycles;
}
int CB_45(Registers& registers, BUS& bus) {
	// BIT 0 L
	int cycles = BIT(registers, 0, registers.l);
	//;
	return cycles;
}
int CB_46(Registers& registers, BUS& bus) {
	// BIT 0 (HL)
	int cycles = BIT_a16(registers, bus, 0, registers.hl);
	//;
	return cycles;
}
int CB_47(Registers& registers, BUS& bus) {
	// BIT 0 A
	int cycles = BIT(registers, 0, registers.a);
	//;
	return cycles;
}
int CB_48(Registers& registers, BUS& bus) {
	// BIT 1 B
	int cycles = BIT(registers, 1, registers.b);
	//;
	return cycles;
}
int CB_49(Registers& registers, BUS& bus) {
	// BIT 1 C
	int cycles = BIT(registers, 1, registers.c);
	//;
	return cycles;
}
int CB_4A(Registers& registers, BUS& bus) {
	// BIT 1 D
	int cycles = BIT(registers, 1, registers.d);
	//;
	return cycles;
}
int CB_4B(Registers& registers, BUS& bus) {
	// BIT 1 E
	int cycles = BIT(registers, 1, registers.e);
	//;
	return cycles;
}
int CB_4C(Registers& registers, BUS& bus) {
	// BIT 1 H
	int cycles = BIT(registers, 1, registers.h);
	//;
	return cycles;
}
int CB_4D(Registers& registers, BUS& bus) {
	// BIT 1 L
	int cycles = BIT(registers, 1, registers.l);
	//;
	return cycles;
}
int CB_4E(Registers& registers, BUS& bus) {
	// BIT 1 (HL)
	int cycles = BIT_a16(registers, bus, 1, registers.hl);
	//;
	return cycles;
}
int CB_4F(Registers& registers, BUS& bus) {
	// BIT 1 A
	int cycles = BIT(registers, 1, registers.a);
	//;
	return cycles;
}

//5x
int CB_50(Registers& registers, BUS& bus) {
	// BIT 2 B
	int cycles = BIT(registers, 2, registers.b);
	//;
	return cycles;
}
int CB_51(Registers& registers, BUS& bus) {
	// BIT 2 C
	int cycles = BIT(registers, 2, registers.c);
	//;
	return cycles;
}
int CB_52(Registers& registers, BUS& bus) {
	// BIT 2 D
	int cycles = BIT(registers, 2, registers.d);
	//;
	return cycles;
}
int CB_53(Registers& registers, BUS& bus) {
	// BIT 2 E
	int cycles = BIT(registers, 2, registers.e);
	//;
	return cycles;
}
int CB_54(Registers& registers, BUS& bus) {
	// BIT 2 H
	int cycles = BIT(registers, 2, registers.h);
	//;
	return cycles;
}
int CB_55(Registers& registers, BUS& bus) {
	// BIT 2 L
	int cycles = BIT(registers, 2, registers.l);
	//;
	return cycles;
}
int CB_56(Registers& registers, BUS& bus) {
	// BIT 2 (HL)
	int cycles = BIT_a16(registers, bus, 2, registers.hl);
	//;
	return cycles;
}
int CB_57(Registers& registers, BUS& bus) {
	// BIT 2 A
	int cycles = BIT(registers, 2, registers.a);
	//;
	return cycles;
}
int CB_58(Registers& registers, BUS& bus) {
	// BIT 3 B
	int cycles = BIT(registers, 3, registers.b);
	//;
	return cycles;
}
int CB_59(Registers& registers, BUS& bus) {
	// BIT 3 C
	int cycles = BIT(registers, 3, registers.c);
	//;
	return cycles;
}
int CB_5A(Registers& registers, BUS& bus) {
	// BIT 3 D
	int cycles = BIT(registers, 3, registers.d);
	//;
	return cycles;
}
int CB_5B(Registers& registers, BUS& bus) {
	// BIT 3 E
	int cycles = BIT(registers, 3, registers.e);
	//;
	return cycles;
}
int CB_5C(Registers& registers, BUS& bus) {
	// BIT 3 H
	int cycles = BIT(registers, 3, registers.h);
	//;
	return cycles;
}
int CB_5D(Registers& registers, BUS& bus) {
	// BIT 3 L
	int cycles = BIT(registers, 3, registers.l);
	//;
	return cycles;
}
int CB_5E(Registers& registers, BUS& bus) {
	// BIT 3 (HL)
	int cycles = BIT_a16(registers, bus, 3, registers.hl);
	//;
	return cycles;
}
int CB_5F(Registers& registers, BUS& bus) {
	// BIT 3 A
	int cycles = BIT(registers, 3, registers.a);
	//;
	return cycles;
}

//6x
int CB_60(Registers& registers, BUS& bus) {
	// BIT 4 B
	int cycles = BIT(registers, 4, registers.b);
	//;
	return cycles;
}
int CB_61(Registers& registers, BUS& bus) {
	// BIT 4 C
	int cycles = BIT(registers, 4, registers.c);
	//;
	return cycles;
}
int CB_62(Registers& registers, BUS& bus) {
	// BIT 4 D
	int cycles = BIT(registers, 4, registers.d);
	//;
	return cycles;
}
int CB_63(Registers& registers, BUS& bus) {
	// BIT 4 E
	int cycles = BIT(registers, 4, registers.e);
	//;
	return cycles;
}
int CB_64(Registers& registers, BUS& bus) {
	// BIT 4 H
	int cycles = BIT(registers, 4, registers.h);
	//;
	return cycles;
}
int CB_65(Registers& registers, BUS& bus) {
	// BIT 4 L
	int cycles = BIT(registers, 4, registers.l);
	//;
	return cycles;
}
int CB_66(Registers& registers, BUS& bus) {
	// BIT 4 (HL)
	int cycles = BIT_a16(registers, bus, 4, registers.hl);
	//;
	return cycles;
}
int CB_67(Registers& registers, BUS& bus) {
	// BIT 4 A
	int cycles = BIT(registers, 4, registers.a);
	//;
	return cycles;
}
int CB_68(Registers& registers, BUS& bus) {
	// BIT 5 B
	int cycles = BIT(registers, 5, registers.b);
	//;
	return cycles;
}
int CB_69(Registers& registers, BUS& bus) {
	// BIT 5 C
	int cycles = BIT(registers, 5, registers.c);
	//;
	return cycles;
}
int CB_6A(Registers& registers, BUS& bus) {
	// BIT 5 D
	int cycles = BIT(registers, 5, registers.d);
	//;
	return cycles;
}
int CB_6B(Registers& registers, BUS& bus) {
	// BIT 5 E
	int cycles = BIT(registers, 5, registers.e);
	//;
	return cycles;
}
int CB_6C(Registers& registers, BUS& bus) {
	// BIT 5 H
	int cycles = BIT(registers, 5, registers.h);
	//;
	return cycles;
}
int CB_6D(Registers& registers, BUS& bus) {
	// BIT 5 L
	int cycles = BIT(registers, 5, registers.l);
	//;
	return cycles;
}
int CB_6E(Registers& registers, BUS& bus) {
	// BIT 5 (HL)
	int cycles = BIT_a16(registers, bus, 5, registers.hl);
	//;
	return cycles;
}
int CB_6F(Registers& registers, BUS& bus) {
	// BIT 5 A
	int cycles = BIT(registers, 5, registers.a);
	//;
	return cycles;
}

//7x
int CB_70(Registers& registers, BUS& bus) {
	// BIT 6 B
	int cycles = BIT(registers, 6, registers.b);
	//;
	return cycles;
}
int CB_71(Registers& registers, BUS& bus) {
	// BIT 6 C
	int cycles = BIT(registers, 6, registers.c);
	//;
	return cycles;
}
int CB_72(Registers& registers, BUS& bus) {
	// BIT 6 D
	int cycles = BIT(registers, 6, registers.d);
	//;
	return cycles;
}
int CB_73(Registers& registers, BUS& bus) {
	// BIT 6 E
	int cycles = BIT(registers, 6, registers.e);
	//;
	return cycles;
}
int CB_74(Registers& registers, BUS& bus) {
	// BIT 6 H
	int cycles = BIT(registers, 6, registers.h);
	//;
	return cycles;
}
int CB_75(Registers& registers, BUS& bus) {
	// BIT 6 L
	int cycles = BIT(registers, 6, registers.l);
	//;
	return cycles;
}
int CB_76(Registers& registers, BUS& bus) {
	// BIT 6 (HL)
	int cycles = BIT_a16(registers, bus, 6, registers.hl);
	//;
	return cycles;
}
int CB_77(Registers& registers, BUS& bus) {
	// BIT 6 A
	int cycles = BIT(registers, 6, registers.a);
	//;
	return cycles;
}
int CB_78(Registers& registers, BUS& bus) {
	// BIT 7 B
	int cycles = BIT(registers, 7, registers.b);
	//;
	return cycles;
}
int CB_79(Registers& registers, BUS& bus) {
	// BIT 7 C
	int cycles = BIT(registers, 7, registers.c);
	//;
	return cycles;
}
int CB_7A(Registers& registers, BUS& bus) {
	// BIT 7 D
	int cycles = BIT(registers, 7, registers.d);
	//;
	return cycles;
}
int CB_7B(Registers& registers, BUS& bus) {
	// BIT 7 E
	int cycles = BIT(registers, 7, registers.e);
	//;
	return cycles;
}
int CB_7C(Registers& registers, BUS& bus) {
	// BIT 7 H
	int cycles = BIT(registers, 7, registers.h);
	//;
	return cycles;
}
int CB_7D(Registers& registers, BUS& bus) {
	// BIT 7 L
	int cycles = BIT(registers, 7, registers.l);
	//;
	return cycles;
}
int CB_7E(Registers& registers, BUS& bus) {
	// BIT 7 (HL)
	int cycles = BIT_a16(registers, bus, 7, registers.hl);
	//;
	return cycles;
}
int CB_7F(Registers& registers, BUS& bus) {
	// BIT 7 A
	int cycles = BIT(registers, 7, registers.a);
	//;
	return cycles;
}

//8x
int CB_80(Registers& registers, BUS& bus) {
	// RES 0 B
	int cycles = RES(registers, 0, registers.b);
	//;
	return cycles;
}
int CB_81(Registers& registers, BUS& bus) {
	// RES 0 C
	int cycles = RES(registers, 0, registers.c);
	//;
	return cycles;
}
int CB_82(Registers& registers, BUS& bus) {
	// RES 0 D
	int cycles = RES(registers, 0, registers.d);
	//;
	return cycles;
}
int CB_83(Registers& registers, BUS& bus) {
	// RES 0 E
	int cycles = RES(registers, 0, registers.e);
	//;
	return cycles;
}
int CB_84(Registers& registers, BUS& bus) {
	// RES 0 H
	int cycles = RES(registers, 0, registers.h);
	//;
	return cycles;
}
int CB_85(Registers& registers, BUS& bus) {
	// RES 0 L
	int cycles = RES(registers, 0, registers.l);
	//;
	return cycles;
}
int CB_86(Registers& registers, BUS& bus) {
	// RES 0 (HL)
	int cycles = RES_a16(registers, bus, 0, registers.hl);
	//;
	return cycles;
}
int CB_87(Registers& registers, BUS& bus) {
	// RES 0 A
	int cycles = RES(registers, 0, registers.a);
	//;
	return cycles;
}
int CB_88(Registers& registers, BUS& bus) {
	// RES 1 B
	int cycles = RES(registers, 1, registers.b);
	//;
	return cycles;
}
int CB_89(Registers& registers, BUS& bus) {
	// RES 1 C
	int cycles = RES(registers, 1, registers.c);
	//;
	return cycles;
}
int CB_8A(Registers& registers, BUS& bus) {
	// RES 1 D
	int cycles = RES(registers, 1, registers.d);
	//;
	return cycles;
}
int CB_8B(Registers& registers, BUS& bus) {
	// RES 1 E
	int cycles = RES(registers, 1, registers.e);
	//;
	return cycles;
}
int CB_8C(Registers& registers, BUS& bus) {
	// RES 1 H
	int cycles = RES(registers, 1, registers.h);
	//;
	return cycles;
}
int CB_8D(Registers& registers, BUS& bus) {
	// RES 1 L
	int cycles = RES(registers, 1, registers.l);
	//;
	return cycles;
}
int CB_8E(Registers& registers, BUS& bus) {
	// RES 1 (HL)
	int cycles = RES_a16(registers, bus, 1, registers.hl);
	//;
	return cycles;
}
int CB_8F(Registers& registers, BUS& bus) {
	// RES 1 A
	int cycles = RES(registers, 1, registers.a);
	//;
	return cycles;
}

//9x
int CB_90(Registers& registers, BUS& bus) {
	// RES 2 B
	int cycles = RES(registers, 2, registers.b);
	//;
	return cycles;
}
int CB_91(Registers& registers, BUS& bus) {
	// RES 2 C
	int cycles = RES(registers, 2, registers.c);
	//;
	return cycles;
}
int CB_92(Registers& registers, BUS& bus) {
	// RES 2 D
	int cycles = RES(registers, 2, registers.d);
	//;
	return cycles;
}
int CB_93(Registers& registers, BUS& bus) {
	// RES 2 E
	int cycles = RES(registers, 2, registers.e);
	//;
	return cycles;
}
int CB_94(Registers& registers, BUS& bus) {
	// RES 2 H
	int cycles = RES(registers, 2, registers.h);
	//;
	return cycles;
}
int CB_95(Registers& registers, BUS& bus) {
	// RES 2 L
	int cycles = RES(registers, 2, registers.l);
	//;
	return cycles;
}
int CB_96(Registers& registers, BUS& bus) {
	// RES 2 (HL)
	int cycles = RES_a16(registers, bus, 2, registers.hl);
	//;
	return cycles;
}
int CB_97(Registers& registers, BUS& bus) {
	// RES 2 A
	int cycles = RES(registers, 2, registers.a);
	//;
	return cycles;
}
int CB_98(Registers& registers, BUS& bus) {
	// RES 3 B
	int cycles = RES(registers, 3, registers.b);
	//;
	return cycles;
}
int CB_99(Registers& registers, BUS& bus) {
	// RES 3 C
	int cycles = RES(registers, 3, registers.c);
	//;
	return cycles;
}
int CB_9A(Registers& registers, BUS& bus) {
	// RES 3 D
	int cycles = RES(registers, 3, registers.d);
	//;
	return cycles;
}
int CB_9B(Registers& registers, BUS& bus) {
	// RES 3 E
	int cycles = RES(registers, 3, registers.e);
	//;
	return cycles;
}
int CB_9C(Registers& registers, BUS& bus) {
	// RES 3 H
	int cycles = RES(registers, 3, registers.h);
	//;
	return cycles;
}
int CB_9D(Registers& registers, BUS& bus) {
	// RES 3 L
	int cycles = RES(registers, 3, registers.l);
	//;
	return cycles;
}
int CB_9E(Registers& registers, BUS& bus) {
	// RES 3 (HL)
	int cycles = RES_a16(registers, bus, 3, registers.hl);
	//;
	return cycles;
}
int CB_9F(Registers& registers, BUS& bus) {
	// RES 3 A
	int cycles = RES(registers, 3, registers.a);
	//;
	return cycles;
}

//Ax
int CB_A0(Registers& registers, BUS& bus) {
	// RES 4 B
	int cycles = RES(registers, 4, registers.b);
	//;
	return cycles;
}
int CB_A1(Registers& registers, BUS& bus) {
	// RES 4 C
	int cycles = RES(registers, 4, registers.c);
	//;
	return cycles;
}
int CB_A2(Registers& registers, BUS& bus) {
	// RES 4 D
	int cycles = RES(registers, 4, registers.d);
	//;
	return cycles;
}
int CB_A3(Registers& registers, BUS& bus) {
	// RES 4 E
	int cycles = RES(registers, 4, registers.e);
	//;
	return cycles;
}
int CB_A4(Registers& registers, BUS& bus) {
	// RES 4 H
	int cycles = RES(registers, 4, registers.h);
	//;
	return cycles;
}
int CB_A5(Registers& registers, BUS& bus) {
	// RES 4 L
	int cycles = RES(registers, 4, registers.l);
	//;
	return cycles;
}
int CB_A6(Registers& registers, BUS& bus) {
	// RES 4 (HL)
	int cycles = RES_a16(registers, bus, 4, registers.hl);
	//;
	return cycles;
}
int CB_A7(Registers& registers, BUS& bus) {
	// RES 4 A
	int cycles = RES(registers, 4, registers.a);
	//;
	return cycles;
}
int CB_A8(Registers& registers, BUS& bus) {
	// RES 5 B
	int cycles = RES(registers, 5, registers.b);
	//;
	return cycles;
}
int CB_A9(Registers& registers, BUS& bus) {
	// RES 5 C
	int cycles = RES(registers, 5, registers.c);
	//;
	return cycles;
}
int CB_AA(Registers& registers, BUS& bus) {
	// RES 5 D
	int cycles = RES(registers, 5, registers.d);
	//;
	return cycles;
}
int CB_AB(Registers& registers, BUS& bus) {
	// RES 5 E
	int cycles = RES(registers, 5, registers.e);
	//;
	return cycles;
}
int CB_AC(Registers& registers, BUS& bus) {
	// RES 5 H
	int cycles = RES(registers, 5, registers.h);
	//;
	return cycles;
}
int CB_AD(Registers& registers, BUS& bus) {
	// RES 5 L
	int cycles = RES(registers, 5, registers.l);
	//;
	return cycles;
}
int CB_AE(Registers& registers, BUS& bus) {
	// RES 5 (HL)
	int cycles = RES_a16(registers, bus, 5, registers.hl);
	//;
	return cycles;
}
int CB_AF(Registers& registers, BUS& bus) {
	// RES 5 A
	int cycles = RES(registers, 5, registers.a);
	//;
	return cycles;
}

//Bx
int CB_B0(Registers& registers, BUS& bus) {
	// RES 6 B
	int cycles = RES(registers, 6, registers.b);
	//;
	return cycles;
}
int CB_B1(Registers& registers, BUS& bus) {
	// RES 6 C
	int cycles = RES(registers, 6, registers.c);
	//;
	return cycles;
}
int CB_B2(Registers& registers, BUS& bus) {
	// RES 6 D
	int cycles = RES(registers, 6, registers.d);
	//;
	return cycles;
}
int CB_B3(Registers& registers, BUS& bus) {
	// RES 6 E
	int cycles = RES(registers, 6, registers.e);
	//;
	return cycles;
}
int CB_B4(Registers& registers, BUS& bus) {
	// RES 6 H
	int cycles = RES(registers, 6, registers.h);
	//;
	return cycles;
}
int CB_B5(Registers& registers, BUS& bus) {
	// RES 6 L
	int cycles = RES(registers, 6, registers.l);
	//;
	return cycles;
}
int CB_B6(Registers& registers, BUS& bus) {
	// RES 6 (HL)
	int cycles = RES_a16(registers, bus, 6, registers.hl);
	//;
	return cycles;
}
int CB_B7(Registers& registers, BUS& bus) {
	// RES 6 A
	int cycles = RES(registers, 6, registers.a);
	//;
	return cycles;
}
int CB_B8(Registers& registers, BUS& bus) {
	// RES 7 B
	int cycles = RES(registers, 7, registers.b);
	//;
	return cycles;
}
int CB_B9(Registers& registers, BUS& bus) {
	// RES 7 C
	int cycles = RES(registers, 7, registers.c);
	//;
	return cycles;
}
int CB_BA(Registers& registers, BUS& bus) {
	// RES 7 D
	int cycles = RES(registers, 7, registers.d);
	//;
	return cycles;
}
int CB_BB(Registers& registers, BUS& bus) {
	// RES 7 E
	int cycles = RES(registers, 7, registers.e);
	//;
	return cycles;
}
int CB_BC(Registers& registers, BUS& bus) {
	// RES 7 H
	int cycles = RES(registers, 7, registers.h);
	//;
	return cycles;
}
int CB_BD(Registers& registers, BUS& bus) {
	// RES 7 L
	int cycles = RES(registers, 7, registers.l);
	//;
	return cycles;
}
int CB_BE(Registers& registers, BUS& bus) {
	// RES 7 (HL)
	int cycles = RES_a16(registers, bus, 7, registers.hl);
	//;
	return cycles;
}
int CB_BF(Registers& registers, BUS& bus) {
	// RES 7 A
	int cycles = RES(registers, 7, registers.a);
	//;
	return cycles;
}

//Cx
int CB_C0(Registers& registers, BUS& bus) {
	// SET 0 B
	int cycles = SET(registers, 0, registers.b);
	//;
	return cycles;
}
int CB_C1(Registers& registers, BUS& bus) {
	// SET 0 C
	int cycles = SET(registers, 0, registers.c);
	//;
	return cycles;
}
int CB_C2(Registers& registers, BUS& bus) {
	// SET 0 D
	int cycles = SET(registers, 0, registers.d);
	//;
	return cycles;
}
int CB_C3(Registers& registers, BUS& bus) {
	// SET 0 E
	int cycles = SET(registers, 0, registers.e);
	//;
	return cycles;
}
int CB_C4(Registers& registers, BUS& bus) {
	// SET 0 H
	int cycles = SET(registers, 0, registers.h);
	//;
	return cycles;
}
int CB_C5(Registers& registers, BUS& bus) {
	// SET 0 L
	int cycles = SET(registers, 0, registers.l);
	//;
	return cycles;
}
int CB_C6(Registers& registers, BUS& bus) {
	// SET 0 (HL)
	int cycles = SET_a16(registers, bus, 0, registers.hl);
	//;
	return cycles;
}
int CB_C7(Registers& registers, BUS& bus) {
	// SET 0 A
	int cycles = SET(registers, 0, registers.a);
	//;
	return cycles;
}
int CB_C8(Registers& registers, BUS& bus) {
	// SET 1 B
	int cycles = SET(registers, 1, registers.b);
	//;
	return cycles;
}
int CB_C9(Registers& registers, BUS& bus) {
	// SET 1 C
	int cycles = SET(registers, 1, registers.c);
	//;
	return cycles;
}
int CB_CA(Registers& registers, BUS& bus) {
	// SET 1 D
	int cycles = SET(registers, 1, registers.d);
	//;
	return cycles;
}
int CB_CB(Registers& registers, BUS& bus) {
	// SET 1 E
	int cycles = SET(registers, 1, registers.e);
	//;
	return cycles;
}
int CB_CC(Registers& registers, BUS& bus) {
	// SET 1 H
	int cycles = SET(registers, 1, registers.h);
	//;
	return cycles;
}
int CB_CD(Registers& registers, BUS& bus) {
	// SET 1 L
	int cycles = SET(registers, 1, registers.l);
	//;
	return cycles;
}
int CB_CE(Registers& registers, BUS& bus) {
	// SET 1 (HL)
	int cycles = SET_a16(registers, bus, 1, registers.hl);
	//;
	return cycles;
}
int CB_CF(Registers& registers, BUS& bus) {
	// SET 1 A
	int cycles = SET(registers, 1, registers.a);
	//;
	return cycles;
}

//Dx
int CB_D0(Registers& registers, BUS& bus) {
	// SET 2 B
	int cycles = SET(registers, 2, registers.b);
	//;
	return cycles;
}
int CB_D1(Registers& registers, BUS& bus) {
	// SET 2 C
	int cycles = SET(registers, 2, registers.c);
	//;
	return cycles;
}
int CB_D2(Registers& registers, BUS& bus) {
	// SET 2 D
	int cycles = SET(registers, 2, registers.d);
	//;
	return cycles;
}
int CB_D3(Registers& registers, BUS& bus) {
	// SET 2 E
	int cycles = SET(registers, 2, registers.e);
	//;
	return cycles;
}
int CB_D4(Registers& registers, BUS& bus) {
	// SET 2 H
	int cycles = SET(registers, 2, registers.h);
	//;
	return cycles;
}
int CB_D5(Registers& registers, BUS& bus) {
	// SET 2 L
	int cycles = SET(registers, 2, registers.l);
	//;
	return cycles;
}
int CB_D6(Registers& registers, BUS& bus) {
	// SET 2 (HL)
	int cycles = SET_a16(registers, bus, 2, registers.hl);
	//;
	return cycles;
}
int CB_D7(Registers& registers, BUS& bus) {
	// SET 2 A
	int cycles = SET(registers, 2, registers.a);
	//;
	return cycles;
}
int CB_D8(Registers& registers, BUS& bus) {
	// SET 3 B
	int cycles = SET(registers, 3, registers.b);
	//;
	return cycles;
}
int CB_D9(Registers& registers, BUS& bus) {
	// SET 3 C
	int cycles = SET(registers, 3, registers.c);
	//;
	return cycles;
}
int CB_DA(Registers& registers, BUS& bus) {
	// SET 3 D
	int cycles = SET(registers, 3, registers.d);
	//;
	return cycles;
}
int CB_DB(Registers& registers, BUS& bus) {
	// SET 3 E
	int cycles = SET(registers, 3, registers.e);
	//;
	return cycles;
}
int CB_DC(Registers& registers, BUS& bus) {
	// SET 3 H
	int cycles = SET(registers, 3, registers.h);
	//;
	return cycles;
}
int CB_DD(Registers& registers, BUS& bus) {
	// SET 3 L
	int cycles = SET(registers, 3, registers.l);
	//;
	return cycles;
}
int CB_DE(Registers& registers, BUS& bus) {
	// SET 3 (HL)
	int cycles = SET_a16(registers, bus, 3, registers.hl);
	//;
	return cycles;
}
int CB_DF(Registers& registers, BUS& bus) {
	// SET 3 A
	int cycles = SET(registers, 3, registers.a);
	//;
	return cycles;
}

//Ex
int CB_E0(Registers& registers, BUS& bus) {
	// SET 4 B
	int cycles = SET(registers, 4, registers.b);
	//;
	return cycles;
}
int CB_E1(Registers& registers, BUS& bus) {
	// SET 4 C
	int cycles = SET(registers, 4, registers.c);
	//;
	return cycles;
}
int CB_E2(Registers& registers, BUS& bus) {
	// SET 4 D
	int cycles = SET(registers, 4, registers.d);
	//;
	return cycles;
}
int CB_E3(Registers& registers, BUS& bus) {
	// SET 4 E
	int cycles = SET(registers, 4, registers.e);
	//;
	return cycles;
}
int CB_E4(Registers& registers, BUS& bus) {
	// SET 4 H
	int cycles = SET(registers, 4, registers.h);
	//;
	return cycles;
}
int CB_E5(Registers& registers, BUS& bus) {
	// SET 4 L
	int cycles = SET(registers, 4, registers.l);
	//;
	return cycles;
}
int CB_E6(Registers& registers, BUS& bus) {
	// SET 4 (HL)
	int cycles = SET_a16(registers, bus, 4, registers.hl);
	//;
	return cycles;
}
int CB_E7(Registers& registers, BUS& bus) {
	// SET 4 A
	int cycles = SET(registers, 4, registers.a);
	//;
	return cycles;
}
int CB_E8(Registers& registers, BUS& bus) {
	// SET 5 B
	int cycles = SET(registers, 5, registers.b);
	//;
	return cycles;
}
int CB_E9(Registers& registers, BUS& bus) {
	// SET 5 C
	int cycles = SET(registers, 5, registers.c);
	//;
	return cycles;
}
int CB_EA(Registers& registers, BUS& bus) {
	// SET 5 D
	int cycles = SET(registers, 5, registers.d);
	//;
	return cycles;
}
int CB_EB(Registers& registers, BUS& bus) {
	// SET 5 E
	int cycles = SET(registers, 5, registers.e);
	//;
	return cycles;
}
int CB_EC(Registers& registers, BUS& bus) {
	// SET 5 H
	int cycles = SET(registers, 5, registers.h);
	//;
	return cycles;
}
int CB_ED(Registers& registers, BUS& bus) {
	// SET 5 L
	int cycles = SET(registers, 5, registers.l);
	//;
	return cycles;
}
int CB_EE(Registers& registers, BUS& bus) {
	// SET 5 (HL)
	int cycles = SET_a16(registers, bus, 5, registers.hl);
	//;
	return cycles;
}
int CB_EF(Registers& registers, BUS& bus) {
	// SET 5 A
	int cycles = SET(registers, 5, registers.a);
	//;
	return cycles;
}

//Fx
int CB_F0(Registers& registers, BUS& bus) {
	// SET 6 B
	int cycles = SET(registers, 6, registers.b);
	//;
	return cycles;
}
int CB_F1(Registers& registers, BUS& bus) {
	// SET 6 C
	int cycles = SET(registers, 6, registers.c);
	//;
	return cycles;
}
int CB_F2(Registers& registers, BUS& bus) {
	// SET 6 D
	int cycles = SET(registers, 6, registers.d);
	//;
	return cycles;
}
int CB_F3(Registers& registers, BUS& bus) {
	// SET 6 E
	int cycles = SET(registers, 6, registers.e);
	//;
	return cycles;
}
int CB_F4(Registers& registers, BUS& bus) {
	// SET 6 H
	int cycles = SET(registers, 6, registers.h);
	//;
	return cycles;
}
int CB_F5(Registers& registers, BUS& bus) {
	// SET 6 L
	int cycles = SET(registers, 6, registers.l);
	//;
	return cycles;
}
int CB_F6(Registers& registers, BUS& bus) {
	// SET 6 (HL)
	int cycles = SET_a16(registers, bus, 6, registers.hl);
	//;
	return cycles;
}
int CB_F7(Registers& registers, BUS& bus) {
	// SET 6 A
	int cycles = SET(registers, 6, registers.a);
	//;
	return cycles;
}
int CB_F8(Registers& registers, BUS& bus) {
	// SET 7 B
	int cycles = SET(registers, 7, registers.b);
	//;
	return cycles;
}
int CB_F9(Registers& registers, BUS& bus) {
	// SET 7 C
	int cycles = SET(registers, 7, registers.c);
	//;
	return cycles;
}
int CB_FA(Registers& registers, BUS& bus) {
	// SET 7 D
	int cycles = SET(registers, 7, registers.d);
	//;
	return cycles;
}
int CB_FB(Registers& registers, BUS& bus) {
	// SET 7 E
	int cycles = SET(registers, 7, registers.e);
	//;
	return cycles;
}
int CB_FC(Registers& registers, BUS& bus) {
	// SET 7 H
	int cycles = SET(registers, 7, registers.h);
	//;
	return cycles;
}
int CB_FD(Registers& registers, BUS& bus) {
	// SET 7 L
	int cycles = SET(registers, 7, registers.l);
	//;
	return cycles;
}
int CB_FE(Registers& registers, BUS& bus) {
	// SET 7 (HL)
	int cycles = SET_a16(registers, bus, 7, registers.hl);
	//;
	return cycles;
}
int CB_FF(Registers& registers, BUS& bus) {
	// SET 7 A
	int cycles = SET(registers, 7, registers.a);
	//;
	return cycles;
}

void initOpcodesCB(CB_OPCode* opcodes) {
	opcodes[0x00] = CB_00;
	opcodes[0x01] = CB_01;
	opcodes[0x02] = CB_02;
	opcodes[0x03] = CB_03;
	opcodes[0x04] = CB_04;
	opcodes[0x05] = CB_05;
	opcodes[0x06] = CB_06;
	opcodes[0x07] = CB_07;
	opcodes[0x08] = CB_08;
	opcodes[0x09] = CB_09;
	opcodes[0x0A] = CB_0A;
	opcodes[0x0B] = CB_0B;
	opcodes[0x0C] = CB_0C;
	opcodes[0x0D] = CB_0D;
	opcodes[0x0E] = CB_0E;
	opcodes[0x0F] = CB_0F;

	opcodes[0x10] = CB_10;
	opcodes[0x11] = CB_11;
	opcodes[0x12] = CB_12;
	opcodes[0x13] = CB_13;
	opcodes[0x14] = CB_14;
	opcodes[0x15] = CB_15;
	opcodes[0x16] = CB_16;
	opcodes[0x17] = CB_17;
	opcodes[0x18] = CB_18;
	opcodes[0x19] = CB_19;
	opcodes[0x1A] = CB_1A;
	opcodes[0x1B] = CB_1B;
	opcodes[0x1C] = CB_1C;
	opcodes[0x1D] = CB_1D;
	opcodes[0x1E] = CB_1E;
	opcodes[0x1F] = CB_1F;

	opcodes[0x20] = CB_20;
	opcodes[0x21] = CB_21;
	opcodes[0x22] = CB_22;
	opcodes[0x23] = CB_23;
	opcodes[0x24] = CB_24;
	opcodes[0x25] = CB_25;
	opcodes[0x26] = CB_26;
	opcodes[0x27] = CB_27;
	opcodes[0x28] = CB_28;
	opcodes[0x29] = CB_29;
	opcodes[0x2A] = CB_2A;
	opcodes[0x2B] = CB_2B;
	opcodes[0x2C] = CB_2C;
	opcodes[0x2D] = CB_2D;
	opcodes[0x2E] = CB_2E;
	opcodes[0x2F] = CB_2F;

	opcodes[0x30] = CB_30;
	opcodes[0x31] = CB_31;
	opcodes[0x32] = CB_32;
	opcodes[0x33] = CB_33;
	opcodes[0x34] = CB_34;
	opcodes[0x35] = CB_35;
	opcodes[0x36] = CB_36;
	opcodes[0x37] = CB_37;
	opcodes[0x38] = CB_38;
	opcodes[0x39] = CB_39;
	opcodes[0x3A] = CB_3A;
	opcodes[0x3B] = CB_3B;
	opcodes[0x3C] = CB_3C;
	opcodes[0x3D] = CB_3D;
	opcodes[0x3E] = CB_3E;
	opcodes[0x3F] = CB_3F;

	opcodes[0x40] = CB_40;
	opcodes[0x41] = CB_41;
	opcodes[0x42] = CB_42;
	opcodes[0x43] = CB_43;
	opcodes[0x44] = CB_44;
	opcodes[0x45] = CB_45;
	opcodes[0x46] = CB_46;
	opcodes[0x47] = CB_47;
	opcodes[0x48] = CB_48;
	opcodes[0x49] = CB_49;
	opcodes[0x4A] = CB_4A;
	opcodes[0x4B] = CB_4B;
	opcodes[0x4C] = CB_4C;
	opcodes[0x4D] = CB_4D;
	opcodes[0x4E] = CB_4E;
	opcodes[0x4F] = CB_4F;

	opcodes[0x50] = CB_50;
	opcodes[0x51] = CB_51;
	opcodes[0x52] = CB_52;
	opcodes[0x53] = CB_53;
	opcodes[0x54] = CB_54;
	opcodes[0x55] = CB_55;
	opcodes[0x56] = CB_56;
	opcodes[0x57] = CB_57;
	opcodes[0x58] = CB_58;
	opcodes[0x59] = CB_59;
	opcodes[0x5A] = CB_5A;
	opcodes[0x5B] = CB_5B;
	opcodes[0x5C] = CB_5C;
	opcodes[0x5D] = CB_5D;
	opcodes[0x5E] = CB_5E;
	opcodes[0x5F] = CB_5F;

	opcodes[0x60] = CB_60;
	opcodes[0x61] = CB_61;
	opcodes[0x62] = CB_62;
	opcodes[0x63] = CB_63;
	opcodes[0x64] = CB_64;
	opcodes[0x65] = CB_65;
	opcodes[0x66] = CB_66;
	opcodes[0x67] = CB_67;
	opcodes[0x68] = CB_68;
	opcodes[0x69] = CB_69;
	opcodes[0x6A] = CB_6A;
	opcodes[0x6B] = CB_6B;
	opcodes[0x6C] = CB_6C;
	opcodes[0x6D] = CB_6D;
	opcodes[0x6E] = CB_6E;
	opcodes[0x6F] = CB_6F;

	opcodes[0x70] = CB_70;
	opcodes[0x71] = CB_71;
	opcodes[0x72] = CB_72;
	opcodes[0x73] = CB_73;
	opcodes[0x74] = CB_74;
	opcodes[0x75] = CB_75;
	opcodes[0x76] = CB_76;
	opcodes[0x77] = CB_77;
	opcodes[0x78] = CB_78;
	opcodes[0x79] = CB_79;
	opcodes[0x7A] = CB_7A;
	opcodes[0x7B] = CB_7B;
	opcodes[0x7C] = CB_7C;
	opcodes[0x7D] = CB_7D;
	opcodes[0x7E] = CB_7E;
	opcodes[0x7F] = CB_7F;

	opcodes[0x80] = CB_80;
	opcodes[0x81] = CB_81;
	opcodes[0x82] = CB_82;
	opcodes[0x83] = CB_83;
	opcodes[0x84] = CB_84;
	opcodes[0x85] = CB_85;
	opcodes[0x86] = CB_86;
	opcodes[0x87] = CB_87;
	opcodes[0x88] = CB_88;
	opcodes[0x89] = CB_89;
	opcodes[0x8A] = CB_8A;
	opcodes[0x8B] = CB_8B;
	opcodes[0x8C] = CB_8C;
	opcodes[0x8D] = CB_8D;
	opcodes[0x8E] = CB_8E;
	opcodes[0x8F] = CB_8F;

	opcodes[0x90] = CB_90;
	opcodes[0x91] = CB_91;
	opcodes[0x92] = CB_92;
	opcodes[0x93] = CB_93;
	opcodes[0x94] = CB_94;
	opcodes[0x95] = CB_95;
	opcodes[0x96] = CB_96;
	opcodes[0x97] = CB_97;
	opcodes[0x98] = CB_98;
	opcodes[0x99] = CB_99;
	opcodes[0x9A] = CB_9A;
	opcodes[0x9B] = CB_9B;
	opcodes[0x9C] = CB_9C;
	opcodes[0x9D] = CB_9D;
	opcodes[0x9E] = CB_9E;
	opcodes[0x9F] = CB_9F;

	opcodes[0xA0] = CB_A0;
	opcodes[0xA1] = CB_A1;
	opcodes[0xA2] = CB_A2;
	opcodes[0xA3] = CB_A3;
	opcodes[0xA4] = CB_A4;
	opcodes[0xA5] = CB_A5;
	opcodes[0xA6] = CB_A6;
	opcodes[0xA7] = CB_A7;
	opcodes[0xA8] = CB_A8;
	opcodes[0xA9] = CB_A9;
	opcodes[0xAA] = CB_AA;
	opcodes[0xAB] = CB_AB;
	opcodes[0xAC] = CB_AC;
	opcodes[0xAD] = CB_AD;
	opcodes[0xAE] = CB_AE;
	opcodes[0xAF] = CB_AF;

	opcodes[0xB0] = CB_B0;
	opcodes[0xB1] = CB_B1;
	opcodes[0xB2] = CB_B2;
	opcodes[0xB3] = CB_B3;
	opcodes[0xB4] = CB_B4;
	opcodes[0xB5] = CB_B5;
	opcodes[0xB6] = CB_B6;
	opcodes[0xB7] = CB_B7;
	opcodes[0xB8] = CB_B8;
	opcodes[0xB9] = CB_B9;
	opcodes[0xBA] = CB_BA;
	opcodes[0xBB] = CB_BB;
	opcodes[0xBC] = CB_BC;
	opcodes[0xBD] = CB_BD;
	opcodes[0xBE] = CB_BE;
	opcodes[0xBF] = CB_BF;

	opcodes[0xC0] = CB_C0;
	opcodes[0xC1] = CB_C1;
	opcodes[0xC2] = CB_C2;
	opcodes[0xC3] = CB_C3;
	opcodes[0xC4] = CB_C4;
	opcodes[0xC5] = CB_C5;
	opcodes[0xC6] = CB_C6;
	opcodes[0xC7] = CB_C7;
	opcodes[0xC8] = CB_C8;
	opcodes[0xC9] = CB_C9;
	opcodes[0xCA] = CB_CA;
	opcodes[0xCB] = CB_CB;
	opcodes[0xCC] = CB_CC;
	opcodes[0xCD] = CB_CD;
	opcodes[0xCE] = CB_CE;
	opcodes[0xCF] = CB_CF;

	opcodes[0xD0] = CB_D0;
	opcodes[0xD1] = CB_D1;
	opcodes[0xD2] = CB_D2;
	opcodes[0xD3] = CB_D3;
	opcodes[0xD4] = CB_D4;
	opcodes[0xD5] = CB_D5;
	opcodes[0xD6] = CB_D6;
	opcodes[0xD7] = CB_D7;
	opcodes[0xD8] = CB_D8;
	opcodes[0xD9] = CB_D9;
	opcodes[0xDA] = CB_DA;
	opcodes[0xDB] = CB_DB;
	opcodes[0xDC] = CB_DC;
	opcodes[0xDD] = CB_DD;
	opcodes[0xDE] = CB_DE;
	opcodes[0xDF] = CB_DF;

	opcodes[0xE0] = CB_E0;
	opcodes[0xE1] = CB_E1;
	opcodes[0xE2] = CB_E2;
	opcodes[0xE3] = CB_E3;
	opcodes[0xE4] = CB_E4;
	opcodes[0xE5] = CB_E5;
	opcodes[0xE6] = CB_E6;
	opcodes[0xE7] = CB_E7;
	opcodes[0xE8] = CB_E8;
	opcodes[0xE9] = CB_E9;
	opcodes[0xEA] = CB_EA;
	opcodes[0xEB] = CB_EB;
	opcodes[0xEC] = CB_EC;
	opcodes[0xED] = CB_ED;
	opcodes[0xEE] = CB_EE;
	opcodes[0xEF] = CB_EF;

	opcodes[0xF0] = CB_F0;
	opcodes[0xF1] = CB_F1;
	opcodes[0xF2] = CB_F2;
	opcodes[0xF3] = CB_F3;
	opcodes[0xF4] = CB_F4;
	opcodes[0xF5] = CB_F5;
	opcodes[0xF6] = CB_F6;
	opcodes[0xF7] = CB_F7;
	opcodes[0xF8] = CB_F8;
	opcodes[0xF9] = CB_F9;
	opcodes[0xFA] = CB_FA;
	opcodes[0xFB] = CB_FB;
	opcodes[0xFC] = CB_FC;
	opcodes[0xFD] = CB_FD;
	opcodes[0xFE] = CB_FE;
	opcodes[0xFF] = CB_FF;

}