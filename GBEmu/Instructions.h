#pragma once

#include "hardware/Registers.h"
#include "hardware/BUS.h"

UInt8 ADC(Registers& registers, Reg8& dr, UInt8 sr);
UInt8 ADC_d8(Registers& registers, Reg8& r, UInt8 d);
UInt8 ADD(Registers& registers, Reg8& dr, UInt8 sr);
UInt8 ADD(Registers& registers, Reg16& dr, UInt16 sr);
UInt8 ADD(Registers& registers, Reg16& dr, Int8 n);
UInt8 ADD_d8(Registers& registers, Reg8& r, UInt8 d);
UInt8 ADD_r8(Registers& registers, Reg16& dr, Int8 n);
UInt8 AND(Registers& registers, Reg8& dr, UInt8 sr);
UInt8 AND_d8(Registers& registers, Reg8& dr, UInt8 d);
UInt8 BIT(Registers& registers, UInt8 bit, UInt8 sr);
UInt8 BIT_a16(Registers& reg, BUS& bus, UInt8 bit, UInt16 addr);
UInt8 CALL(Registers& reg, BUS& bus, UInt16 addr);
UInt8 CALL(Registers& reg, BUS& bus, UInt8 cc, UInt16 addr);
UInt8 CCF(Registers& registers);
UInt8 CP(Registers& registers, Reg8 ar, UInt8 r);
UInt8 CP_d8(Registers& registers, UInt8 ar, UInt8 d);
UInt8 CPL(Registers& registers, Reg8& r);
UInt8 DAA(Registers& registers);
UInt8 DEC(Registers& registers, Reg8& r);
UInt8 DEC(Registers& registers, Reg16& r);
UInt8 DEC_HL(Registers& registers, BUS& bus);
UInt8 DI(Registers& registers);
UInt8 EI(Registers& registers);
UInt8 INC(Registers& registers, Reg8& r);
UInt8 INC(Registers& registers, Reg16& r);
UInt8 INC_HL(Registers& registers, BUS& bus);
UInt8 JP(Registers& registers, UInt16 addr);
UInt8 JP(Registers& registers, UInt8 c, UInt16 addr);
UInt8 JP(Registers& registers);
UInt8 JR(Registers& registers, Int8 offset);
UInt8 JR(Registers& registers, UInt8 c, Int8 offset);
UInt8 HALT(Registers& registers);
UInt8 LD(Registers& registers, Reg8& dr, UInt8 sr);
UInt8 LD_d8(Registers& registers, Reg8& dr, UInt8 data);
UInt8 LD_d16(Registers& registers, Reg16& dr, UInt16 data);
UInt8 LD(Registers& registers, Reg16& dr, UInt8 sr);
UInt8 LD(Registers& registers, Reg16& r, UInt16 v);
UInt8 LDAC(Registers& registers, Reg8& r, UInt8 c);
UInt8 LDCA(Registers& reg, BUS& bus, UInt8 c, UInt8 a);
UInt8 LDD(Registers& registers, Reg8& r, UInt16 sr);
UInt8 LDD(Registers& registers, Reg16& r, UInt8 sr);
UInt8 LDH(Registers& registers, UInt8 n, UInt8 r);
UInt8 LDH_A(Registers& registers, Reg8& r, UInt8 n);
UInt8 LDHL_R(Registers& registers, BUS& bus, UInt8 r);
UInt8 LDHL_D(Registers& registers, BUS& bus, UInt8 d);
UInt8 LD_a16(Registers& registers, BUS& bus, UInt16 addr, UInt16 r);
UInt8 LD_a8(Registers& registers, BUS& bus, UInt16 addr, UInt8 r);
UInt8 LDHLSP(Registers& registers,BUS& bus, Int8 r8);
UInt8 LDI(Registers& registers, Reg8& r, UInt16 sr);
UInt8 LDI(Registers& registers, Reg16& r, UInt8 sr);
UInt8 NOP(Registers& registers);
UInt8 OR(Registers& registers, Reg8& r, UInt8 n);
UInt8 OR_d8(Registers& registers, Reg8& r, UInt8 n);
UInt8 POP(Registers& reg, BUS& bus, Reg16& nn);
UInt8 POP_AF(Registers& reg, BUS& bus);
UInt8 PUSH(Registers& reg, BUS& bus, UInt16 nn);
UInt8 RES(Registers& registers, UInt8 bit, Reg8& r);
UInt8 RES_a16(Registers& reg, BUS& bus, UInt8 bit, UInt16 addr);
UInt8 RET(Registers& registers, BUS& bus);
UInt8 RET(Registers& registers, BUS& bus, UInt8 cc);
UInt8 RETI(Registers& registers, BUS& bus);
UInt8 RL(Registers& registers, Reg8& r);
UInt8 RLC(Registers& registers, Reg8& r);
UInt8 RR(Registers& registers, Reg8&);
UInt8 RRC(Registers& registers, Reg8& r);
UInt8 CB_RL(Registers& registers, Reg8& r);
UInt8 CB_RLC(Registers& registers, Reg8& r);
UInt8 CB_RR(Registers& registers, Reg8&);
UInt8 CB_RRC(Registers& registers, Reg8& r);
UInt8 RST(Registers& registers, BUS& bus, UInt8 n);
UInt8 SBC(Registers& registers, Reg8& r, UInt8 n);
UInt8 SBC_d8(Registers& registers, Reg8& r, UInt8 n);
UInt8 SCF(Registers& registers);
UInt8 SET(Registers& registers, UInt8 bit, Reg8& r);
UInt8 SET_a16(Registers& reg, BUS& bus, UInt8 bit, UInt16 addr);
UInt8 SLA(Registers& registers, Reg8& r);
UInt8 SRA(Registers& registers, Reg8& r);
UInt8 SRL(Registers& registers, Reg8& r);
UInt8 SRL_HL(Registers& registers, BUS& bus);
UInt8 STOP(Registers& registers);
UInt8 SUB(Registers& registers, Reg8& r, UInt8 n);
UInt8 SUB_d8(Registers& registers, Reg8& r, UInt8 d);
UInt8 SUB_d8(Registers& registers, Reg8& r, UInt8 n);
UInt8 SWAP(Registers& registers, Reg8& n);
UInt8 XOR(Registers& registers, Reg8& r, UInt8 n);
UInt8 XOR_d8(Registers& registers, Reg8& r, UInt8 d);

inline UInt8 carryCheck3(UInt32 a, Int32 b) {
	return (((a & 0xf) + (b & 0xf)) & 0x10) ? 1 : 0;
}

inline UInt8 carryCheck7(UInt32 a, Int32 b) {
	return (((a & 0xff) + (b & 0xff)) & 0x100) ? 1 : 0;
}

inline UInt8 carryCheck11(UInt32 a, Int32 b) {
	return (((a & 0xfff) + (b & 0xfff)) & 0x1000) ? 1 : 0;
}

inline UInt8 carryCheck15(UInt32 a, Int32 b) {
	return (((a & 0xffff) + (b & 0xffff)) & 0x10000) ? 1 : 0;
}

inline UInt8 carryCheck3Sub(Int32 a, Int32 b) {
	return ((a & 0xF) < (b & 0xF));
}

inline UInt8 carryCheck7Sub(Int32 a, Int32 b) {
	return ((a & 0xFF) < (b & 0xFF));
}

inline void setFlag(Reg8& r, UInt8 mask) {
	r |= mask;
}

inline void resetFlag(Reg8& r, UInt8 mask) {
	r &= 0xff - mask;
}

inline UInt8 conditionCheck(UInt8 cc, UInt8 flags) {
	if (cc & 0x02) {
		if (cc & 0x01) { // 11
			return (flags & C_MASK) ? 1 : 0;
		}
		else { // 10
			return (flags & C_MASK) ? 0 : 1;
		}
	}
	else { // 01
		if (cc & 0x01) {
			return (flags & Z_MASK) ? 1 : 0;
		}
		else { // 00
			return (flags & Z_MASK) ? 0 : 1;
		}
	}


}


inline UInt8 ADC(Registers& reg, Reg8& dr, UInt8 sr) {

	UInt16 v = sr;
	int c = (reg.f & C_MASK) ? 1 : 0;

	bool carry = carryCheck7(dr, sr);
	carry |= carryCheck7(dr + sr, c);

	bool half = carryCheck3(dr, sr);
	half |= carryCheck3(dr + sr, c);

	dr += v + c;

	// Sets C flag if carry
	if (carry) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	// Sets H flag if half carry
	if (half) setFlag(reg.f, H_MASK);
	else resetFlag(reg.f, H_MASK);

	// Sets Z flag if 0
	if (dr) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);

	return 1;
}

inline UInt8 ADC_d8(Registers& registers, Reg8& r, UInt8 d) {
	ADC(registers, r, d);
	return 2;
}

inline UInt8 ADD(Registers& reg, Reg8& dr, UInt8 sr) {

	// Sets C flag if carry
	if (carryCheck7(dr, sr)) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	// Sets H flag if half carry
	if (carryCheck3(dr, sr)) setFlag(reg.f, H_MASK);
	else resetFlag(reg.f, H_MASK);

	dr += sr;

	// Sets Z flag if 0
	if (dr) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);

	return 1;
}

inline UInt8 ADD(Registers& reg, Reg16& dr, UInt16 sr) {
	
	// Sets C flag if carry
	if (carryCheck15(dr, sr)) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	// Sets H flag if half carry
	if (carryCheck11(dr, sr)) setFlag(reg.f, H_MASK);
	else resetFlag(reg.f, H_MASK);
	
	dr += sr;

	resetFlag(reg.f, N_MASK);

	return 2;
}

inline UInt8 ADD(Registers& reg, Reg16& dr, Int8 n) {
	// TODO
	return 0;
}

inline UInt8 ADD_r8(Registers& reg, Reg16& dr, Int8 n) {
	
	// Sets C flag if carry
	if (carryCheck7(dr, n)) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	// Sets H flag if half carry
	if (carryCheck3(dr, n)) setFlag(reg.f, H_MASK);
	else resetFlag(reg.f, H_MASK);
	
	dr += n;

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, Z_MASK);

	return 4;
}

inline UInt8 ADD_d8(Registers& registers, Reg8& r, UInt8 d) {
	ADD(registers, r, d);
	return 2;
}

inline UInt8 AND(Registers& reg, Reg8& dr, UInt8 sr) {

	dr &= sr;

	if (dr) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);

	setFlag(reg.f, H_MASK);

	resetFlag(reg.f, C_MASK);

	return 1;
}

inline UInt8 AND_d8(Registers& registers, Reg8& dr, UInt8 d) {
	AND(registers, dr, d);
	return 2;
}

inline UInt8 BIT(Registers& reg, UInt8 bit, UInt8 sr) {

	if (sr & 1 << bit) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);

	setFlag(reg.f, H_MASK);

	return 2;
}

inline UInt8 BIT_a16(Registers& reg, BUS& bus, UInt8 bit, UInt16 addr) {
	UInt8 value = bus.read8(addr);

	if (value & 1 << bit) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);

	setFlag(reg.f, H_MASK);

	return 4;
}

inline UInt8 CALL(Registers& reg, BUS& bus, UInt16 addr) {
	PUSH(reg, bus, reg.pc + 2);
	reg.pc = addr;

	return 6;
}

inline UInt8 CALL(Registers& reg, BUS& bus, UInt8 cc, UInt16 addr) {

	if (conditionCheck(cc, reg.f)) {
		CALL(reg, bus, addr);
		return 6;
	}
	reg.pc += 2;
	return 3;
}

inline UInt8 CCF(Registers& reg) {

	reg.f ^= C_MASK;

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 CP(Registers& reg, Reg8 ar, UInt8 r) {
	SUB(reg, ar, r);
	return 1;
}

inline UInt8 CP_d8(Registers& registers, UInt8 ar, UInt8 d) {
	CP(registers, ar, d);
	return 2;
}

inline UInt8 CPL(Registers& reg, Reg8& r) {

	r ^= 0xff;

	setFlag(reg.f, N_MASK);
	setFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 DAA(Registers& reg) {
	// This code is pretty much all taken from here
	// because I had no idea how to implement this on my own
	// https://github.com/nnarain/gameboycore/blob/master/src/gameboycore/src/cpu.cpp

	Reg8& a = reg.a;

	bool n = (reg.f & N_MASK) != 0;
	bool h = (reg.f & H_MASK) != 0;
	bool c = (reg.f & C_MASK) != 0;

	if (!n)
	{
		if (c || (a > 0x99)) {
			a = (a + 0x60) & 0xFF;
			setFlag(reg.f, C_MASK);
		}
		if (h || ((a & 0x0F) > 9)) {
			a = (a + 0x06) & 0xFF;
			resetFlag(reg.f, H_MASK);
		}
	}
	else if (c && h)
	{
		a = (a + 0x9A) & 0xFF;
		resetFlag(reg.f, H_MASK);
	}
	else if (c)
	{
		a = (a + 0xA0) & 0xFF;
	}
	else if (h)
	{
		a = (a + 0xFA) & 0xFF;
		resetFlag(reg.f, H_MASK);
	}

	if(a)resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	return 1;
}

inline UInt8 DEC(Registers& reg, Reg8& r) {
	r--;

	if (r)resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	setFlag(reg.f, N_MASK);

	if ((r & 0x0F) == 0x0F) setFlag(reg.f, H_MASK);
	else resetFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 DEC(Registers& reg, Reg16& r) {
	r--;
	return 2;
}

inline UInt8 DEC_HL(Registers& registers, BUS& bus) {
	Reg8 data = bus.read8(registers.hl);
	DEC(registers, data);
	bus.write8(registers.hl, data);
	return 3;
}

inline UInt8 DI(Registers& reg) {
	reg.interruptPending = -1;
	return 1;
}

inline UInt8 EI(Registers& reg) {
	reg.IME = 1;
	return 1;
}

inline UInt8 INC(Registers& reg, Reg8& r) {

	if (carryCheck3(r, 1)) setFlag(reg.f, H_MASK);
	else resetFlag(reg.f, H_MASK);

	r++;

	resetFlag(reg.f, N_MASK);

	if (r) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	return 1;
}

inline UInt8 INC(Registers& reg, Reg16& r) {
	r++;

	return 2;
}

inline UInt8 INC_HL(Registers& registers, BUS& bus) {
	Reg8 data = bus.read8(registers.hl);
	INC(registers, data);
	bus.write8(registers.hl, data);
	return 3;
}

inline UInt8 JP(Registers& reg, UInt16 addr) {
	reg.pc = addr;
	return 4;
}

inline UInt8 JP(Registers& reg, UInt8 cc, UInt16 addr) {
	if (conditionCheck(cc, reg.f)) {
		reg.pc = addr;
		return 4;
	}
	reg.pc += 2;
	return 3;
}

inline UInt8 JP(Registers& reg) {
	return 0;
}

inline UInt8 JR(Registers& reg, Int8 offset) {
	reg.pc += offset;
	return 3;
}

inline UInt8 JR(Registers& reg, UInt8 cc, Int8 offset) {
	if (conditionCheck(cc, reg.f)) {
		reg.pc += offset;
		return 3;
	}
	//reg.pc += 1;
	return 2;
}

inline UInt8 HALT(Registers& reg) {
	reg.halted = true;
	return 1;
}

inline UInt8 LD(Registers& reg, Reg8& dr, UInt8 sr) {
	dr = sr;
	return 1;
}

inline UInt8 LD_d8(Registers& registers, Reg8& dr, UInt8 data) {
	dr = data;
	return 2;
}

inline UInt8 LD_d16(Registers& registers, Reg16& dr, UInt16 data) {
	dr = data;
	return 3;
}

inline UInt8 LD(Registers& reg, Reg16& dr, UInt8 sr) {
	return 0;
}

inline UInt8 LDAC(Registers& reg, Reg8& r, UInt8 c) {
	r = c;
	return 2;
}

inline UInt8 LDCA(Registers& reg, BUS& bus, UInt8 c, UInt8 a) {
	bus.write8(0xFF00 + c, a);
	return 2;
}

inline UInt8 LD(Registers& reg, Reg16& r, UInt16 v) {
	r = v;
	return 2;
}

inline UInt8 LDD(Registers& reg, Reg8& r, UInt16 sr) {
	return 0;
}

inline UInt8 LDD(Registers& reg, Reg16& r, UInt8 sr) {
	return 0;
}

inline UInt8 LDH(Registers& reg, UInt8 n, UInt8 r) {
	return 0;
}

inline UInt8 LDH_A(Registers& reg, Reg8& r, UInt8 n) {
	LD(reg, r, n);
	return 3;
}

inline UInt8 LDHL_R(Registers& registers, BUS& bus, UInt8 r) {
	bus.write8(registers.hl, r);
	return 2;
}

inline UInt8 LDHL_D(Registers& registers, BUS& bus, UInt8 d) {
	bus.write8(registers.hl, d);
	return 3;
}

inline UInt8 LD_a16(Registers& registers, BUS& bus, UInt16 addr, UInt16 r) {
	bus.write16(addr, r);
	return 4;
}

inline UInt8 LD_a8(Registers& registers, BUS& bus, UInt16 addr, UInt8 r) {
	bus.write8(addr, r);
	return 2;
}

inline UInt8 LDHL(Registers& reg, UInt16 sp, UInt8 n) {
	return 0;
}

inline UInt8 LDHLSP(Registers& reg, BUS& bus, Int8 n) {
	UInt16 sp = reg.sp;

	UInt16 result = sp + n;

	bool carry = carryCheck7(sp, n);
	bool half = carryCheck3(sp, n);
	
	reg.hl = result;
	
	if (carry) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	if (half) setFlag(reg.f, H_MASK);
	else resetFlag(reg.f, H_MASK);

	resetFlag(reg.f, Z_MASK);
	resetFlag(reg.f, N_MASK);

	return 3;
}

inline UInt8 LDI(Registers& reg, Reg8& r, UInt16 sr) {
	return 0;
}

inline UInt8 LDI(Registers& reg, Reg16& r, UInt8 sr) {
	return 0;
}

inline UInt8 NOP(Registers& reg) {
	return 1;
}

inline UInt8 OR(Registers& reg, Reg8& r, UInt8 n) {

	r |= n;

	if (r) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);
	resetFlag(reg.f, C_MASK);

	return 1;
}

inline UInt8 OR_d8(Registers& registers, Reg8& r, UInt8 n) {
	OR(registers, r, n);
	return 2;
}

inline UInt8 POP(Registers& reg, BUS& bus, Reg16& nn) {
	//std::cout << "POP" << std::endl;

	nn = bus.read16(reg.sp);

	reg.sp += 2;
	return 3;
}

inline UInt8 POP_AF(Registers& reg, BUS& bus) {

	UInt16 af = bus.read16(reg.sp);
	af &= 0xFFF0;

	reg.af = af;

	reg.sp += 2;
	return 3;
}

inline UInt8 PUSH(Registers& reg, BUS& bus, UInt16 nn) {
	//std::cout << "PUSH" << std::endl;

	reg.sp -= 2;
	bus.write16(reg.sp, nn);

	return 4;
}

inline UInt8 RES(Registers& reg, UInt8 bit, Reg8& r) {
	UInt8 mask = 0xFF - (1 << bit);
	r &= mask;
	return 2;
}

inline UInt8 RES_a16(Registers& reg, BUS& bus, UInt8 bit, UInt16 addr){
	Reg8 data = bus.read8(addr);
	RES(reg, bit, data);
	bus.write8(addr, data);
	return 4;
}

inline UInt8 RET(Registers& reg, BUS& bus) {
	Reg16 addr;
	POP(reg, bus, addr);
	reg.pc = addr;
	return 4;
}

inline UInt8 RET(Registers& reg, BUS& bus, UInt8 cc) {
	if (conditionCheck(cc, reg.f)) {
		RET(reg, bus);
		return 5;
	}
	else {
		return 2;
	}
	
}

inline UInt8 RETI(Registers& reg, BUS& bus) {
	reg.IME = 0xFF;
	reg.interruptPending = 0;
	return RET(reg, bus);
}

inline UInt8 RL(Registers& reg, Reg8& r) {

	UInt8 old = r;
	r <<= 1;

	if (reg.f & C_MASK) r |= 0x01;

	if (old & 0x80) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	resetFlag(reg.f, Z_MASK);
	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 RLC(Registers& reg, Reg8& r) {

	if (r & 0x80) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	r <<= 1;

	if (reg.f & C_MASK) r |= 0x01;

	resetFlag(reg.f, Z_MASK);
	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 RR(Registers& reg, Reg8& r) {
	UInt8 old = r;
	r >>= 1;

	if (reg.f & C_MASK) r |= 0x80;

	if (old & 0x01) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	resetFlag(reg.f, Z_MASK);
	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 RRC(Registers& reg, Reg8& r) {
	if (r & 0x01) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	r >>= 1;

	if (reg.f & C_MASK) r |= 0x80;

	resetFlag(reg.f, Z_MASK);
	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 CB_RL(Registers& reg, Reg8& r) {

	UInt8 old = r;
	r <<= 1;

	if (reg.f & C_MASK) r |= 0x01;

	if (old & 0x80) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	if (r) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 CB_RLC(Registers& reg, Reg8& r) {

	if (r & 0x80) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	r <<= 1;

	if (reg.f & C_MASK) r |= 0x01;

	if (r) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 CB_RR(Registers& reg, Reg8& r) {
	UInt8 old = r;
	r >>= 1;

	if (reg.f & C_MASK) r |= 0x80;

	if (old & 0x01) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	if (r) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 CB_RRC(Registers& reg, Reg8& r) {
	if (r & 0x01) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	r >>= 1;

	if (reg.f & C_MASK) r |= 0x80;

	if (r) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);
	
	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 1;
}

// PUSH(reg, bus, reg.pc + 3);
// reg.pc = addr;
inline UInt8 RST(Registers& reg, BUS& bus, UInt8 n) {

	PUSH(reg, bus, reg.pc);
	reg.pc = (UInt16)n;

	return 4;
}


inline UInt8 SBC(Registers& reg, Reg8& dr, UInt8 sr) {

	int c = (reg.f & C_MASK) ? 1 : 0;

	bool carry = carryCheck7Sub(dr, sr);
	carry |= carryCheck7Sub(dr - sr, c);

	bool half = carryCheck3Sub(dr, sr);
	half |= carryCheck3Sub(dr - sr, c);

	dr -= (sr + c);

	// Sets C flag if carry
	if (carry) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	// Sets H flag if half carry
	if (half) setFlag(reg.f, H_MASK);
	else resetFlag(reg.f, H_MASK);

	// Sets Z flag if 0
	if (dr) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	setFlag(reg.f, N_MASK);

	return 1;
}

inline UInt8 SBC_d8(Registers& registers, Reg8& r, UInt8 n) {
	SBC(registers, r, n);
	return 2;
}

inline UInt8 SCF(Registers& reg) {
	setFlag(reg.f, C_MASK);
	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 1;
}

inline UInt8 SET(Registers& reg, UInt8 b, Reg8& r) {
	setFlag(r, 1 << b);

	return 2;
}

inline UInt8 SET_a16(Registers& reg, BUS& bus, UInt8 b, UInt16 addr) {
	Reg8 data = bus.read8(addr);
	SET(reg, b, data);
	bus.write8(addr, data);
	return 4;
}

inline UInt8 SLA(Registers& reg, Reg8& r) {
	if (r & 0x80) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	r <<= 1;

	if (r)resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 2;
}

inline UInt8 SRA(Registers& reg, Reg8& r) {

	if (r & 0x1) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	r >>= 1;
	r += (r & 0x40) << 1;

	if (r)resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 2;
}

inline UInt8 SRL(Registers& reg, Reg8& r) {

	if (r & 0x1) setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	r >>= 1;

	if (r)resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);

	return 2;
}

inline UInt8 SRL_HL(Registers& registers, BUS& bus) {
	Reg8 data = bus.read8(registers.hl);

	SRL(registers, data);

	bus.write8(registers.hl, data);

	return 4;
}

inline UInt8 STOP(Registers& reg) {
	return 1;
}

inline UInt8 SUB(Registers& reg, Reg8& r, UInt8 n) {

	if (carryCheck7Sub(r, n))setFlag(reg.f, C_MASK);
	else resetFlag(reg.f, C_MASK);

	if (carryCheck3Sub(r, n))setFlag(reg.f, H_MASK);
	else resetFlag(reg.f, H_MASK);

	r -= n;

	if (r)resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	setFlag(reg.f, N_MASK);
	return 1;
}

inline UInt8 SUB_d8(Registers& reg, Reg8& r, UInt8 n) {
	SUB(reg, r, n);
	return 2;
}

inline UInt8 SWAP(Registers& reg, Reg8& n) {
	n = (n << 4) | (n >> 4);

	if (n) resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);
	resetFlag(reg.f, C_MASK);

	return 2;
}

inline UInt8 XOR(Registers& reg, Reg8& r, UInt8 n) {

	r ^= n;

	if (r)resetFlag(reg.f, Z_MASK);
	else setFlag(reg.f, Z_MASK);

	resetFlag(reg.f, N_MASK);
	resetFlag(reg.f, H_MASK);
	resetFlag(reg.f, C_MASK);

	return 1;
}

inline UInt8 XOR_d8(Registers& registers, Reg8& r, UInt8 d) {
	XOR(registers, r, d);
	return 2;
}

/*
UInt8 ADC(Registers& registers, Reg8& dr, UInt8 sr);
UInt8 ADC_d8(Registers& registers, Reg8& r, UInt8 d);
UInt8 ADD(Registers& registers, Reg8& dr, UInt8 sr);
UInt8 ADD(Registers& registers, Reg16& dr, UInt16 sr);
UInt8 ADD(Registers& registers, Reg16& dr, Int8 n);
UInt8 ADD_d8(Registers& registers, Reg8& r, UInt8 d);
UInt8 AND(Registers& registers, Reg8& dr, UInt8 sr);
UInt8 AND_d8(Registers& registers, Reg8& dr, UInt8 d);
UInt8 BIT(Registers& registers, UInt8 bit, UInt8 sr);
UInt8 BIT_a16(Registers& reg, BUS& bus, UInt8 bit, UInt16 addr);
UInt8 CALL(Registers& reg, BUS& bus, UInt16 addr);
UInt8 CALL(Registers& reg, BUS& bus, UInt8 cc, UInt16 addr);
UInt8 CCF(Registers& registers);
UInt8 CP(Registers& registers, Reg8 ar, UInt8 r);
UInt8 CP_d8(Registers& registers, UInt8 ar, UInt8 d);
UInt8 CPL(Registers& registers, Reg8& r);
UInt8 DAA(Registers& registers);
UInt8 DEC(Registers& registers, Reg8& r);
UInt8 DEC(Registers& registers, Reg16& r);
UInt8 DEC_HL(Registers& registers, BUS& bus);
UInt8 DI(Registers& registers);
UInt8 EI(Registers& registers);
UInt8 INC(Registers& registers, Reg8& r);
UInt8 INC(Registers& registers, Reg16& r);
UInt8 INC_HL(Registers& registers, BUS& bus);
UInt8 JP(Registers& registers, UInt16 addr);
UInt8 JP(Registers& registers, UInt8 c, UInt16 addr);
UInt8 JP(Registers& registers);
UInt8 JR(Registers& registers, Int8 offset);
UInt8 JR(Registers& registers, UInt8 c, Int8 offset);
UInt8 HALT(Registers& registers);
UInt8 LD(Registers& registers, Reg8& dr, UInt8 sr);
UInt8 LD_d8(Registers& registers, Reg8& dr, UInt8 data);
UInt8 LD_d16(Registers& registers, Reg16& dr, UInt16 data);
UInt8 LD(Registers& registers, Reg16& dr, UInt8 sr);
UInt8 LD(Registers& registers, Reg16& r, UInt16 v);
UInt8 LDAC(Registers& registers, Reg8& r, UInt8 c);
UInt8 LDCA(Registers& reg, BUS& bus, UInt8 c, UInt8 a);
UInt8 LDD(Registers& registers, Reg8& r, UInt16 sr);
UInt8 LDD(Registers& registers, Reg16& r, UInt8 sr);
UInt8 LDH(Registers& registers, UInt8 n, UInt8 r);
UInt8 LDH_A(Registers& registers, Reg8& r, UInt8 n);
UInt8 LDHL_R(Registers& registers, BUS& bus, UInt8 r);
UInt8 LDHL_D(Registers& registers, BUS& bus, UInt8 d);
UInt8 LD_a16(Registers& registers, BUS& bus, UInt16 addr, UInt16 r);
UInt8 LD_a8(Registers& registers, BUS& bus, UInt16 addr, UInt8 r);
UInt8 LDHLSP(Registers& registers, UInt16 sp, UInt8 n);
UInt8 LDI(Registers& registers, Reg8& r, UInt16 sr);
UInt8 LDI(Registers& registers, Reg16& r, UInt8 sr);
UInt8 NOP(Registers& registers);
UInt8 OR(Registers& registers, Reg8& r, UInt8 n);
UInt8 OR_d8(Registers& registers, Reg8& r, UInt8 n);
UInt8 POP(Registers& reg, BUS& bus, Reg16& nn);
UInt8 PUSH(Registers& reg, BUS& bus, UInt16 nn);
UInt8 RES(Registers& registers, UInt8 bit, Reg8& r);
UInt8 RES_a16(Registers& reg, BUS& bus, UInt8 bit, UInt16 addr);
UInt8 RET(Registers& registers, BUS& bus);
UInt8 RET(Registers& registers, BUS& bus, UInt8 cc);
UInt8 RETI(Registers& registers, BUS& bus);
UInt8 RL(Registers& registers, Reg8& r);
UInt8 RLC(Registers& registers, Reg8& r);
UInt8 RR(Registers& registers, Reg8&);
UInt8 RRC(Registers& registers, Reg8& r);
UInt8 RST(Registers& registers, BUS& bus, UInt8 n);
UInt8 SBC(Registers& registers, Reg8& r, UInt8 n);
UInt8 SBC_d8(Registers& registers, Reg8& r, UInt8 n);
UInt8 SCF(Registers& registers);
UInt8 SET(Registers& registers, UInt8 bit, Reg8& r);
UInt8 SET_a16(Registers& reg, BUS& bus, UInt8 bit, UInt16 addr);
UInt8 SLA(Registers& registers, Reg8& r);
UInt8 SRA(Registers& registers, Reg8& r);
UInt8 SRL(Registers& registers, Reg8& r);
UInt8 STOP(Registers& registers);
UInt8 SUB(Registers& registers, Reg8& r, UInt8 n);
UInt8 SUB_d8(Registers& registers, Reg8& r, UInt8 d);
UInt8 SUB_d8(Registers& registers, Reg8& r, UInt8 n);
UInt8 SWAP(Registers& registers, Reg8& n);
UInt8 XOR(Registers& registers, Reg8& r, UInt8 n);
UInt8 XOR_d8(Registers& registers, Reg8& r, UInt8 d);
*/



