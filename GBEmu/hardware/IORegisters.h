#pragma once

#include "BUS.h"
#include <iostream>
#include <iomanip>

enum IOAddresses : UInt16 {

	TIMA = 0xFF05,
	TMA = 0xFF06,
	TMC = 0xFF07,

	LDCD = 0xFF40,
	LDCSTAT = 0xFF41,
	SCY = 0xFF42,
	SCX = 0xFF43,
	LY = 0XFF44,
	LYC = 0XFF45,

	BGP = 0xFF47,
	OBP0 = 0xFF48,
	OBP1 = 0xFF49,

	WY = 0xFF4A,
	WX = 0xFF4B,
};



struct JoypadState {
	bool start;
	bool select;
	bool b;
	bool a;
	bool down;
	bool up;
	bool left;
	bool right;


};

class IORegisters : BUSDevice {

public:
	JoypadState& joypad;



	IORegisters(BUS& bus, JoypadState& joypad) : BUSDevice(bus), joypad(joypad) {
		joypad.a = false;
		joypad.b = false;
		joypad.start = false;
		joypad.select = false;
		joypad.up = false;
		joypad.down = false;
		joypad.left = false;
		joypad.right =  false;
	};

	UInt8 ram[0x100] = {0};

	UInt8 inRange(UInt16 addr) {
		return ((addr >= 0xFF00) && (addr <= 0xFF7F));
	}

	UInt8 read(UInt16 addr) {
		if (addr >= 0xFF04 && addr <= 0xFF08) return defaultReturnValue;
		if (addr == 0xFF00) {
			UInt8 s = ram[0] & (0x30);
			if (!(s&(1 << 5))) {
				if (!joypad.start) s |= (1 << 3);
				if (!joypad.select) s |= (1 << 2);
				if (!joypad.b) s |= (1 << 1);
				if (!joypad.a) s |= (1 << 0);
			}
			else {
				if (!joypad.down) s |= (1 << 3);
				if (!joypad.up) s |= (1 << 2);
				if (!joypad.left) s |= (1 << 1);
				if (!joypad.right) s |= (1 << 0);
			}
			return s;

		}
		if (inRange(addr)) {
			return ram[addr - 0xFF00];
		}
		return defaultReturnValue;
	}

	void write(UInt16 addr, UInt8 value) {
		if (inRange(addr)) {
			ram[addr - 0xFF00] = value;
		}
	}
};
