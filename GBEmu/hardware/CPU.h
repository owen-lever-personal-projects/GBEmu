#pragma once

#include <iostream>
#include <fstream>

#include "Types.h"

#include "BUS.h"
#include "Registers.h"

#include "OPCodes.h"

#include "Instructions.h"

#include "GPU.h"
#include "TIMER.h"

const UInt16 InterruptEnable = 0xFFFF;
const UInt16 InterruptFlag = 0xFF0F;



struct CPU : public BUSDevice{

	unsigned long frequency = 0;
	unsigned long cycles = 0;
	int machineCycles;
	Registers registers = Registers();

	std::ofstream myfile;
	
	GPU& gpu;
	Timer& timer;

	bool runCodes[256];
	bool runCB[256];
	bool& log;

	CPU(BUS& bus, GPU& gpu, Timer& timer, bool& log) : BUSDevice(bus), gpu(gpu), timer(timer), log(log){
		initOpcodes(opCodes);
		//registers.initGB();
		//bus.booting = false;

		myfile.open("example.txt");

		for (bool& b : runCodes)b = false;
		for (bool& b : runCB)b = false;
	}

	UInt8 inRange(UInt16 addr) override{ return 0; }
	UInt8 BUSDevice::read(UInt16 addr) override{ return defaultReturnValue; }
	void BUSDevice::write(UInt16 addr, UInt8 value) override{ }

	OPCode opCodes[256];

	int stepCount = 0;

	void step() {
		stepCount++;
		machineCycles = 0;

		if (log) {
			myfile << registers << "\n";
		}

		if (registers.halted) {
			machineCycles = 1;
		}
		else {
			UInt8 op = bus.read8(registers.pc);

			registers.pc++;
			machineCycles = opCodes[op](registers, bus);
		}

		checkInterupts();
		int clockCycles = machineCycles * 4;


		cycles += clockCycles;
		timer.update(clockCycles);
		gpu.update(clockCycles, registers.IME);
		
	}

	void updateInterruptMasterEnable() {
		Int8& pending = registers.interruptPending;

		Int8 enableTime = 2;	// Interrupts are enabled after the next instruction
		Int8 disableTime = 1;	// Interrupts are disabled immediately
		
		if (pending > 0) pending++;
		else if (pending < 0) pending--;
		


		if(pending >= enableTime){
			pending = 0;
			registers.IME = 0xFF;
		} else if (pending >= -disableTime) {
			pending == 0;
			registers.IME = 0x00;
		}

	}

	bool checkInterupts() {
		
		if (registers.IME) {
			UInt8 IE = bus.read8(InterruptEnable);
			
			UInt8 IF = bus.read8(InterruptFlag);

			UInt8 readyInterrupts = IE & IF;

			if (readyInterrupts) registers.halted = false;

			//std::cout << std::hex << (int)IE << ":" << (int)IF << std::endl;


			if (readyInterrupts & InterruptMasks::V_Blank) {
				std::cout << "Entered VBLANK" << std::endl;
				interrupt(InterruptVectors::V_Blank, InterruptMasks::V_Blank);
				return true;
			}
				
			else if (readyInterrupts & InterruptMasks::LCD_STAT) {
				interrupt(InterruptVectors::LCD_STAT, InterruptMasks::LCD_STAT);
				return true;
			}

			else if (readyInterrupts & InterruptMasks::TIMER) {
				interrupt(InterruptVectors::TIMER, InterruptMasks::TIMER);
				return true;
			}

			else if (readyInterrupts & InterruptMasks::SERIAL) {
				interrupt(InterruptVectors::SERIAL, InterruptMasks::SERIAL);
				return true;
			}

			else if (readyInterrupts & InterruptMasks::JOYPAD) {
				interrupt(InterruptVectors::JOYPAD, InterruptMasks::JOYPAD);
				return true;
			}
		}
		return false;

		updateInterruptMasterEnable();
	}

	void interrupt(UInt16 vector, UInt8 mask) {
		//std::cout << "Called interupt at address :" << std::hex << std::uppercase << std::setw(4) << vector << std::endl;
		// Disables interrupts
		registers.IME = 0x00;

		// Resets interrupt flag bit
		UInt8 IF = bus.read8(InterruptFlag);
		bus.write8(InterruptFlag, IF ^= mask);

		// Calls the interupt
		PUSH(registers, bus, registers.pc);
		registers.pc = vector;

		// Updates the cycles count
		machineCycles += 5;
	}

	
};