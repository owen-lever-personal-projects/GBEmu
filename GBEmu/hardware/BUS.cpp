#pragma once

#include <iostream>
#include "BUS.h"

UInt8 BUS::read8(UInt16 addr) {
	if (booting && addr < 0x100) return bootRom[addr];


	UInt8 r = defaultReturnValue;
	int devCount = 0;

	for (BUSDevice* device : connectedDevices) {
		if (device->inRange(addr)) {
			r &= device->read(addr);
			devCount++;
		}
	}
	return r;
}

UInt16 BUS::read16(UInt16 addr) {
	UInt16 r = read8(addr);
	addr++;
	r += (read8(addr) << 8) & 0xFF00;
	return r;
}

void BUS::write8(UInt16 addr, UInt8 data) {
	if (booting && addr == 0xFF50) {
		booting = false;
		std::cout << "Exited boot routine" << std::endl;
	}

	int devCount = 0;
	for (BUSDevice* device : connectedDevices) {
		if (device->inRange(addr) || true) {
			device->write(addr, data);
			devCount++;
		}
	}
}
void BUS::write16(UInt16 addr, UInt16 data) {
	write8(addr, data);
	addr++;
	write8(addr, (data >> 8));
}

void BUS::addDevice(BUSDevice* device) {
	std::cout << "Device attached to bus" << std::endl;
	connectedDevices.push_back(device);
}

void BUS::removeDevice(BUSDevice* device) {

	for (auto it = connectedDevices.begin(); it != connectedDevices.end(); it++) {
		if ((BUSDevice*) & *it == device) connectedDevices.erase(it);
	}
}

BUSDevice::BUSDevice(BUS& bus) : bus(bus){
	bus.addDevice(this);
}