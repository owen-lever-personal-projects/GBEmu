#pragma once

#include "BUS.h"
#include <iostream>

class HighRAM : BUSDevice {

public:

	HighRAM(BUS& bus) : BUSDevice(bus) {
		for (UInt8& byte : hram) byte = 0x00;
	};

	UInt8 inRange(UInt16 addr) {
		return ((addr >= 0xFF80) && (addr <= 0xFFFF)) &&
			!(addr >= 0xFE00 && addr <= 0xFE9F);// CHANGE LATER TO NOT INCLUDE FFFF
	}

	UInt8 hram[0x80] = {0};

	UInt8 read(UInt16 addr) {
		if (inRange(addr)) {
			return hram[addr - 0xFF80];
		}
		return defaultReturnValue;
	}

	void write(UInt16 addr, UInt8 value) {
		if (inRange(addr)) {
			hram[addr - 0xFF80] = value;
		}
	}
};