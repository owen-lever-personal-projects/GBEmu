#pragma once

#include "../Types.h"
#include <vector>

struct AddressRange;
struct BUS;
struct BUSDevice;



struct BUS {

	std::vector<BUSDevice*> connectedDevices;
	UInt8 bootRom[0x100];
	bool booting = true;

	void addDevice(BUSDevice* device);
	void removeDevice(BUSDevice* device);


	UInt8 read8(UInt16 addr);
	UInt16 read16(UInt16 addr);

	void write8(UInt16 addr, UInt8 data);
	void write16(UInt16 addr, UInt16 data);

	static const UInt8 defaultReturnValue = 0xFF;
};

class BUSDevice {

	public:
	BUS& bus;

	BUSDevice(BUS& bus);

	virtual UInt8 inRange(UInt16 addr) = 0;

	virtual UInt8 read(UInt16 addr) = 0;
	virtual void write(UInt16 addr, UInt8 value) = 0;

	static const UInt8 defaultReturnValue = BUS::defaultReturnValue;
};