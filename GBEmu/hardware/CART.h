#pragma once

#include "BUS.h"
#include <vector>

#define ROM_PAGE_SIZE 0x4000
#define RAM_PAGE_SIZE 0x2000

struct CartRamPage {
	char data[RAM_PAGE_SIZE];

	char& operator[](int index) {
		return data[index];
	}
};

struct CartRomPage {
	char data[ROM_PAGE_SIZE];

	char& operator[](int index) {
		return data[index];
	}
};

struct CartRom {
	std::string filepath;
	std::vector<CartRomPage> data;

	CartRom(std::string filepath, std::vector<CartRomPage> data) {
		this->filepath = filepath;
	}

	CartRom(std::string filepath, int pages, const char* data) {
		this->filepath = filepath;
		this->data.resize(pages);
		for (int i = 0; i < pages; i++) {
			auto& page = this->data[i];
			int offset = ROM_PAGE_SIZE * i;
			memcpy(page.data, data + offset, ROM_PAGE_SIZE);
		}
	}

	CartRomPage& operator[](int index) {
		return data[index];
	}
};

struct CartRam {
	std::string filepath;
	std::vector<CartRamPage> data;

	CartRam(std::string filepath, std::vector<CartRamPage> data) {
		this->filepath = filepath;
		this->data = data;
	}

	CartRam(std::string filepath, int pages, const char* data) {
		this->filepath = filepath;
		this->data.resize(pages);
		for (int i = 0; i < pages; i++) {
			auto& page = this->data[i];
			int offset = RAM_PAGE_SIZE * i;
			memcpy(page.data, data + offset, RAM_PAGE_SIZE);
		}
	}

	CartRamPage& operator[](int index) {
		return data[index];
	}
};

enum CartType {
	NoMBC,
	MBC1,
	MBC2,
	MBC3,
	MBC5,
	MBC6,
	PocketCamera,
	TAMA5,
	MBC7,
	HuC1,
	Huc3,
	MMM01,
};

class Cart : public BUSDevice {

	public :
	CartType type;

	CartRom rom;
	CartRam ram;

	Cart(BUS& bus, CartRom& rom, CartRam& ram, CartType type) : BUSDevice(bus), rom(rom), ram(ram), type(type) {
	};
};


class CartNoMBC : public Cart {

	
	public:
	CartNoMBC(BUS& bus, CartRom& rom, CartRam& ram) : Cart(bus, rom, ram, NoMBC){
	}

	UInt8 inRange(UInt16 addr) override {
		return (addr < 0x8000);
	}

	UInt8 read(UInt16 addr) override {
		if (addr >= 0x0000 && addr <= 0x3FFF) {
			return rom[0][addr - 0x0000];
		}
		if (addr >= 0x4000 && addr <= 0x7FFF) {
			return rom[1][addr - 0x4000];
		}
		return defaultReturnValue;
	}
	
	void write(UInt16 addr, UInt8 value) override{
	
	}


};

