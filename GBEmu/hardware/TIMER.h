#pragma once

#include "BUS.h"
#include "IORegisters.h"

class Timer : public BUSDevice {

	int CPU_CLOCKSPEED = 4194304;


	UInt16 divFull = 0;

	
	UInt8 tima = 0;
	UInt8 tma = 0;
	UInt8 tac = 0;

	bool fallingEdge = false;

	bool toIncTima = false;



public:

	Timer(BUS& bus) : BUSDevice(bus) {

	}

	void update(UInt16 cycles) {

		divFull += cycles;

		if (toIncTima) {
			toIncTima = false;
			incrementTima();
		}

		bool isEnabled = (tac & 0x04);
		int bits[] = { 9, 3, 5, 7 };
		int bit = bits[tac & 0x03];


		bool tick = isEnabled && (divFull & (1 << bit));


		if (!tick && fallingEdge) {
			toIncTima = true;
		}

		fallingEdge = tick;

	}

	UInt8 inRange(UInt16 addr) override {
		return (addr == 0xFF04 || 
			addr == 0xFF05 || 
			addr == 0xFF06 || 
			addr == 0xFF07);
	}

	UInt8 read(UInt16 addr) override {

		if (addr == 0xFF04) return (divFull >> 8);
		if (addr == 0xFF05) return tima;
		if (addr == 0xFF06) return tma;
		if (addr == 0xFF07) return tac;
		return defaultReturnValue;
	}

	void write(UInt16 addr, UInt8 value) override {
		if (addr == 0xFF04) {
			divFull = 0;
		}
		if (addr == 0xFF05) {
			tima = value;
		}
		if (addr == 0xFF06) {
			tma = value;
		}
		if (addr == 0xFF07) {
			tac = value;
		}
	}

	void incrementTima() {
		if (tima == 0xFF) {
			tima = tma;
			UInt8 IF = bus.read8(0xFF0F);
			bus.write8(0xFF0F, IF | (1 << 2));
		}
		else {
			tima++;
		}
	}



};
