#pragma once

#include "Types.h"
#include <iostream>
#include <iomanip>

struct Reg8 {
	UInt8 value;

	// Conversion operators
	Reg8() { value = 0; };
	Reg8(UInt8 other) { this->value = other;};
	operator UInt8() { return value; };
	
	// Arithmetic operators
	inline Reg8& operator += (UInt8 right) {
		value += right;
		return *this;
	}

	inline Reg8& operator -= (UInt8 right) {
		value -= right;
		return *this;
	}

	inline Reg8& operator |= (UInt8 right) {
		value |= right;
		return *this;
	}

	inline Reg8& operator &= (UInt8 right) {
		value &= right;
		return *this;
	}

	inline Reg8& operator ^= (UInt8 right) {
		value ^= right;
		return *this;
	}

	inline Reg8& operator >>= (UInt8 right) {
		value >>= right;
		return *this;
	}

	inline Reg8& operator <<= (UInt8 right) {
		value <<= right;
		return *this;
	}

	inline void operator -- (int) {
		value--;
	}

	inline void operator ++ (int) {
		value++;
	}
};

struct Reg16 {
	Reg8 low;
	Reg8 high;

	Reg16() {};

	Reg16(UInt16 other) {
		low = other;
		high = (other >> 8);
	};

	inline operator UInt16(){
		return low + (high << 8);
	}

	inline void operator += (int other) {
		UInt16 v = *this;
		v += other;
		*this = v;
	}

	inline void operator -= (int other) {
		UInt16 v = *this;
		v -= other;
		*this = v;
	}

	inline void operator -- (int) {
		//UInt16 v = *this;
		if (!low) high--;
		low--;
		//return v;
	}

	inline void operator ++ (int) {
		//UInt16 v = *this;
		low++;
		if(!low)high++;
		//return v;
	}
};

/*
#define LITTLE_ENDIAN 1

#if LITTLE_ENDIAN
#define UNION(a, b) union { UInt16 a ## b; struct { UInt8 a; UInt8 b; }; };
#else
#define UNION(a, b) union { UInt16 a ## b; struct { UInt8 b; UInt8 a; }; };
#endif

#define REGISTERS					\
UInt16 pc;							\
UInt16 sp;							\
union {								\
	struct {						\
		UInt8 a;					\
		union {						\
			UInt8 f;				\
			struct {				\
				UInt8 unused : 4;	\
				UInt8 c_flag : 1;	\
				UInt8 h_flag : 1;	\
				UInt8 n_flag : 1;	\
				UInt8 z_flag : 1;	\
			};						\
		};							\
	};								\
	UInt16 af;						\
};									\
									\
UNION(b, c);						\
UNION(d, e);						\
UNION(h, l); 
*/






const UInt8 Z_MASK = 1 << 7;
const UInt8 N_MASK = 1 << 6;
const UInt8 H_MASK = 1 << 5;
const UInt8 C_MASK = 1 << 4;

struct Registers {

	UInt8 IME;
	Int8 interruptPending;
	bool halted;

	Reg16 af;
	Reg16 bc;
	Reg16 de;
	Reg16 hl;

	Reg16 sp;
	Reg16 pc;

	Reg8& a;
	Reg8& b;
	Reg8& c;
	Reg8& d;
	Reg8& e;
	Reg8& f;

	Reg8& h;
	Reg8& l;

	Registers() : a(af.high), f(af.low), b(bc.high), c(bc.low), d(de.high), e(de.low), h(hl.high), l(hl.low) {
		a = 0;
		f = 0;
		bc = 0;
		de = 0;
		hl = 0;
		pc = 0;
		sp = 0;

		IME = 0xFF;
		interruptPending = 0;
		halted = false;
	}

	void initGB() {
		// State after running the gameboy bootrom

		a = 0x11;
		f = 0xB0;// ZHC 1011
		b = 0x00;
		c = 0x13;
		d = 0x00;
		e = 0xD8;
		h = 0x01;
		l = 0x4D;
		sp = 0xFFFE;
		pc = 0x0100;

		/*
		af = 0x11B0;
		bc = 0x0013;
		de = 0x00D8;
		hl = 0x014D;
		sp = 0xFFFE;
		pc = 0x0100;
		*/
	}

	friend std::ostream& operator<< (std::ostream& os, Registers& reg) {
		os	<< std::setfill('0') << std::hex << std::uppercase

			<< "PC:" << std::setw(4) << (UInt16)reg.pc << " "
			<< "A:" << std::setw(2) << (UInt16)reg.a << " "
			<< "B:" << std::setw(2) << (UInt16)reg.b << " "
			<< "C:" << std::setw(2) << (UInt16)reg.c << " "
			<< "D:" << std::setw(2) << (UInt16)reg.d << " "
			<< "E:" << std::setw(2) << (UInt16)reg.e << " "
			<< "F:" << std::setw(2) << (UInt16)reg.f << " "
			<< "HL:" << std::setw(4) << (UInt16)reg.hl << " "
			<< "SP:" << std::setw(4) << (UInt16)reg.sp << " "

			<< std::dec << std::nouppercase;

		return os;
	}
};