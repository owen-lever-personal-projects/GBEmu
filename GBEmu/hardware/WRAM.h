#pragma once

#include "BUS.h"

class WorkRAM : BUSDevice {

public:

	WorkRAM(BUS& bus) : BUSDevice(bus) {
		for (UInt8& byte : ram) {
			byte = 0x00;
		}
	};

	UInt8 ram[0x2000];

	UInt8 inRange(UInt16 addr) {
		return ((addr >= 0xC000) && (addr <= 0xDFFF));
	}
	
	UInt8 read(UInt16 addr) {
		if (inRange(addr)) {
			return ram[addr - 0xC000];
		}
		return defaultReturnValue;
	}

	void write(UInt16 addr, UInt8 value) {
		if (!inRange(addr)) return;
		ram[addr - 0xC000] = value;
	}
};

/*
class CartNoMBC : CART {


public:
	UInt8 rom[0x8000];
	CartNoMBC(BUS& bus) : CART(bus) {
		this->type = NoMBC;
	}

	UInt8 inRange(UInt16 addr) {
		return (addr < 0x8000);
	}

	UInt8 read(UInt16 addr) {
		if (inRange(addr)) {
			return rom[addr];
		}
		return 0;
	}

	void write(UInt16 addr, UInt8 value) {

	}


};
*/