#pragma once

#include <iostream>
#include <fstream>


#include "BUS.h"

#include <windows.h>
#include <string>
#include <list>

typedef void(*ScanlineCallback)(int scanline);
typedef void(*FrameCallback)(int frame);

class GPUEventHandler {

	public:
		virtual void OnScanlineEvent(int scanline) = 0;
		virtual void OnFrameEvent(int frame) = 0;

};

struct InterruptVectors {
	enum : UInt16 {
		V_Blank = 0x40,
		LCD_STAT = 0x48,
		TIMER = 0x50,
		SERIAL = 0x58,
		JOYPAD = 0x60,
	};
};


struct InterruptMasks {
	enum : UInt8 {
		V_Blank = 1 << 0,
		LCD_STAT = 1 << 1,
		TIMER = 1 << 2,
		SERIAL = 1 << 3,
		JOYPAD = 1 << 4,
	};
};


struct Scanline {
	UInt8 lineNum;
	UInt8 scx, scy;
};


struct GPU : public BUSDevice {

	std::list<ScanlineCallback*> scanlineCallbacks;
	std::list<FrameCallback*> frameCallbacks;
	std::list<GPUEventHandler*> eventHandlers;

	const UInt16 LDCD = 0xFF40;
	const UInt16 LDCSTAT = 0xFF41;
	const UInt16 SCY = 0xFF42;
	const UInt16 SCX = 0xFF43;
	const UInt16 LY = 0xFF44;
	const UInt16 LYC = 0xFF45;
	const UInt16 DMA = 0xFF46;

	const UInt16 BGP = 0xFF47;
	const UInt16 OBP0 = 0xFF48;
	const UInt16 OBP1 = 0xFF49;
	
	const UInt16 WY = 0xFF4A;
	const UInt16 WX = 0xFF4B;


	UInt16 current_dot = 0;
	UInt8 current_scanline = 0;
	int frames = 0;

	long cycles = 0;

	const UInt16 DOTS_PER_LINE = 456;
	const UInt8 VBLANK_LINE = 144;
	const UInt8 LAST_LINE = 153;

	UInt8 videoRam[0x2000];
	UInt8 oam[0xA0];

	enum Mode : UInt8 {
		OAM = 2,
		DRAW = 3,
		HBLANK = 0,
		VBLANK = 1,

	};

	Mode mode;
	bool dma = false;

	GPU(BUS& bus) : BUSDevice(bus) {

	}

	void beginDMA(UInt16 addr) {
		dma = true;
		for (int i = 0; i < 0xA0; i++) {
			UInt8 data = bus.read8(addr + i);
			bus.write8(0xFE00 + i, data);
		}
		dma = false;
	}

	UInt8 inRangeVRAM(UInt16 addr) {
		return (addr >= 0x8000 && addr <= 0x9FFF);
	}

	UInt8 inRangeOAM(UInt16 addr) {
		return (addr >= 0xFE00 && addr <= 0xFE9F);
	}

	UInt8 inRange(UInt16 addr) override {
		return inRangeVRAM(addr) || inRangeOAM(addr);
	}

	UInt8 BUSDevice::read(UInt16 addr) override {
		if(inRangeVRAM(addr)) return videoRam[addr - 0x8000];
		if(inRangeOAM(addr)) return oam[addr - 0xFE00];
		return defaultReturnValue;
	}

	void BUSDevice::write(UInt16 addr, UInt8 value) override {

		if (dma && false) {
			std::cout << std::setw(4) << std::hex << std::uppercase
				<< (UInt16)addr << " : " << (UInt16)value
				<< std::nouppercase << std::dec << std::endl;
		}

		if (inRangeVRAM(addr))  videoRam[addr - 0x8000] = value;
		if (inRangeOAM(addr)) oam[addr - 0xFE00] = value;

		if (addr == DMA) {
			beginDMA(value * 0x100);
		}
	}

	void update(UInt16 cycles, UInt8 ime) {
		current_dot += cycles;
		this->cycles += cycles;
		while (current_dot >= DOTS_PER_LINE) {
			current_dot -= DOTS_PER_LINE;
			updateScanline();
		}
		setMode();
	}

	void setInterrupt(UInt8 mask) {
		UInt8 IF = bus.read8(0xFF0F);
		bus.write8(0xFF0F, IF |= mask);
	}

	void setMode() {


		if (current_scanline >= VBLANK_LINE) {
			mode = Mode::VBLANK;
		}
		else {
			if (current_dot < 80) {
				mode = Mode::OAM;
			}
			else if (current_dot < 80 + 168) {
				mode = Mode::DRAW;
			}
			else {
				mode = Mode::HBLANK;
			}
		}
	}

	void updateScanline() {
		current_scanline++;
		if (current_scanline > LAST_LINE)
			updateImage();

		// Trigger VBlank interrupt 
		if (current_scanline == VBLANK_LINE) {
			setInterrupt((UInt8)InterruptMasks::V_Blank);
		}

		// Trigger LYC Interrupt
		UInt8 lycVal = bus.read8(LYC);
		if (lycVal == current_scanline) {
			setInterrupt(InterruptMasks::LCD_STAT);
		}

		// Update LY
		bus.write8(LY, current_scanline);




		for (ScanlineCallback* callback : scanlineCallbacks) {
			(*callback)(current_scanline);
		}
		for (GPUEventHandler* handler : eventHandlers) {
			handler->OnScanlineEvent(current_scanline);
		}
	}

	void updateImage() {
		current_scanline = 0;
		frames++;

		for (FrameCallback* callback : frameCallbacks) {
			(*callback)(frames);
		}
		for (GPUEventHandler* handler : eventHandlers) {
			handler->OnFrameEvent(frames);
		}
	}
	
	void addScanlineCallback(ScanlineCallback* callback) {
		scanlineCallbacks.push_back(callback);
	}

	void removeScanlineCallback(ScanlineCallback* callback) {
		scanlineCallbacks.remove(callback);
	}

	void addFrameCallback(FrameCallback* callback) {
		frameCallbacks.push_back(callback);
	}

	void removeFrameCallback(FrameCallback* callback) {
		frameCallbacks.remove(callback);
	}

	void addEventHandler(GPUEventHandler* handler) {
		eventHandlers.push_back(handler);
	}

	void removeEventHandler(GPUEventHandler* handler) {
		eventHandlers.remove(handler);
	}

};