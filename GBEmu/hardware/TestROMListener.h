#pragma once

#include "BUS.h"
#include <iostream>

class TestListener : BUSDevice {

public:

	TestListener(BUS& bus) : BUSDevice(bus) {
	};

	char c = 0;
	UInt8 inRange(UInt16 addr) {
		return 0xFF;
		//return ((addr == 0xFF01) || (addr == 0xFF02));
	}
	
	UInt8 read(UInt16 addr) {
		return defaultReturnValue;
	}

	void write(UInt16 addr, UInt8 value) {
		if (addr == 0xFF01) c = value;

		if (addr == 0xFF02)
			if (value == 0x81)
				std::cout << c;
		
		if (addr == 0xFF46) {
			//std::cout << "Initialised DMA" << std::endl;
		}

	
	}
};
