#pragma once

#include "BUS.h"

class VRAM : BUSDevice {

public:

	VRAM(BUS& bus) : BUSDevice(bus) {
		for (UInt8& byte : vram) {
			byte = 0x00;
		}
	};

	UInt8 vram[0x2000];

	UInt8 inRange(UInt16 addr) {
		return ((addr >= 0x8000) && (addr <= 0x9FFF));
	}

	UInt8 read(UInt16 addr) {
		if (inRange(addr)) {
			return vram[addr - 0x8000];
		}
		return defaultReturnValue;
	}

	void write(UInt16 addr, UInt8 value) {
		if (!inRange(addr)) return;
		vram[addr - 0x8000] = value;
	}
};