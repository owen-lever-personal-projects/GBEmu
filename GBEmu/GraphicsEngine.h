#pragma once

#include "PixelImage.h"
#include "hardware/GPU.h"

class GraphicsEngine : GPUEventHandler{

	bool newImage;

	PixelImage image[2];
	GPU& gpu;
	BUS& bus;

	int currentBuffer = 0;

	public:
	GraphicsEngine(GPU& gpu) : gpu(gpu), bus(gpu.bus){
		image[0].init(160, 144);
		image[1].init(160, 144);
		gpu.addEventHandler(this);
		newImage = false;
	}



	void GPUEventHandler::OnScanlineEvent(int scanline) {
		UInt8 lcdc = bus.read8(0xFF40);

		UInt8 scx = bus.read8(0xFF43);
		UInt8 scy = bus.read8(0xFF42);

		UInt8 wx = bus.read8(0xFF4B)-7;
		UInt8 wy = bus.read8(0xFF4A);

		UInt8 bgp = bus.read8(0xFF47);

		Pixel pixels[4];

		for (int i = 0; i < 4; i++) {
			UInt8 shade = (bgp >> (i * 2)) & 0x03;
			shade = 3 - shade;
			shade *= 83;
			pixels[i] = Pixel(shade, shade, shade, 255);

		}

		bool windowEnable = (lcdc & (1 << 5));
		bool spriteEnable = (lcdc & (1 << 1));

		UInt8 scalineBuffer[160] = {-1};

		for (int dot = 0; dot < 160; dot++) {
			int backgroundX = (dot + scx) % 256; // Xpos of background
			int backgroundY = (scanline + scy) % 256; // Ypos of background

			int windowX = (dot + wx);
			int windowY = (dot + wy);
			
			UInt8 tileColour = 0;

			Int8 bgColour = getBackgroundColour(backgroundX, backgroundY, lcdc);
			if (bgColour != -1) tileColour = bgColour;

			if (windowEnable) {
				Int8 windowColour = getWindowColour(windowX, windowY, lcdc);
				if (windowColour != -1) tileColour = windowColour;
			}

			/*if (spriteEnable) {
				Int8 spriteColour = getSpriteColour(dot, scanline, lcdc);

				if (spriteColour != -1) tileColour = spriteColour;

			}*/

			scalineBuffer[dot] = tileColour;
		}

		for (int i = 0; i < 40; i++) {


			UInt16 baseAddress = 0xFE00 + (i * 4);

			UInt8 yPos = bus.read8(baseAddress + 0) - 16;
			UInt8 xPos = bus.read8(baseAddress + 1) - 8;
			UInt8 tileNum = bus.read8(baseAddress + 2);
			UInt8 attribs = bus.read8(baseAddress + 3);

			/*if (scanline == 0) {
				
				if (i == 0) {
					std::cout << "========================" << std::endl;

				}
				std::cout << "X : " << (UInt16)xPos << std::endl;
				std::cout << "Y : " << (UInt16)yPos << std::endl;
			}*/

			if (yPos <= scanline && (yPos + 8) > scanline) {

				bool behindBG = attribs & (1 << 7);
				bool yFlip = attribs & (1 << 6);
				bool xFlip = attribs & (1 << 5);
				bool paletteNum = attribs & (1 << 4);
				bool cgb_vramBank = attribs & (1 << 3);
				int cgb_paletteNum = attribs & 0x07;

				int spriteLine = scanline - yPos;
				if (yFlip) spriteLine = 7 - spriteLine;

				UInt8 pal = bus.read8(paletteNum ? 0xFF48 : 0xFF49);

				UInt16 tileAddr = 0x8000 + (tileNum * 16) + spriteLine*2;

				UInt8 tileMSB = bus.read8(tileAddr);
				UInt8 tileLSB = bus.read8(tileAddr + 1);

				for (int j = 0; j < 8; j++) {
					int xPixel = (xFlip) ? j : 7 - j;
					int spriteColour = 0;
					if (tileMSB & 0x80 >> xPixel) spriteColour += 2;
					if (tileLSB & 0x80 >> xPixel) spriteColour += 1;

					int trueColour = 0x03 & (pal >> spriteColour*2);

					int dot = xPos + xPixel;

					if (dot < 160 && spriteColour) {
						scalineBuffer[dot] = spriteColour;
					}
				}
			}

			
		}


		for (int i = 0; i < 160; i++) {
			image[currentBuffer].setPixel(i, scanline, pixels[scalineBuffer[i]]);
		}

	}

	Int8 getSpriteColour(int x, int y, UInt8 lcdc) {

		Int8 spriteColour = -1;

		for(int i = 0; i < 40; i++) {
			UInt16 baseAddress = 0xFE00 + (i * 4);

			UInt8 yPos = bus.read8(baseAddress + 0);
			UInt8 xPos = bus.read8(baseAddress + 1);
			UInt8 tileNum = bus.read8(baseAddress + 2);
			UInt8 attribs = bus.read8(baseAddress + 3);


			if (x == 0 && y == 0) {
				//std::cout << "X: " << UInt16(xPos) << std::endl;
				//std::cout << "Y: " << UInt16(yPos) << std::endl;
			}

			if (yPos == 0 || yPos >= 160) continue;
			if (xPos == 0 || xPos >= 168) continue;

			int relXPos = x - xPos;
			int relYPos = y - yPos;

			if (relYPos < 0 || relYPos > 8) continue;
			if (relXPos < 0 || relXPos > 8) continue;
			
			bool behindBG = attribs & (1 << 7);
			bool yFlip = attribs & (1 << 6);
			bool xFlip = attribs & (1 << 5);
			bool paletteNum = attribs & (1 << 4);
			bool cgb_vramBank = attribs & (1 << 3);
			int cgb_paletteNum = attribs & 0x07;


			UInt16 tileAddr = 0x8000 + tileNum * 16;


			UInt8 tileMSB = bus.read8(tileAddr);
			UInt8 tileLSB = bus.read8(tileAddr + 1);


			spriteColour = 0;
			if (tileMSB & 0x80 >> relXPos) spriteColour += 2;
			if (tileLSB & 0x80 >> relXPos) spriteColour += 1;





		}

		return spriteColour;
	}

	Int8 getWindowColour(int x, int y, UInt8 lcdc) {
		if (x > 255 || y > 255) return -1;
		if (x < 0 || y < 0) return -1;

		UInt16 tileMapDisplayAddress = (lcdc & (1<<6)) ? 0x9C00 : 0x9800;

		UInt16 tileNumAddr = ((int)(y / 8) * 32) + (int)(x / 8) + tileMapDisplayAddress;
		Int8 tileNum = bus.read8(tileNumAddr);

		UInt16 tileAddr = 0x9000 + tileNum * 16;

		if (tileAddr > 0x8FFF && lcdc & (1<<4)) tileAddr -= 0x1000;

		UInt8 tileY = y % 8;
		UInt8 tileX = x % 8;

		tileAddr += tileY * 2;

		UInt8 tileMSB = bus.read8(tileAddr);
		UInt8 tileLSB = bus.read8(tileAddr + 1);

		UInt8 tileColour = 0;
		if (tileMSB & 0x80 >> tileX) tileColour += 2;
		if (tileLSB & 0x80 >> tileX) tileColour += 1;

		return tileColour;
	}

	Int8 getBackgroundColour(int x, int y, UInt8 lcdc) {
		UInt16 tileMapDisplayAddress = (lcdc & (1 << 3)) ? 0x9C00 : 0x9800;

		UInt16 tileNumAddr = ((int)(y / 8) * 32) + (int)(x / 8) + tileMapDisplayAddress;
		Int8 tileNum = bus.read8(tileNumAddr);

		UInt16 tileAddr = 0x9000 + tileNum * 16;

		UInt8 pal = bus.read8(0xFF47);

		if (tileAddr > 0x8FFF && lcdc & (1<<4)) tileAddr -= 0x1000;
		
		UInt8 tileY = y % 8;
		UInt8 tileX = x % 8;

		tileAddr += tileY * 2;

		UInt8 tileMSB = bus.read8(tileAddr);
		UInt8 tileLSB = bus.read8(tileAddr + 1);

		UInt8 tileColour = 0;
		if (tileMSB & 0x80 >> tileX) tileColour += 2;
		if (tileLSB & 0x80 >> tileX) tileColour += 1;

		int trueColour = 0x03 & (pal >> (tileColour*2));

		return tileColour;
	}

	void GPUEventHandler::OnFrameEvent(int scanline) {
		newImage = true;
	}

	bool isReady() {
		return newImage;
	}

	PixelImage* getImage() {
		newImage = false;
		int oldImage = currentBuffer;
		currentBuffer++;
		currentBuffer %= 2;
		//std::cout << currentBuffer << ":" << oldImage << std::endl;
		return &image[oldImage];
	}

};