#pragma once
#include <cstdint>


typedef uint8_t UInt8;
typedef int8_t Int8;

typedef uint16_t UInt16;
typedef int16_t Int16;

typedef uint32_t UInt32;
typedef int32_t Int32;
