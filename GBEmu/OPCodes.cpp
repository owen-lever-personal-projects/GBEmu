#include "OPCodes.h"
#include "Instructions.h"

CB_OPCode cb_opcodes[256];

// 0x
inline int OPCode_00(Registers& registers, BUS& bus) {
	// NOP
	int cycles = NOP(registers);
	return cycles;
}
inline int OPCode_01(Registers& registers, BUS& bus) {
	// LD BC, d16
	UInt16 data = bus.read16(registers.pc);
	int cycles = LD_d16(registers, registers.bc, data);
	registers.pc += 2;
	return cycles;
}
inline int OPCode_02(Registers& registers, BUS& bus) {
	// LD (BC),A
	int cycles = LD_a8(registers, bus, registers.bc, registers.a);
	return 2;
}
inline int OPCode_03(Registers& registers, BUS& bus) {
	// INC BC
	int cycles = INC(registers, registers.bc);
	return cycles;
}
inline int OPCode_04(Registers& registers, BUS& bus) {
	// INC B
	int cycles = INC(registers, registers.b);
	return cycles;
}
inline int OPCode_05(Registers& registers, BUS& bus) {
	// DEC B
	int cycles = DEC(registers, registers.b);
	return cycles;
}
inline int OPCode_06(Registers& registers, BUS& bus) {
	// LD B, d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = LD_d8(registers, registers.b, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_07(Registers& registers, BUS& bus) {
	// RLCA
	int cycles = RLC(registers, registers.a);
	return cycles;
}
inline int OPCode_08(Registers& registers, BUS& bus) {
	// LD a16, SP
	UInt16 addr = bus.read16(registers.pc);
	int cycles = LD_a16(registers, bus, addr, registers.sp);
	registers.pc += 2;
	return cycles;
}
inline int OPCode_09(Registers& registers, BUS& bus) {
	// ADD HL, BC
	int cycles = ADD(registers, registers.hl, registers.bc);
	return cycles;
}
inline int OPCode_0A(Registers& registers, BUS& bus) {
	// LD A, (BC)
	UInt8 data = bus.read8(registers.bc);
	int cycles = LD_d8(registers, registers.a, data);
	return cycles;
}
inline int OPCode_0B(Registers& registers, BUS& bus) {
	// DEC BC
	int cycles = DEC(registers, registers.bc);
	return cycles;
}
inline int OPCode_0C(Registers& registers, BUS& bus) {
	// INC C
	int cycles = INC(registers, registers.c);
	return cycles;
}
inline int OPCode_0D(Registers& registers, BUS& bus) {
	//DEC C
	int cycles = DEC(registers, registers.c);
	return cycles;
}
inline int OPCode_0E(Registers& registers, BUS& bus) {
	// LD C, d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = LD_d8(registers, registers.c, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_0F(Registers& registers, BUS& bus) {
	// RRCA
	int cycles = RRC(registers, registers.a);
	return cycles;
}

//1x
inline int OPCode_10(Registers& registers, BUS& bus) {
	// STOP
	int cycles = STOP(registers);
	//registers.pc += 1;
	return cycles;
}
inline int OPCode_11(Registers& registers, BUS& bus) {
	// LD DE, d16
	UInt16 data = bus.read16(registers.pc);
	int cycles = LD_d16(registers, registers.de, data);
	registers.pc += 2;
	return cycles;
}
inline int OPCode_12(Registers& registers, BUS& bus) {
	// LD (DE), A
	int cycles = LD_a8(registers, bus, registers.de, registers.a);
	return cycles;
}
inline int OPCode_13(Registers& registers, BUS& bus) {
	// INC DE
	int cycles = INC(registers, registers.de);
	return cycles;
}
inline int OPCode_14(Registers& registers, BUS& bus) {
	// INC D
	int cycles = INC(registers, registers.d);
	return cycles;
}
inline int OPCode_15(Registers& registers, BUS& bus) {
	// DEC D
	int cycles = DEC(registers, registers.d);
	return cycles;
}
inline int OPCode_16(Registers& registers, BUS& bus) {
	// LD D, d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = LD_d8(registers, registers.d, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_17(Registers& registers, BUS& bus) {
	// RLA
	int cycles = RL(registers, registers.a);
	return cycles;
}
inline int OPCode_18(Registers& registers, BUS& bus) {
	// JR r8
	Int8 offset = bus.read8(registers.pc);
	int cycles = JR(registers, offset);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_19(Registers& registers, BUS& bus) {
	// ADD HL, DE
	int cycles = ADD(registers, registers.hl, registers.de);
	return cycles;
}
inline int OPCode_1A(Registers& registers, BUS& bus) {
	// LD A, (DE)
	UInt8 data = bus.read8(registers.de);
	int cycles = LD_d8(registers, registers.a, data);
	return cycles;
}
inline int OPCode_1B(Registers& registers, BUS& bus) {
	// DEC DE
	int cycles = DEC(registers, registers.de);
	return cycles;
}
inline int OPCode_1C(Registers& registers, BUS& bus) {
	// INC E
	int cycles = INC(registers, registers.e);
	return cycles;
}
inline int OPCode_1D(Registers& registers, BUS& bus) {
	// DEC E
	int cycles = DEC(registers, registers.e);
	return cycles;
}
inline int OPCode_1E(Registers& registers, BUS& bus) {
	// LD E, d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = LD_d8(registers, registers.e, data);
	registers.pc += 1;
	return cycles;

}
inline int OPCode_1F(Registers& registers, BUS& bus) {
	// RRA
	int cycles = RR(registers, registers.a);
	return cycles;
}

//2x
inline int OPCode_20(Registers& registers, BUS& bus) {
	// JR NZ, r8
	UInt8 data = bus.read8(registers.pc);
	int cycles = JR(registers, CC_NZ, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_21(Registers& registers, BUS& bus) {
	// LD HL, d16
	UInt16 data = bus.read16(registers.pc);
	int cycles = LD_d16(registers, registers.hl, data);
	registers.pc += 2;
	return cycles;
}
inline int OPCode_22(Registers& registers, BUS& bus) {
	// LD (HL+), A
	int cycles = LD_a8(registers, bus, registers.hl, registers.a);
	registers.hl++;
	return cycles;
}
inline int OPCode_23(Registers& registers, BUS& bus) {
	// INC HL
	int cycles = INC(registers, registers.hl);
	return cycles;
}
inline int OPCode_24(Registers& registers, BUS& bus) {
	// INC H
	int cycles = INC(registers, registers.h);
	return cycles;
}
inline int OPCode_25(Registers& registers, BUS& bus) {
	// DEC H
	int cycles = DEC(registers, registers.h);
	return cycles;
}
inline int OPCode_26(Registers& registers, BUS& bus) {
	// LD H, d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = LD_d8(registers, registers.h, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_27(Registers& registers, BUS& bus) {
	// DAA
	int cycles = DAA(registers);
	return cycles;
}
inline int OPCode_28(Registers& registers, BUS& bus) {
	// JR Z, r8
	UInt8 offset = bus.read8(registers.pc);
	int cycles = JR(registers, CC_Z, offset);
	registers.pc += 1;
	return cycles; 
}
inline int OPCode_29(Registers& registers, BUS& bus) {
	// ADD HL, HL
	int cycles = ADD(registers, registers.hl, registers.hl);
	return cycles;
}
inline int OPCode_2A(Registers& registers, BUS& bus) {
	// LD A, (HL+)
	UInt8 data = bus.read8(registers.hl);
	int cycles = LD_d8(registers, registers.a, data);
	registers.hl++;
	return cycles;
}
inline int OPCode_2B(Registers& registers, BUS& bus) {
	// DEC HL
	int cycles = DEC(registers, registers.hl);
	return cycles;
}
inline int OPCode_2C(Registers& registers, BUS& bus) {
	// INC L
	int cycles = INC(registers, registers.l);
	return cycles;
}
inline int OPCode_2D(Registers& registers, BUS& bus) {
	// DEC L
	int cycles = DEC(registers, registers.l);
	return cycles;
}
inline int OPCode_2E(Registers& registers, BUS& bus) {
	// LD L, d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = LD_d8(registers, registers.l, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_2F(Registers& registers, BUS& bus) {
	// CPL
	int cycles = CPL(registers, registers.a);
	return cycles;
}

//3x
inline int OPCode_30(Registers& registers, BUS& bus) {
	// JR NC, r8
	UInt8 data = bus.read8(registers.pc);
	int cycles = JR(registers, CC_NC, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_31(Registers& registers, BUS& bus) {
	// LD SP, d16
	UInt16 data = bus.read16(registers.pc);
	int cycles = LD_d16(registers, registers.sp, data);
	registers.pc += 2;
	return cycles;
}
inline int OPCode_32(Registers& registers, BUS& bus) {
	// LD (HL-), A
	int cycles = LD_a8(registers, bus, registers.hl, registers.a);
	registers.hl--;
	return cycles;
}
inline int OPCode_33(Registers& registers, BUS& bus) {
	// INC SP
	int cycles = INC(registers, registers.sp);
	return cycles;
}
inline int OPCode_34(Registers& registers, BUS& bus) {
	// INC (HL)
	int cycles = INC_HL(registers, bus);
	return cycles;
}
inline int OPCode_35(Registers& registers, BUS& bus) {
	// DEC (HL)
	int cycles = DEC_HL(registers, bus);
	return cycles;
}
inline int OPCode_36(Registers& registers, BUS& bus) {
	// LD (HL), d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = LDHL_D(registers, bus, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_37(Registers& registers, BUS& bus) {
	// SCF
	int cycles = SCF(registers);
	return cycles;
}
inline int OPCode_38(Registers& registers, BUS& bus) {
	// JR C, r8
	UInt8 data = bus.read8(registers.pc);
	int cycles = JR(registers, CC_C, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_39(Registers& registers, BUS& bus) {
	// ADD HL, SP
	int cycles = ADD(registers, registers.hl, registers.sp);
	return cycles;
}
inline int OPCode_3A(Registers& registers, BUS& bus) {
	// LD A, (HL-)
	UInt8 data = bus.read8(registers.hl);
	int cycles = LD_d8(registers, registers.a, data);
	registers.hl--;
	return cycles;
}
inline int OPCode_3B(Registers& registers, BUS& bus) {
	// DEC SP
	int cycles = DEC(registers, registers.sp);
	return cycles;
}
inline int OPCode_3C(Registers& registers, BUS& bus) {
	// INC A
	int cycles = INC(registers, registers.a);
	return cycles;
}
inline int OPCode_3D(Registers& registers, BUS& bus) {
	// DEC A
	int cycles = DEC(registers, registers.a);
	return cycles;
}
inline int OPCode_3E(Registers& registers, BUS& bus) {
	// LD A, d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = LD_d8(registers, registers.a, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_3F(Registers& registers, BUS& bus) {
	// CCF
	int cycles = CCF(registers);
	return cycles;
}

//4x
inline int OPCode_40(Registers& registers, BUS& bus) {
	// LD B, B
	int cycles = LD(registers, registers.b, registers.b);
	return cycles;
}
inline int OPCode_41(Registers& registers, BUS& bus) {
	// LD B, C
	int cycles = LD(registers, registers.b, registers.c);
	return cycles;
}
inline int OPCode_42(Registers& registers, BUS& bus) {
	// LD B, D
	int cycles = LD(registers, registers.b, registers.d);
	return cycles;
}
inline int OPCode_43(Registers& registers, BUS& bus) {
	// LD B, E
	int cycles = LD(registers, registers.b, registers.e);
	return cycles;
}
inline int OPCode_44(Registers& registers, BUS& bus) {
	// LD B, H
	int cycles = LD(registers, registers.b, registers.h);
	return cycles;
}
inline int OPCode_45(Registers& registers, BUS& bus) {
	// LD B, L
	int cycles = LD(registers, registers.b, registers.l);
	return cycles;
}
inline int OPCode_46(Registers& registers, BUS& bus) {
	// LD B, (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = LD_d8(registers, registers.b, data);
	return cycles;
}
inline int OPCode_47(Registers& registers, BUS& bus) {
	// LD B, A
	int cycles = LD(registers, registers.b, registers.a);
	return cycles;
}
inline int OPCode_48(Registers& registers, BUS& bus) {
	// LD C, B
	int cycles = LD(registers, registers.c, registers.b);
	return cycles;
}
inline int OPCode_49(Registers& registers, BUS& bus) {
	// LD C, C
	int cycles = LD(registers, registers.c, registers.c);
	return cycles;
}
inline int OPCode_4A(Registers& registers, BUS& bus) {
	// LD C, D
	int cycles = LD(registers, registers.c, registers.d);
	return cycles;
}
inline int OPCode_4B(Registers& registers, BUS& bus) {
	// LD C, E
	int cycles = LD(registers, registers.c, registers.e);
	return cycles;
}
inline int OPCode_4C(Registers& registers, BUS& bus) {
	// LD C, H
	int cycles = LD(registers, registers.c, registers.h);
	return cycles;
}
inline int OPCode_4D(Registers& registers, BUS& bus) {
	// LD C, L
	int cycles = LD(registers, registers.c, registers.l);
	return cycles;
}
inline int OPCode_4E(Registers& registers, BUS& bus) {
	// LD C, (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = LD_d8(registers, registers.c, data);
	return cycles;
}
inline int OPCode_4F(Registers& registers, BUS& bus) {
	// LD C, A
	int cycles = LD(registers, registers.c, registers.a);
	return cycles;
}

//5x
inline int OPCode_50(Registers& registers, BUS& bus) {
	// LD D, B
	int cycles = LD(registers, registers.d, registers.b);
	return cycles;
}
inline int OPCode_51(Registers& registers, BUS& bus) {
	// LD D, C
	int cycles = LD(registers, registers.d, registers.c);
	return cycles;
}
inline int OPCode_52(Registers& registers, BUS& bus) {
	// LD D, D
	int cycles = LD(registers, registers.d, registers.d);
	return cycles;
}
inline int OPCode_53(Registers& registers, BUS& bus) {
	// LD D, E
	int cycles = LD(registers, registers.d, registers.e);
	return cycles;
}
inline int OPCode_54(Registers& registers, BUS& bus) {
	// LD D, H
	int cycles = LD(registers, registers.d, registers.h);
	return cycles;
}
inline int OPCode_55(Registers& registers, BUS& bus) {
	// LD D, L
	int cycles = LD(registers, registers.d, registers.l);
	return cycles;
}
inline int OPCode_56(Registers& registers, BUS& bus) {
	// LD D, (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = LD_d8(registers, registers.d, data);
	return cycles;
}
inline int OPCode_57(Registers& registers, BUS& bus) {
	// LD D, A
	int cycles = LD(registers, registers.d, registers.a);
	return cycles;
}
inline int OPCode_58(Registers& registers, BUS& bus) {
	// LD E, B
	int cycles = LD(registers, registers.e, registers.b);
	return cycles;
}
inline int OPCode_59(Registers& registers, BUS& bus) {
	// LD E, C
	int cycles = LD(registers, registers.e, registers.c);
	return cycles;
}
inline int OPCode_5A(Registers& registers, BUS& bus) {
	// LD E, D
	int cycles = LD(registers, registers.e, registers.d);
	return cycles;
}
inline int OPCode_5B(Registers& registers, BUS& bus) {
	// LD E, E
	int cycles = LD(registers, registers.e, registers.e);
	return cycles;
}
inline int OPCode_5C(Registers& registers, BUS& bus) {
	// LD E, H
	int cycles = LD(registers, registers.e, registers.h);
	return cycles;
}
inline int OPCode_5D(Registers& registers, BUS& bus) {
	// LD E, L
	int cycles = LD(registers, registers.e, registers.l);
	return cycles;
}
inline int OPCode_5E(Registers& registers, BUS& bus) {
	// LD E, (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = LD_d8(registers, registers.e, data);
	return cycles;
}
inline int OPCode_5F(Registers& registers, BUS& bus) {
	// LD E, A
	int cycles = LD(registers, registers.e, registers.a);
	return cycles;
}

//6x
inline int OPCode_60(Registers& registers, BUS& bus) {
	// LD H, B
	int cycles = LD(registers, registers.h, registers.b);
	return cycles;
}
inline int OPCode_61(Registers& registers, BUS& bus) {
	// LD H, C
	int cycles = LD(registers, registers.h, registers.c);
	return cycles;
}
inline int OPCode_62(Registers& registers, BUS& bus) {
	// LD H, D
	int cycles = LD(registers, registers.h, registers.d);
	return cycles;
}
inline int OPCode_63(Registers& registers, BUS& bus) {
	// LD H, E
	int cycles = LD(registers, registers.h, registers.e);
	return cycles;
}
inline int OPCode_64(Registers& registers, BUS& bus) {
	// LD H, H
	int cycles = LD(registers, registers.h, registers.h);
	return cycles;
}
inline int OPCode_65(Registers& registers, BUS& bus) {
	// LD H, L
	int cycles = LD(registers, registers.h, registers.l);
	return cycles;
}
inline int OPCode_66(Registers& registers, BUS& bus) {
	// LD H, (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = LD_d8(registers, registers.h, data);
	return cycles;
}
inline int OPCode_67(Registers& registers, BUS& bus) {
	// LD H, A
	int cycles = LD(registers, registers.h, registers.a);
	return cycles;
}
inline int OPCode_68(Registers& registers, BUS& bus) {
	// LD L, B
	int cycles = LD(registers, registers.l, registers.b);
	return cycles;
}
inline int OPCode_69(Registers& registers, BUS& bus) {
	// LD L, C
	int cycles = LD(registers, registers.l, registers.c);
	return cycles;
}
inline int OPCode_6A(Registers& registers, BUS& bus) {
	// LD L, D
	int cycles = LD(registers, registers.l, registers.d);
	return cycles;
}
inline int OPCode_6B(Registers& registers, BUS& bus) {
	// LD L, E
	int cycles = LD(registers, registers.l, registers.e);
	return cycles;
}
inline int OPCode_6C(Registers& registers, BUS& bus) {
	// LD L, H
	int cycles = LD(registers, registers.l, registers.h);
	return cycles;
}
inline int OPCode_6D(Registers& registers, BUS& bus) {
	// LD L, L
	int cycles = LD(registers, registers.l, registers.l);
	return cycles;
}
inline int OPCode_6E(Registers& registers, BUS& bus) {
	// LD L, (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = LD_d8(registers, registers.l, data);
	return cycles;
}
inline int OPCode_6F(Registers& registers, BUS& bus) {
	// LD L, A
	int cycles = LD(registers, registers.l, registers.a);
	return cycles;
}

//7x
inline int OPCode_70(Registers& registers, BUS& bus) {
	// LD (HL),B
	int cycles = LDHL_R(registers, bus, registers.b);
	return cycles;
}
inline int OPCode_71(Registers& registers, BUS& bus) {
	// LD (HL),C
	int cycles = LDHL_R(registers, bus, registers.c);
	return cycles;
}
inline int OPCode_72(Registers& registers, BUS& bus) {
	// LD (HL),D
	int cycles = LDHL_R(registers, bus, registers.d);
	return cycles;
}
inline int OPCode_73(Registers& registers, BUS& bus) {
	// LD (HL),E
	int cycles = LDHL_R(registers, bus, registers.e);
	return cycles;
}
inline int OPCode_74(Registers& registers, BUS& bus) {
	// LD (HL),H
	int cycles = LDHL_R(registers, bus, registers.h);
	return cycles;
}
inline int OPCode_75(Registers& registers, BUS& bus) {
	// LD (HL),L
	int cycles = LDHL_R(registers, bus, registers.l);
	return cycles;
}
inline int OPCode_76(Registers& registers, BUS& bus) {
	// HALT
	int cycles = HALT(registers);
	return cycles;
}
inline int OPCode_77(Registers& registers, BUS& bus) {
	// LD (HL),A
	int cycles = LDHL_R(registers, bus, registers.a);
	return cycles;
}
inline int OPCode_78(Registers& registers, BUS& bus) {
	// LD A,B
	int cycles = LD(registers, registers.a, registers.b);
	return cycles;
}
inline int OPCode_79(Registers& registers, BUS& bus) {
	// LD A,C
	int cycles = LD(registers, registers.a, registers.c);
	return cycles;
}
inline int OPCode_7A(Registers& registers, BUS& bus) {
	// LD A,D
	int cycles = LD(registers, registers.a, registers.d);
	return cycles;
}
inline int OPCode_7B(Registers& registers, BUS& bus) {
	// LD A,E
	int cycles = LD(registers, registers.a, registers.e);
	return cycles;
}
inline int OPCode_7C(Registers& registers, BUS& bus) {
	// LD A,H
	int cycles = LD(registers, registers.a, registers.h);
	return cycles;
}
inline int OPCode_7D(Registers& registers, BUS& bus) {
	// LD A,L
	int cycles = LD(registers, registers.a, registers.l);
	return cycles;
}
inline int OPCode_7E(Registers& registers, BUS& bus) {
	// LD A, (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = LD_d8(registers, registers.a, data);
	return cycles;
}
inline int OPCode_7F(Registers& registers, BUS& bus) {
	// LD A,A
	int cycles = LD(registers, registers.a, registers.a);
	return cycles;
}

//8x
inline int OPCode_80(Registers& registers, BUS& bus) {
	// ADD A,B
	int cycles = ADD(registers, registers.a, registers.b);
	return cycles;
}
inline int OPCode_81(Registers& registers, BUS& bus) {
	// ADD A,C
	int cycles = ADD(registers, registers.a, registers.c);
	return cycles;
}
inline int OPCode_82(Registers& registers, BUS& bus) {
	// ADD A,D
	int cycles = ADD(registers, registers.a, registers.d);
	return cycles;
}
inline int OPCode_83(Registers& registers, BUS& bus) {
	//ADD A,E
	int cycles = ADD(registers, registers.a, registers.e);
	return cycles;
}
inline int OPCode_84(Registers& registers, BUS& bus) {
	// ADD A,H
	int cycles = ADD(registers, registers.a, registers.h);
	return cycles;
}
inline int OPCode_85(Registers& registers, BUS& bus) {
	// ADD A,L
	int cycles = ADD(registers, registers.a, registers.l);
	return cycles;
}
inline int OPCode_86(Registers& registers, BUS& bus) {
	// ADD A, (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = ADD_d8(registers, registers.a, data);
	return cycles;
}
inline int OPCode_87(Registers& registers, BUS& bus) {
	// ADD A,A
	int cycles = ADD(registers, registers.a, registers.a);
	return cycles;
}
inline int OPCode_88(Registers& registers, BUS& bus) {
	// ADC A,B
	int cycles = ADC(registers, registers.a, registers.b);
	return cycles;
}
inline int OPCode_89(Registers& registers, BUS& bus) {
	// ADC A,C
	int cycles = ADC(registers, registers.a, registers.c);
	return cycles;
}
inline int OPCode_8A(Registers& registers, BUS& bus) {
	// ADC A,D
	int cycles = ADC(registers, registers.a, registers.d);
	return cycles;
}
inline int OPCode_8B(Registers& registers, BUS& bus) {
	// ADC A,E
	int cycles = ADC(registers, registers.a, registers.e);
	return cycles;
}
inline int OPCode_8C(Registers& registers, BUS& bus) {
	// ADC A,H
	int cycles = ADC(registers, registers.a, registers.h);
	return cycles;
}
inline int OPCode_8D(Registers& registers, BUS& bus) {
	// ADC A,L
	int cycles = ADC(registers, registers.a, registers.l);
	return cycles;
}
inline int OPCode_8E(Registers& registers, BUS& bus) {
	// ADC A, (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = ADC_d8(registers, registers.a, data);
	return cycles;
}
inline int OPCode_8F(Registers& registers, BUS& bus) {
	// ADC A,A
	int cycles = ADC(registers, registers.a, registers.a);
	return cycles;
}

//9x
inline int OPCode_90(Registers& registers, BUS& bus) {
	// SUB B
	int cycles = SUB(registers, registers.a, registers.b);
	return cycles;
}
inline int OPCode_91(Registers& registers, BUS& bus) {
	// SUB C
	int cycles = SUB(registers, registers.a, registers.c);
	return cycles;
}
inline int OPCode_92(Registers& registers, BUS& bus) {
	// SUB D
	int cycles = SUB(registers, registers.a, registers.d);
	return cycles;
}
inline int OPCode_93(Registers& registers, BUS& bus) {
	// SUB E
	int cycles = SUB(registers, registers.a, registers.e);
	return cycles;
}
inline int OPCode_94(Registers& registers, BUS& bus) {
	// SUB H
	int cycles = SUB(registers, registers.a, registers.h);
	return cycles;
}
inline int OPCode_95(Registers& registers, BUS& bus) {
	// SUB L
	int cycles = SUB(registers, registers.a, registers.l);
	return cycles;
}
inline int OPCode_96(Registers& registers, BUS& bus) {
	// SUB (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = SUB(registers, registers.a, data);
	return cycles;
}
inline int OPCode_97(Registers& registers, BUS& bus) {
	// SUB A
	int cycles = SUB(registers, registers.a, registers.a);
	return cycles;
}
inline int OPCode_98(Registers& registers, BUS& bus) {
	// SBC A,B
	int cycles = SBC(registers, registers.a, registers.b);
	return cycles;
}
inline int OPCode_99(Registers& registers, BUS& bus) {
	// SBC A,C
	int cycles = SBC(registers, registers.a, registers.c);
	return cycles;
}
inline int OPCode_9A(Registers& registers, BUS& bus) {
	// SBC A,D
	int cycles = SBC(registers, registers.a, registers.d);
	return cycles;
}
inline int OPCode_9B(Registers& registers, BUS& bus) {
	// SBC A,E
	int cycles = SBC(registers, registers.a, registers.e);
	return cycles;
}
inline int OPCode_9C(Registers& registers, BUS& bus) {
	// SBC A,H
	int cycles = SBC(registers, registers.a, registers.h);
	return cycles;
}
inline int OPCode_9D(Registers& registers, BUS& bus) {
	// SBC A,L
	int cycles = SBC(registers, registers.a, registers.l);
	return cycles;
}
inline int OPCode_9E(Registers& registers, BUS& bus) {
	// SBC A, (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = SBC_d8(registers, registers.a, data);
	return cycles;
}
inline int OPCode_9F(Registers& registers, BUS& bus) {
	// SBC A,A
	int cycles = SBC(registers, registers.a, registers.a);
	return cycles;
}

//Ax
inline int OPCode_A0(Registers& registers, BUS& bus) {
	// AND B
	int cycles = AND(registers, registers.a, registers.b);
	return cycles;
}
inline int OPCode_A1(Registers& registers, BUS& bus) {
	// AND C
	int cycles = AND(registers, registers.a, registers.c);
	return cycles;
}
inline int OPCode_A2(Registers& registers, BUS& bus) {
	// AND D
	int cycles = AND(registers, registers.a, registers.d);
	return cycles;
}
inline int OPCode_A3(Registers& registers, BUS& bus) {
	// AND E
	int cycles = AND(registers, registers.a, registers.e);
	return cycles;
}
inline int OPCode_A4(Registers& registers, BUS& bus) {
	// AND H
	int cycles = AND(registers, registers.a, registers.h);
	return cycles;
}
inline int OPCode_A5(Registers& registers, BUS& bus) {
	// AND L
	int cycles = AND(registers, registers.a, registers.l);
	return cycles;
}
inline int OPCode_A6(Registers& registers, BUS& bus) {
	// AND (HL)
	Reg8 hl = bus.read8(registers.hl);
	AND(registers, registers.a, hl);
	return 2;
}
inline int OPCode_A7(Registers& registers, BUS& bus) {
	// AND A
	int cycles = AND(registers, registers.a, registers.a);
	return cycles;
}
inline int OPCode_A8(Registers& registers, BUS& bus) {
	// XOR B
	int cycles = XOR(registers, registers.a, registers.b);
	return cycles;
}
inline int OPCode_A9(Registers& registers, BUS& bus) {
	// XOR C
	int cycles = XOR(registers, registers.a, registers.c);
	return cycles;
}
inline int OPCode_AA(Registers& registers, BUS& bus) {
	// XOR D
	int cycles = XOR(registers, registers.a, registers.d);
	return cycles;
}
inline int OPCode_AB(Registers& registers, BUS& bus) {
	// XOR E
	int cycles = XOR(registers, registers.a, registers.e);
	return cycles;
}
inline int OPCode_AC(Registers& registers, BUS& bus) {
	// XOR H
	int cycles = XOR(registers, registers.a, registers.h);
	return cycles;
}
inline int OPCode_AD(Registers& registers, BUS& bus) {
	// XOR l
	int cycles = XOR(registers, registers.a, registers.l);
	return cycles;
}
inline int OPCode_AE(Registers& registers, BUS& bus) {
	// XOR (HL)
	UInt8 data = bus.read8(registers.hl);
	XOR(registers, registers.a, data);
	
	return 2;
}
inline int OPCode_AF(Registers& registers, BUS& bus) {
	// XOR A
	int cycles = XOR(registers, registers.a, registers.a);
	return cycles;
}

//Bx
inline int OPCode_B0(Registers& registers, BUS& bus) {
	// OR B
	int cycles = OR(registers, registers.a, registers.b);
	return cycles;
}
inline int OPCode_B1(Registers& registers, BUS& bus) {
	// OR C
	int cycles = OR(registers, registers.a, registers.c);
	return cycles;
}
inline int OPCode_B2(Registers& registers, BUS& bus) {
	// OR D
	int cycles = OR(registers, registers.a, registers.d);
	return cycles;
}
inline int OPCode_B3(Registers& registers, BUS& bus) {
	// OR E
	int cycles = OR(registers, registers.a, registers.e);
	return cycles;
}
inline int OPCode_B4(Registers& registers, BUS& bus) {
	// OR H
	int cycles = OR(registers, registers.a, registers.h);
	return cycles;
}
inline int OPCode_B5(Registers& registers, BUS& bus) {
	// OR L
	int cycles = OR(registers, registers.a, registers.l);
	return cycles;
}
inline int OPCode_B6(Registers& registers, BUS& bus) {
	// OR (HL)
	UInt8 data = bus.read8(registers.hl);
	OR(registers, registers.a, data);
	return 2;
}
inline int OPCode_B7(Registers& registers, BUS& bus) {
	// OR A
	int cycles = OR(registers, registers.a, registers.a);
	return cycles;
}
inline int OPCode_B8(Registers& registers, BUS& bus) {
	// CP B
	int cycles = CP(registers, registers.a, registers.b);
	return cycles;
}
inline int OPCode_B9(Registers& registers, BUS& bus) {
	// CP C
	int cycles = CP(registers, registers.a, registers.c);
	return cycles;
}
inline int OPCode_BA(Registers& registers, BUS& bus) {
	// CP D
	int cycles = CP(registers, registers.a, registers.d);
	return cycles;
}
inline int OPCode_BB(Registers& registers, BUS& bus) {
	// CP E
	int cycles = CP(registers, registers.a, registers.e);
	return cycles;
}
inline int OPCode_BC(Registers& registers, BUS& bus) {
	// CP H
	int cycles = CP(registers, registers.a, registers.h);
	return cycles;
}
inline int OPCode_BD(Registers& registers, BUS& bus) {
	// CP L
	int cycles = CP(registers, registers.a, registers.l);
	return cycles;
}
inline int OPCode_BE(Registers& registers, BUS& bus) {
	// CP (HL)
	UInt8 data = bus.read8(registers.hl);
	int cycles = CP(registers, registers.a, data);
	return cycles + 1;
}
inline int OPCode_BF(Registers& registers, BUS& bus) {
	// CP A
	int cycles = CP(registers, registers.a, registers.a);
	return cycles;
}

//Cx
inline int OPCode_C0(Registers& registers, BUS& bus) {
	// RET NZ
	int cycles = RET(registers, bus, CC_NZ);
	return cycles;
}
inline int OPCode_C1(Registers& registers, BUS& bus) {
	// POP BC
	int cycles = POP(registers, bus, registers.bc);
	return cycles;
}
inline int OPCode_C2(Registers& registers, BUS& bus) {
	// JP NZ , a16
	UInt16 addr = bus.read16(registers.pc);
	int cycles = JP(registers, CC_NZ, addr);
	//registers.pc += 2;
	return cycles;
}
inline int OPCode_C3(Registers& registers, BUS& bus) {
	// JP a16
	UInt16 addr = bus.read16(registers.pc);
	int cycles = JP(registers, addr);
	
	return cycles;
}
inline int OPCode_C4(Registers& registers, BUS& bus) {
	// CALL NZ,a16
	UInt16 addr = bus.read16(registers.pc);
	int cycles = CALL(registers, bus, CC_NZ, addr);
	
	return cycles;
}
inline int OPCode_C5(Registers& registers, BUS& bus) {
	// PUSH BC
	int cycles = PUSH(registers, bus, registers.bc);
	return cycles;
}
inline int OPCode_C6(Registers& registers, BUS& bus) {
	// ADD A, d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = ADD(registers, registers.a, data); // NOTE Incorrect cycle count
	registers.pc += 1;
	return cycles;
}
inline int OPCode_C7(Registers& registers, BUS& bus) {
	// RST 00H
	int cycles = RST(registers, bus, 0x00);
	return cycles;
}
inline int OPCode_C8(Registers& registers, BUS& bus) {
	// RET Z
	int cycles = RET(registers, bus, CC_Z);
	return cycles;
}
inline int OPCode_C9(Registers& registers, BUS& bus) {
	// RET
	int cycles = RET(registers, bus);
	return cycles;
}
inline int OPCode_CA(Registers& registers, BUS& bus) {
	// JP Z, a16
	UInt16 addr = bus.read16(registers.pc);
	int cycles = JP(registers, CC_Z, addr);

	return cycles;
}
inline int OPCode_CB(Registers& registers, BUS& bus) {
	
	UInt8 cb_op = bus.read8(registers.pc);
	int cycles = cb_opcodes[cb_op](registers, bus);
	if(!cycles)std::cout << "Unimplemented CB Opcode " << std::hex << (int)cb_op << std::dec << std::endl;
	registers.pc += 1;
	return cycles;
}
inline int OPCode_CC(Registers& registers, BUS& bus) {
	// CALL Z, a16
	UInt16 addr = bus.read16(registers.pc);
	int cycles = CALL(registers, bus, CC_Z, addr);
	
	return cycles;
}
#include <iostream>
inline int OPCode_CD(Registers& registers, BUS& bus) {
	// CALL a16
	UInt16 addr = bus.read16(registers.pc);
	int cycles = CALL(registers, bus, addr);
	
	return cycles;
}
inline int OPCode_CE(Registers& registers, BUS& bus) {
	// ADC A, d8
	int cycles = ADC(registers, registers.a, bus.read8(registers.pc));
	registers.pc += 1;
	return cycles;
}
inline int OPCode_CF(Registers& registers, BUS& bus) {
	// RST 08H
	int cycles = RST(registers, bus, 0x08);
	return cycles;
}

//Dx
inline int OPCode_D0(Registers& registers, BUS& bus) {
	// RET NC
	int cycles = RET(registers, bus, CC_NC);
	return cycles;
}
inline int OPCode_D1(Registers& registers, BUS& bus) {
	// POP DE
	int cycles = POP(registers, bus, registers.de);
	return cycles;
}
inline int OPCode_D2(Registers& registers, BUS& bus) {
	// JP NC , a16
	UInt16 addr = bus.read16(registers.pc);
	int cycles = JP(registers, CC_NC, addr);
	
	return cycles;
}
inline int OPCode_D3(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP
}
inline int OPCode_D4(Registers& registers, BUS& bus) {
	// CALL NC,a16
	UInt16 addr = bus.read16(registers.pc);
	int cycles = CALL(registers, bus, CC_NC, addr);
	
	return cycles;
}
inline int OPCode_D5(Registers& registers, BUS& bus) {
	// PUSH DE
	int cycles = PUSH(registers, bus, registers.de);
	return cycles;
}
inline int OPCode_D6(Registers& registers, BUS& bus) {
	// SUB d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = SUB(registers, registers.a, data);
	registers.pc += 1;
	return cycles;
}

inline int OPCode_D7(Registers& registers, BUS& bus) {
	// RST 10H
	int cycles = RST(registers, bus, 0x10);
	return cycles;
}
inline int OPCode_D8(Registers& registers, BUS& bus) {
	// RET C
	int cycles = RET(registers, bus, CC_C);
	return cycles;
}
inline int OPCode_D9(Registers& registers, BUS& bus) {
	// RETI
	int cycles = RETI(registers, bus);
	return cycles;
}
inline int OPCode_DA(Registers& registers, BUS& bus) {
	// JP C, a16
	UInt16 addr = bus.read16(registers.pc);
	int cycles = JP(registers, CC_C, addr);

	return cycles;
}
inline int OPCode_DB(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP
}
inline int OPCode_DC(Registers& registers, BUS& bus) {
	// CALL C,a16
	UInt16 addr = bus.read16(registers.pc);
	int cycles = CALL(registers, bus, CC_C, addr);
	
	return cycles;
}
inline int OPCode_DD(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP
}
inline int OPCode_DE(Registers& registers, BUS& bus) {
	// SBC A, d8
	UInt8 data = bus.read8(registers.pc);
	SBC(registers, registers.a, data);
	registers.pc += 1;
	return 2;
}
inline int OPCode_DF(Registers& registers, BUS& bus) {
	// RST 18H
	int cycles = RST(registers, bus, 0x18);
	return cycles;
}

//Ex
inline int OPCode_E0(Registers& registers, BUS& bus) {
	// LDH a8, A
	UInt16 addr = 0xFF00 + bus.read8(registers.pc);
	int cycles = LD_a8(registers, bus, addr, registers.a);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_E1(Registers& registers, BUS& bus) {
	// POP HL
	int cycles = POP(registers, bus, registers.hl);
	return cycles;
}
inline int OPCode_E2(Registers& registers, BUS& bus) {
	// LD (C), A
	int cycles = LDCA(registers, bus, registers.c, registers.a);
	return cycles;
}
inline int OPCode_E3(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP
}
inline int OPCode_E4(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP 
}
inline int OPCode_E5(Registers& registers, BUS& bus) {
	// PUSH HL
	int cycles = PUSH(registers, bus, registers.hl);
	return cycles;
}
inline int OPCode_E6(Registers& registers, BUS& bus) {
	// AND d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = AND(registers, registers.a, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_E7(Registers& registers, BUS& bus) {
	// RST 20H
	int cycles = RST(registers, bus, 0x20);
	return cycles;
}
inline int OPCode_E8(Registers& registers, BUS& bus) {
	// ADD SP, r8
	Int8 data = bus.read8(registers.pc);
	int cycles = ADD_r8(registers, registers.sp, data);
	registers.pc += 1;
	return cycles; 
}
inline int OPCode_E9(Registers& registers, BUS& bus) {
	// JP HL

	// Might be wrong
	//std::cout << "JP (HL)" << std::endl;
	int cycles = JP(registers, registers.hl);
	return cycles;
}
inline int OPCode_EA(Registers& registers, BUS& bus) {
	// LD (a16),A
	UInt16 addr = bus.read16(registers.pc);
	int cycles = LD_a8(registers, bus, addr, registers.a);
	registers.pc += 2;
	return cycles;
}
inline int OPCode_EB(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP
}
inline int OPCode_EC(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP
}
inline int OPCode_ED(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP
}
inline int OPCode_EE(Registers& registers, BUS& bus) {
	// XOR d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = XOR_d8(registers, registers.a, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_EF(Registers& registers, BUS& bus) {
	// RST 28H
	int cycles = RST(registers, bus, 0x28);
	return cycles;
}

//Fx
inline int OPCode_F0(Registers& registers, BUS& bus) {
	// LDH A, (a8)
	UInt16 addr = 0xFF00 + bus.read8(registers.pc);
	UInt8 data = bus.read8(addr);
	int cycles = LDH_A(registers, registers.a, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_F1(Registers& registers, BUS& bus) {
	// POP AF
	int cycles = POP_AF(registers, bus);
	return cycles;
}
inline int OPCode_F2(Registers& registers, BUS& bus) {
	// LD A, (C)
	UInt8 data = bus.read8(0xFF00 + registers.c);
	int cycles = LD_d8(registers, registers.a, data);
	return cycles;
}
inline int OPCode_F3(Registers& registers, BUS& bus) {
	int cycles = DI(registers);
	return cycles;
}
inline int OPCode_F4(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP
}
inline int OPCode_F5(Registers& registers, BUS& bus) {
	// PUSH AF
	int cycles = PUSH(registers, bus, registers.af);
	return cycles;
}
inline int OPCode_F6(Registers& registers, BUS& bus) {
	// OR d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = OR_d8(registers, registers.a, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_F7(Registers& registers, BUS& bus) {
	// RST 30H
	int cycles = RST(registers, bus, 0x30);
	return cycles;
}
inline int OPCode_F8(Registers& registers, BUS& bus) {
	// LD HL, SP+r8
	UInt8 r8 = bus.read8(registers.pc);
	int cycles = LDHLSP(registers, bus, r8);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_F9(Registers& registers, BUS& bus) {
	// LD SP,HL
	int cycles = LD(registers, registers.sp, registers.hl);
	return cycles;
}
inline int OPCode_FA(Registers& registers, BUS& bus) {
	// LD A, (a16)
	UInt16 addr = bus.read16(registers.pc);
	UInt8 data = bus.read16(addr);
	int cycles = LD_d8(registers, registers.a, data);
	registers.pc += 2;
	return cycles;
}
inline int OPCode_FB(Registers& registers, BUS& bus) {
	// EI
	int cycles = EI(registers);
	return cycles;
}
inline int OPCode_FC(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP
}
inline int OPCode_FD(Registers& registers, BUS& bus) {
	return 0;
	//INVALID OP
}
inline int OPCode_FE(Registers& registers, BUS& bus) {
	// CP d8
	UInt8 data = bus.read8(registers.pc);
	int cycles = CP_d8(registers, registers.a, data);
	registers.pc += 1;
	return cycles;
}
inline int OPCode_FF(Registers& registers, BUS& bus) {
	// RST 38H
	int cycles = RST(registers, bus, 0x38);
	return cycles;
}

void initOpcodes(OPCode* opcodes) {
	opcodes[0x00] = OPCode_00;
	opcodes[0x01] = OPCode_01;
	opcodes[0x02] = OPCode_02;
	opcodes[0x03] = OPCode_03;
	opcodes[0x04] = OPCode_04;
	opcodes[0x05] = OPCode_05;
	opcodes[0x06] = OPCode_06;
	opcodes[0x07] = OPCode_07;
	opcodes[0x08] = OPCode_08;
	opcodes[0x09] = OPCode_09;
	opcodes[0x0A] = OPCode_0A;
	opcodes[0x0B] = OPCode_0B;
	opcodes[0x0C] = OPCode_0C;
	opcodes[0x0D] = OPCode_0D;
	opcodes[0x0E] = OPCode_0E;
	opcodes[0x0F] = OPCode_0F;

	opcodes[0x10] = OPCode_10;
	opcodes[0x11] = OPCode_11;
	opcodes[0x12] = OPCode_12;
	opcodes[0x13] = OPCode_13;
	opcodes[0x14] = OPCode_14;
	opcodes[0x15] = OPCode_15;
	opcodes[0x16] = OPCode_16;
	opcodes[0x17] = OPCode_17;
	opcodes[0x18] = OPCode_18;
	opcodes[0x19] = OPCode_19;
	opcodes[0x1A] = OPCode_1A;
	opcodes[0x1B] = OPCode_1B;
	opcodes[0x1C] = OPCode_1C;
	opcodes[0x1D] = OPCode_1D;
	opcodes[0x1E] = OPCode_1E;
	opcodes[0x1F] = OPCode_1F;

	opcodes[0x20] = OPCode_20;
	opcodes[0x21] = OPCode_21;
	opcodes[0x22] = OPCode_22;
	opcodes[0x23] = OPCode_23;
	opcodes[0x24] = OPCode_24;
	opcodes[0x25] = OPCode_25;
	opcodes[0x26] = OPCode_26;
	opcodes[0x27] = OPCode_27;
	opcodes[0x28] = OPCode_28;
	opcodes[0x29] = OPCode_29;
	opcodes[0x2A] = OPCode_2A;
	opcodes[0x2B] = OPCode_2B;
	opcodes[0x2C] = OPCode_2C;
	opcodes[0x2D] = OPCode_2D;
	opcodes[0x2E] = OPCode_2E;
	opcodes[0x2F] = OPCode_2F;

	opcodes[0x30] = OPCode_30;
	opcodes[0x31] = OPCode_31;
	opcodes[0x32] = OPCode_32;
	opcodes[0x33] = OPCode_33;
	opcodes[0x34] = OPCode_34;
	opcodes[0x35] = OPCode_35;
	opcodes[0x36] = OPCode_36;
	opcodes[0x37] = OPCode_37;
	opcodes[0x38] = OPCode_38;
	opcodes[0x39] = OPCode_39;
	opcodes[0x3A] = OPCode_3A;
	opcodes[0x3B] = OPCode_3B;
	opcodes[0x3C] = OPCode_3C;
	opcodes[0x3D] = OPCode_3D;
	opcodes[0x3E] = OPCode_3E;
	opcodes[0x3F] = OPCode_3F;

	opcodes[0x40] = OPCode_40;
	opcodes[0x41] = OPCode_41;
	opcodes[0x42] = OPCode_42;
	opcodes[0x43] = OPCode_43;
	opcodes[0x44] = OPCode_44;
	opcodes[0x45] = OPCode_45;
	opcodes[0x46] = OPCode_46;
	opcodes[0x47] = OPCode_47;
	opcodes[0x48] = OPCode_48;
	opcodes[0x49] = OPCode_49;
	opcodes[0x4A] = OPCode_4A;
	opcodes[0x4B] = OPCode_4B;
	opcodes[0x4C] = OPCode_4C;
	opcodes[0x4D] = OPCode_4D;
	opcodes[0x4E] = OPCode_4E;
	opcodes[0x4F] = OPCode_4F;

	opcodes[0x50] = OPCode_50;
	opcodes[0x51] = OPCode_51;
	opcodes[0x52] = OPCode_52;
	opcodes[0x53] = OPCode_53;
	opcodes[0x54] = OPCode_54;
	opcodes[0x55] = OPCode_55;
	opcodes[0x56] = OPCode_56;
	opcodes[0x57] = OPCode_57;
	opcodes[0x58] = OPCode_58;
	opcodes[0x59] = OPCode_59;
	opcodes[0x5A] = OPCode_5A;
	opcodes[0x5B] = OPCode_5B;
	opcodes[0x5C] = OPCode_5C;
	opcodes[0x5D] = OPCode_5D;
	opcodes[0x5E] = OPCode_5E;
	opcodes[0x5F] = OPCode_5F;

	opcodes[0x60] = OPCode_60;
	opcodes[0x61] = OPCode_61;
	opcodes[0x62] = OPCode_62;
	opcodes[0x63] = OPCode_63;
	opcodes[0x64] = OPCode_64;
	opcodes[0x65] = OPCode_65;
	opcodes[0x66] = OPCode_66;
	opcodes[0x67] = OPCode_67;
	opcodes[0x68] = OPCode_68;
	opcodes[0x69] = OPCode_69;
	opcodes[0x6A] = OPCode_6A;
	opcodes[0x6B] = OPCode_6B;
	opcodes[0x6C] = OPCode_6C;
	opcodes[0x6D] = OPCode_6D;
	opcodes[0x6E] = OPCode_6E;
	opcodes[0x6F] = OPCode_6F;

	opcodes[0x70] = OPCode_70;
	opcodes[0x71] = OPCode_71;
	opcodes[0x72] = OPCode_72;
	opcodes[0x73] = OPCode_73;
	opcodes[0x74] = OPCode_74;
	opcodes[0x75] = OPCode_75;
	opcodes[0x76] = OPCode_76;
	opcodes[0x77] = OPCode_77;
	opcodes[0x78] = OPCode_78;
	opcodes[0x79] = OPCode_79;
	opcodes[0x7A] = OPCode_7A;
	opcodes[0x7B] = OPCode_7B;
	opcodes[0x7C] = OPCode_7C;
	opcodes[0x7D] = OPCode_7D;
	opcodes[0x7E] = OPCode_7E;
	opcodes[0x7F] = OPCode_7F;

	opcodes[0x80] = OPCode_80;
	opcodes[0x81] = OPCode_81;
	opcodes[0x82] = OPCode_82;
	opcodes[0x83] = OPCode_83;
	opcodes[0x84] = OPCode_84;
	opcodes[0x85] = OPCode_85;
	opcodes[0x86] = OPCode_86;
	opcodes[0x87] = OPCode_87;
	opcodes[0x88] = OPCode_88;
	opcodes[0x89] = OPCode_89;
	opcodes[0x8A] = OPCode_8A;
	opcodes[0x8B] = OPCode_8B;
	opcodes[0x8C] = OPCode_8C;
	opcodes[0x8D] = OPCode_8D;
	opcodes[0x8E] = OPCode_8E;
	opcodes[0x8F] = OPCode_8F;

	opcodes[0x90] = OPCode_90;
	opcodes[0x91] = OPCode_91;
	opcodes[0x92] = OPCode_92;
	opcodes[0x93] = OPCode_93;
	opcodes[0x94] = OPCode_94;
	opcodes[0x95] = OPCode_95;
	opcodes[0x96] = OPCode_96;
	opcodes[0x97] = OPCode_97;
	opcodes[0x98] = OPCode_98;
	opcodes[0x99] = OPCode_99;
	opcodes[0x9A] = OPCode_9A;
	opcodes[0x9B] = OPCode_9B;
	opcodes[0x9C] = OPCode_9C;
	opcodes[0x9D] = OPCode_9D;
	opcodes[0x9E] = OPCode_9E;
	opcodes[0x9F] = OPCode_9F;

	opcodes[0xA0] = OPCode_A0;
	opcodes[0xA1] = OPCode_A1;
	opcodes[0xA2] = OPCode_A2;
	opcodes[0xA3] = OPCode_A3;
	opcodes[0xA4] = OPCode_A4;
	opcodes[0xA5] = OPCode_A5;
	opcodes[0xA6] = OPCode_A6;
	opcodes[0xA7] = OPCode_A7;
	opcodes[0xA8] = OPCode_A8;
	opcodes[0xA9] = OPCode_A9;
	opcodes[0xAA] = OPCode_AA;
	opcodes[0xAB] = OPCode_AB;
	opcodes[0xAC] = OPCode_AC;
	opcodes[0xAD] = OPCode_AD;
	opcodes[0xAE] = OPCode_AE;
	opcodes[0xAF] = OPCode_AF;

	opcodes[0xB0] = OPCode_B0;
	opcodes[0xB1] = OPCode_B1;
	opcodes[0xB2] = OPCode_B2;
	opcodes[0xB3] = OPCode_B3;
	opcodes[0xB4] = OPCode_B4;
	opcodes[0xB5] = OPCode_B5;
	opcodes[0xB6] = OPCode_B6;
	opcodes[0xB7] = OPCode_B7;
	opcodes[0xB8] = OPCode_B8;
	opcodes[0xB9] = OPCode_B9;
	opcodes[0xBA] = OPCode_BA;
	opcodes[0xBB] = OPCode_BB;
	opcodes[0xBC] = OPCode_BC;
	opcodes[0xBD] = OPCode_BD;
	opcodes[0xBE] = OPCode_BE;
	opcodes[0xBF] = OPCode_BF;

	opcodes[0xC0] = OPCode_C0;
	opcodes[0xC1] = OPCode_C1;
	opcodes[0xC2] = OPCode_C2;
	opcodes[0xC3] = OPCode_C3;
	opcodes[0xC4] = OPCode_C4;
	opcodes[0xC5] = OPCode_C5;
	opcodes[0xC6] = OPCode_C6;
	opcodes[0xC7] = OPCode_C7;
	opcodes[0xC8] = OPCode_C8;
	opcodes[0xC9] = OPCode_C9;
	opcodes[0xCA] = OPCode_CA;
	opcodes[0xCB] = OPCode_CB;
	opcodes[0xCC] = OPCode_CC;
	opcodes[0xCD] = OPCode_CD;
	opcodes[0xCE] = OPCode_CE;
	opcodes[0xCF] = OPCode_CF;

	opcodes[0xD0] = OPCode_D0;
	opcodes[0xD1] = OPCode_D1;
	opcodes[0xD2] = OPCode_D2;
	opcodes[0xD3] = OPCode_D3;
	opcodes[0xD4] = OPCode_D4;
	opcodes[0xD5] = OPCode_D5;
	opcodes[0xD6] = OPCode_D6;
	opcodes[0xD7] = OPCode_D7;
	opcodes[0xD8] = OPCode_D8;
	opcodes[0xD9] = OPCode_D9;
	opcodes[0xDA] = OPCode_DA;
	opcodes[0xDB] = OPCode_DB;
	opcodes[0xDC] = OPCode_DC;
	opcodes[0xDD] = OPCode_DD;
	opcodes[0xDE] = OPCode_DE;
	opcodes[0xDF] = OPCode_DF;

	opcodes[0xE0] = OPCode_E0;
	opcodes[0xE1] = OPCode_E1;
	opcodes[0xE2] = OPCode_E2;
	opcodes[0xE3] = OPCode_E3;
	opcodes[0xE4] = OPCode_E4;
	opcodes[0xE5] = OPCode_E5;
	opcodes[0xE6] = OPCode_E6;
	opcodes[0xE7] = OPCode_E7;
	opcodes[0xE8] = OPCode_E8;
	opcodes[0xE9] = OPCode_E9;
	opcodes[0xEA] = OPCode_EA;
	opcodes[0xEB] = OPCode_EB;
	opcodes[0xEC] = OPCode_EC;
	opcodes[0xED] = OPCode_ED;
	opcodes[0xEE] = OPCode_EE;
	opcodes[0xEF] = OPCode_EF;

	opcodes[0xF0] = OPCode_F0;
	opcodes[0xF1] = OPCode_F1;
	opcodes[0xF2] = OPCode_F2;
	opcodes[0xF3] = OPCode_F3;
	opcodes[0xF4] = OPCode_F4;
	opcodes[0xF5] = OPCode_F5;
	opcodes[0xF6] = OPCode_F6;
	opcodes[0xF7] = OPCode_F7;
	opcodes[0xF8] = OPCode_F8;
	opcodes[0xF9] = OPCode_F9;
	opcodes[0xFA] = OPCode_FA;
	opcodes[0xFB] = OPCode_FB;
	opcodes[0xFC] = OPCode_FC;
	opcodes[0xFD] = OPCode_FD;
	opcodes[0xFE] = OPCode_FE;
	opcodes[0xFF] = OPCode_FF;

	initOpcodesCB(cb_opcodes);
}