#pragma once

#include "hardware/BUS.h"
#include "hardware/Registers.h"

#include "OPCodesCB.h"

typedef int(*OPCode)(Registers&, BUS&);

void initOpcodes(OPCode* opcodes);

enum CC : UInt8{
	CC_NZ = 0,
	CC_Z = 1,
	CC_NC = 2,
	CC_C = 3,
};

// 0x
int OPCode_00(Registers& registers, BUS& bus);
int Owqe34PCode_01(Registers& registers, BUS& bus);
int OPCode_01(Registers& registers, BUS& bus);
int OPCode_02(Registers& registers, BUS& bus);
int OPCode_03(Registers& registers, BUS& bus);
int OPCode_04(Registers& registers, BUS& bus);
int OPCode_05(Registers& registers, BUS& bus);
int OPCode_06(Registers& registers, BUS& bus);
int OPCode_07(Registers& registers, BUS& bus);
int OPCode_08(Registers& registers, BUS& bus);
int OPCode_09(Registers& registers, BUS& bus);
int OPCode_0A(Registers& registers, BUS& bus);
int OPCode_0B(Registers& registers, BUS& bus);
int OPCode_0C(Registers& registers, BUS& bus);
int OPCode_0D(Registers& registers, BUS& bus);
int OPCode_0E(Registers& registers, BUS& bus);
int OPCode_0F(Registers& registers, BUS& bus);

//1x
int OPCode_10(Registers& registers, BUS& bus);
int OPCode_11(Registers& registers, BUS& bus);
int OPCode_12(Registers& registers, BUS& bus);
int OPCode_13(Registers& registers, BUS& bus);
int OPCode_14(Registers& registers, BUS& bus);
int OPCode_15(Registers& registers, BUS& bus);
int OPCode_16(Registers& registers, BUS& bus);
int OPCode_17(Registers& registers, BUS& bus);
int OPCode_18(Registers& registers, BUS& bus);
int OPCode_19(Registers& registers, BUS& bus);
int OPCode_1A(Registers& registers, BUS& bus);
int OPCode_1B(Registers& registers, BUS& bus);
int OPCode_1C(Registers& registers, BUS& bus);
int OPCode_1D(Registers& registers, BUS& bus);
int OPCode_1E(Registers& registers, BUS& bus);
int OPCode_1F(Registers& registers, BUS& bus);

//2x
int OPCode_20(Registers& registers, BUS& bus);
int OPCode_21(Registers& registers, BUS& bus);
int OPCode_22(Registers& registers, BUS& bus);
int OPCode_23(Registers& registers, BUS& bus);
int OPCode_24(Registers& registers, BUS& bus);
int OPCode_25(Registers& registers, BUS& bus);
int OPCode_26(Registers& registers, BUS& bus);
int OPCode_27(Registers& registers, BUS& bus);
int OPCode_28(Registers& registers, BUS& bus);
int OPCode_29(Registers& registers, BUS& bus);
int OPCode_2A(Registers& registers, BUS& bus);
int OPCode_2B(Registers& registers, BUS& bus);
int OPCode_2C(Registers& registers, BUS& bus);
int OPCode_2D(Registers& registers, BUS& bus);
int OPCode_2E(Registers& registers, BUS& bus);
int OPCode_2F(Registers& registers, BUS& bus);

//3x
int OPCode_30(Registers& registers, BUS& bus);
int OPCode_31(Registers& registers, BUS& bus);
int OPCode_32(Registers& registers, BUS& bus);
int OPCode_33(Registers& registers, BUS& bus);
int OPCode_34(Registers& registers, BUS& bus);
int OPCode_35(Registers& registers, BUS& bus);
int OPCode_36(Registers& registers, BUS& bus);
int OPCode_37(Registers& registers, BUS& bus);
int OPCode_38(Registers& registers, BUS& bus);
int OPCode_39(Registers& registers, BUS& bus);
int OPCode_3A(Registers& registers, BUS& bus);
int OPCode_3B(Registers& registers, BUS& bus);
int OPCode_3C(Registers& registers, BUS& bus);
int OPCode_3D(Registers& registers, BUS& bus);
int OPCode_3E(Registers& registers, BUS& bus);
int OPCode_3F(Registers& registers, BUS& bus);

//4x
int OPCode_40(Registers& registers, BUS& bus);
int OPCode_41(Registers& registers, BUS& bus);
int OPCode_42(Registers& registers, BUS& bus);
int OPCode_43(Registers& registers, BUS& bus);
int OPCode_44(Registers& registers, BUS& bus);
int OPCode_45(Registers& registers, BUS& bus);
int OPCode_46(Registers& registers, BUS& bus);
int OPCode_47(Registers& registers, BUS& bus);
int OPCode_48(Registers& registers, BUS& bus);
int OPCode_49(Registers& registers, BUS& bus);
int OPCode_4A(Registers& registers, BUS& bus);
int OPCode_4B(Registers& registers, BUS& bus);
int OPCode_4C(Registers& registers, BUS& bus);
int OPCode_4D(Registers& registers, BUS& bus);
int OPCode_4E(Registers& registers, BUS& bus);
int OPCode_4F(Registers& registers, BUS& bus);

//5x
int OPCode_50(Registers& registers, BUS& bus);
int OPCode_51(Registers& registers, BUS& bus);
int OPCode_52(Registers& registers, BUS& bus);
int OPCode_53(Registers& registers, BUS& bus);
int OPCode_54(Registers& registers, BUS& bus);
int OPCode_55(Registers& registers, BUS& bus);
int OPCode_56(Registers& registers, BUS& bus);
int OPCode_57(Registers& registers, BUS& bus);
int OPCode_58(Registers& registers, BUS& bus);
int OPCode_59(Registers& registers, BUS& bus);
int OPCode_5A(Registers& registers, BUS& bus);
int OPCode_5B(Registers& registers, BUS& bus);
int OPCode_5C(Registers& registers, BUS& bus);
int OPCode_5D(Registers& registers, BUS& bus);
int OPCode_5E(Registers& registers, BUS& bus);
int OPCode_5F(Registers& registers, BUS& bus);

//6x
int OPCode_60(Registers& registers, BUS& bus);
int OPCode_61(Registers& registers, BUS& bus);
int OPCode_62(Registers& registers, BUS& bus);
int OPCode_63(Registers& registers, BUS& bus);
int OPCode_64(Registers& registers, BUS& bus);
int OPCode_65(Registers& registers, BUS& bus);
int OPCode_66(Registers& registers, BUS& bus);
int OPCode_67(Registers& registers, BUS& bus);
int OPCode_68(Registers& registers, BUS& bus);
int OPCode_69(Registers& registers, BUS& bus);
int OPCode_6A(Registers& registers, BUS& bus);
int OPCode_6B(Registers& registers, BUS& bus);
int OPCode_6C(Registers& registers, BUS& bus);
int OPCode_6D(Registers& registers, BUS& bus);
int OPCode_6E(Registers& registers, BUS& bus);
int OPCode_6F(Registers& registers, BUS& bus);

//7x
int OPCode_70(Registers& registers, BUS& bus);
int OPCode_71(Registers& registers, BUS& bus);
int OPCode_72(Registers& registers, BUS& bus);
int OPCode_73(Registers& registers, BUS& bus);
int OPCode_74(Registers& registers, BUS& bus);
int OPCode_75(Registers& registers, BUS& bus);
int OPCode_76(Registers& registers, BUS& bus);
int OPCode_77(Registers& registers, BUS& bus);
int OPCode_78(Registers& registers, BUS& bus);
int OPCode_79(Registers& registers, BUS& bus);
int OPCode_7A(Registers& registers, BUS& bus);
int OPCode_7B(Registers& registers, BUS& bus);
int OPCode_7C(Registers& registers, BUS& bus);
int OPCode_7D(Registers& registers, BUS& bus);
int OPCode_7E(Registers& registers, BUS& bus);
int OPCode_7F(Registers& registers, BUS& bus);

//8x
int OPCode_80(Registers& registers, BUS& bus);
int OPCode_81(Registers& registers, BUS& bus);
int OPCode_82(Registers& registers, BUS& bus);
int OPCode_83(Registers& registers, BUS& bus);
int OPCode_84(Registers& registers, BUS& bus);
int OPCode_85(Registers& registers, BUS& bus);
int OPCode_86(Registers& registers, BUS& bus);
int OPCode_87(Registers& registers, BUS& bus);
int OPCode_88(Registers& registers, BUS& bus);
int OPCode_89(Registers& registers, BUS& bus);
int OPCode_8A(Registers& registers, BUS& bus);
int OPCode_8B(Registers& registers, BUS& bus);
int OPCode_8C(Registers& registers, BUS& bus);
int OPCode_8D(Registers& registers, BUS& bus);
int OPCode_8E(Registers& registers, BUS& bus);
int OPCode_8F(Registers& registers, BUS& bus);

//9x
int OPCode_90(Registers& registers, BUS& bus);
int OPCode_91(Registers& registers, BUS& bus);
int OPCode_92(Registers& registers, BUS& bus);
int OPCode_93(Registers& registers, BUS& bus);
int OPCode_94(Registers& registers, BUS& bus);
int OPCode_95(Registers& registers, BUS& bus);
int OPCode_96(Registers& registers, BUS& bus);
int OPCode_97(Registers& registers, BUS& bus);
int OPCode_98(Registers& registers, BUS& bus);
int OPCode_99(Registers& registers, BUS& bus);
int OPCode_9A(Registers& registers, BUS& bus);
int OPCode_9B(Registers& registers, BUS& bus);
int OPCode_9C(Registers& registers, BUS& bus);
int OPCode_9D(Registers& registers, BUS& bus);
int OPCode_9E(Registers& registers, BUS& bus);
int OPCode_9F(Registers& registers, BUS& bus);

//Ax
int OPCode_A0(Registers& registers, BUS& bus);
int OPCode_A1(Registers& registers, BUS& bus);
int OPCode_A2(Registers& registers, BUS& bus);
int OPCode_A3(Registers& registers, BUS& bus);
int OPCode_A4(Registers& registers, BUS& bus);
int OPCode_A5(Registers& registers, BUS& bus);
int OPCode_A6(Registers& registers, BUS& bus);
int OPCode_A7(Registers& registers, BUS& bus);
int OPCode_A8(Registers& registers, BUS& bus);
int OPCode_A9(Registers& registers, BUS& bus);
int OPCode_AA(Registers& registers, BUS& bus);
int OPCode_AB(Registers& registers, BUS& bus);
int OPCode_AC(Registers& registers, BUS& bus);
int OPCode_AD(Registers& registers, BUS& bus);
int OPCode_AE(Registers& registers, BUS& bus);
int OPCode_AF(Registers& registers, BUS& bus);

//Bx
int OPCode_B0(Registers& registers, BUS& bus);
int OPCode_B1(Registers& registers, BUS& bus);
int OPCode_B2(Registers& registers, BUS& bus);
int OPCode_B3(Registers& registers, BUS& bus);
int OPCode_B4(Registers& registers, BUS& bus);
int OPCode_B5(Registers& registers, BUS& bus);
int OPCode_B6(Registers& registers, BUS& bus);
int OPCode_B7(Registers& registers, BUS& bus);
int OPCode_B8(Registers& registers, BUS& bus);
int OPCode_B9(Registers& registers, BUS& bus);
int OPCode_BA(Registers& registers, BUS& bus);
int OPCode_BB(Registers& registers, BUS& bus);
int OPCode_BC(Registers& registers, BUS& bus);
int OPCode_BD(Registers& registers, BUS& bus);
int OPCode_BE(Registers& registers, BUS& bus);
int OPCode_BF(Registers& registers, BUS& bus);

//Cx
int OPCode_C0(Registers& registers, BUS& bus);
int OPCode_C1(Registers& registers, BUS& bus);
int OPCode_C2(Registers& registers, BUS& bus);
int OPCode_C3(Registers& registers, BUS& bus);
int OPCode_C4(Registers& registers, BUS& bus);
int OPCode_C5(Registers& registers, BUS& bus);
int OPCode_C6(Registers& registers, BUS& bus);
int OPCode_C7(Registers& registers, BUS& bus);
int OPCode_C8(Registers& registers, BUS& bus);
int OPCode_C9(Registers& registers, BUS& bus);
int OPCode_CA(Registers& registers, BUS& bus);
int OPCode_CB(Registers& registers, BUS& bus);
int OPCode_CC(Registers& registers, BUS& bus);
int OPCode_CD(Registers& registers, BUS& bus);
int OPCode_CE(Registers& registers, BUS& bus);
int OPCode_CF(Registers& registers, BUS& bus);

//Dx
int OPCode_D0(Registers& registers, BUS& bus);
int OPCode_D1(Registers& registers, BUS& bus);
int OPCode_D2(Registers& registers, BUS& bus);
int OPCode_D3(Registers& registers, BUS& bus);
int OPCode_D4(Registers& registers, BUS& bus);
int OPCode_D5(Registers& registers, BUS& bus);
int OPCode_D6(Registers& registers, BUS& bus);
int OPCode_D7(Registers& registers, BUS& bus);
int OPCode_D8(Registers& registers, BUS& bus);
int OPCode_D9(Registers& registers, BUS& bus);
int OPCode_DA(Registers& registers, BUS& bus);
int OPCode_DB(Registers& registers, BUS& bus);
int OPCode_DC(Registers& registers, BUS& bus);
int OPCode_DD(Registers& registers, BUS& bus);
int OPCode_DE(Registers& registers, BUS& bus);
int OPCode_DF(Registers& registers, BUS& bus);

//Ex
int OPCode_E0(Registers& registers, BUS& bus);
int OPCode_E1(Registers& registers, BUS& bus);
int OPCode_E2(Registers& registers, BUS& bus);
int OPCode_E3(Registers& registers, BUS& bus);
int OPCode_E4(Registers& registers, BUS& bus);
int OPCode_E5(Registers& registers, BUS& bus);
int OPCode_E6(Registers& registers, BUS& bus);
int OPCode_E7(Registers& registers, BUS& bus);
int OPCode_E8(Registers& registers, BUS& bus);
int OPCode_E9(Registers& registers, BUS& bus);
int OPCode_EA(Registers& registers, BUS& bus);
int OPCode_EB(Registers& registers, BUS& bus);
int OPCode_EC(Registers& registers, BUS& bus);
int OPCode_ED(Registers& registers, BUS& bus);
int OPCode_EE(Registers& registers, BUS& bus);
int OPCode_EF(Registers& registers, BUS& bus);

//Fx
int OPCode_F0(Registers& registers, BUS& bus);
int OPCode_F1(Registers& registers, BUS& bus);
int OPCode_F2(Registers& registers, BUS& bus);
int OPCode_F3(Registers& registers, BUS& bus);
int OPCode_F4(Registers& registers, BUS& bus);
int OPCode_F5(Registers& registers, BUS& bus);
int OPCode_F6(Registers& registers, BUS& bus);
int OPCode_F7(Registers& registers, BUS& bus);
int OPCode_F8(Registers& registers, BUS& bus);
int OPCode_F9(Registers& registers, BUS& bus);
int OPCode_FA(Registers& registers, BUS& bus);
int OPCode_FB(Registers& registers, BUS& bus);
int OPCode_FC(Registers& registers, BUS& bus);
int OPCode_FD(Registers& registers, BUS& bus);
int OPCode_FE(Registers& registers, BUS& bus);
int OPCode_FF(Registers& registers, BUS& bus);