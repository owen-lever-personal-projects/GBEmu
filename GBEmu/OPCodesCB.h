#pragma once

#include "hardware/BUS.h"
#include "hardware/Registers.h"

typedef int(*CB_OPCode)(Registers&, BUS&);

void initOpcodesCB(CB_OPCode* opcodes);

// 0x
int CB_00(Registers& registers, BUS& bus);
int CB_01(Registers& registers, BUS& bus);
int CB_01(Registers& registers, BUS& bus);
int CB_02(Registers& registers, BUS& bus);
int CB_03(Registers& registers, BUS& bus);
int CB_04(Registers& registers, BUS& bus);
int CB_05(Registers& registers, BUS& bus);
int CB_06(Registers& registers, BUS& bus);
int CB_07(Registers& registers, BUS& bus);
int CB_08(Registers& registers, BUS& bus);
int CB_09(Registers& registers, BUS& bus);
int CB_0A(Registers& registers, BUS& bus);
int CB_0B(Registers& registers, BUS& bus);
int CB_0C(Registers& registers, BUS& bus);
int CB_0D(Registers& registers, BUS& bus);
int CB_0E(Registers& registers, BUS& bus);
int CB_0F(Registers& registers, BUS& bus);

//1x
int CB_10(Registers& registers, BUS& bus);
int CB_11(Registers& registers, BUS& bus);
int CB_12(Registers& registers, BUS& bus);
int CB_13(Registers& registers, BUS& bus);
int CB_14(Registers& registers, BUS& bus);
int CB_15(Registers& registers, BUS& bus);
int CB_16(Registers& registers, BUS& bus);
int CB_17(Registers& registers, BUS& bus);
int CB_18(Registers& registers, BUS& bus);
int CB_19(Registers& registers, BUS& bus);
int CB_1A(Registers& registers, BUS& bus);
int CB_1B(Registers& registers, BUS& bus);
int CB_1C(Registers& registers, BUS& bus);
int CB_1D(Registers& registers, BUS& bus);
int CB_1E(Registers& registers, BUS& bus);
int CB_1F(Registers& registers, BUS& bus);

//2x
int CB_20(Registers& registers, BUS& bus);
int CB_21(Registers& registers, BUS& bus);
int CB_22(Registers& registers, BUS& bus);
int CB_23(Registers& registers, BUS& bus);
int CB_24(Registers& registers, BUS& bus);
int CB_25(Registers& registers, BUS& bus);
int CB_26(Registers& registers, BUS& bus);
int CB_27(Registers& registers, BUS& bus);
int CB_28(Registers& registers, BUS& bus);
int CB_29(Registers& registers, BUS& bus);
int CB_2A(Registers& registers, BUS& bus);
int CB_2B(Registers& registers, BUS& bus);
int CB_2C(Registers& registers, BUS& bus);
int CB_2D(Registers& registers, BUS& bus);
int CB_2E(Registers& registers, BUS& bus);
int CB_2F(Registers& registers, BUS& bus);

//3x
int CB_30(Registers& registers, BUS& bus);
int CB_31(Registers& registers, BUS& bus);
int CB_32(Registers& registers, BUS& bus);
int CB_33(Registers& registers, BUS& bus);
int CB_34(Registers& registers, BUS& bus);
int CB_35(Registers& registers, BUS& bus);
int CB_36(Registers& registers, BUS& bus);
int CB_37(Registers& registers, BUS& bus);
int CB_38(Registers& registers, BUS& bus);
int CB_39(Registers& registers, BUS& bus);
int CB_3A(Registers& registers, BUS& bus);
int CB_3B(Registers& registers, BUS& bus);
int CB_3C(Registers& registers, BUS& bus);
int CB_3D(Registers& registers, BUS& bus);
int CB_3E(Registers& registers, BUS& bus);
int CB_3F(Registers& registers, BUS& bus);

//4x
int CB_40(Registers& registers, BUS& bus);
int CB_41(Registers& registers, BUS& bus);
int CB_42(Registers& registers, BUS& bus);
int CB_43(Registers& registers, BUS& bus);
int CB_44(Registers& registers, BUS& bus);
int CB_45(Registers& registers, BUS& bus);
int CB_46(Registers& registers, BUS& bus);
int CB_47(Registers& registers, BUS& bus);
int CB_48(Registers& registers, BUS& bus);
int CB_49(Registers& registers, BUS& bus);
int CB_4A(Registers& registers, BUS& bus);
int CB_4B(Registers& registers, BUS& bus);
int CB_4C(Registers& registers, BUS& bus);
int CB_4D(Registers& registers, BUS& bus);
int CB_4E(Registers& registers, BUS& bus);
int CB_4F(Registers& registers, BUS& bus);

//5x
int CB_50(Registers& registers, BUS& bus);
int CB_51(Registers& registers, BUS& bus);
int CB_52(Registers& registers, BUS& bus);
int CB_53(Registers& registers, BUS& bus);
int CB_54(Registers& registers, BUS& bus);
int CB_55(Registers& registers, BUS& bus);
int CB_56(Registers& registers, BUS& bus);
int CB_57(Registers& registers, BUS& bus);
int CB_58(Registers& registers, BUS& bus);
int CB_59(Registers& registers, BUS& bus);
int CB_5A(Registers& registers, BUS& bus);
int CB_5B(Registers& registers, BUS& bus);
int CB_5C(Registers& registers, BUS& bus);
int CB_5D(Registers& registers, BUS& bus);
int CB_5E(Registers& registers, BUS& bus);
int CB_5F(Registers& registers, BUS& bus);

//6x
int CB_60(Registers& registers, BUS& bus);
int CB_61(Registers& registers, BUS& bus);
int CB_62(Registers& registers, BUS& bus);
int CB_63(Registers& registers, BUS& bus);
int CB_64(Registers& registers, BUS& bus);
int CB_65(Registers& registers, BUS& bus);
int CB_66(Registers& registers, BUS& bus);
int CB_67(Registers& registers, BUS& bus);
int CB_68(Registers& registers, BUS& bus);
int CB_69(Registers& registers, BUS& bus);
int CB_6A(Registers& registers, BUS& bus);
int CB_6B(Registers& registers, BUS& bus);
int CB_6C(Registers& registers, BUS& bus);
int CB_6D(Registers& registers, BUS& bus);
int CB_6E(Registers& registers, BUS& bus);
int CB_6F(Registers& registers, BUS& bus);

//7x
int CB_70(Registers& registers, BUS& bus);
int CB_71(Registers& registers, BUS& bus);
int CB_72(Registers& registers, BUS& bus);
int CB_73(Registers& registers, BUS& bus);
int CB_74(Registers& registers, BUS& bus);
int CB_75(Registers& registers, BUS& bus);
int CB_76(Registers& registers, BUS& bus);
int CB_77(Registers& registers, BUS& bus);
int CB_78(Registers& registers, BUS& bus);
int CB_79(Registers& registers, BUS& bus);
int CB_7A(Registers& registers, BUS& bus);
int CB_7B(Registers& registers, BUS& bus);
int CB_7C(Registers& registers, BUS& bus);
int CB_7D(Registers& registers, BUS& bus);
int CB_7E(Registers& registers, BUS& bus);
int CB_7F(Registers& registers, BUS& bus);

//8x
int CB_80(Registers& registers, BUS& bus);
int CB_81(Registers& registers, BUS& bus);
int CB_82(Registers& registers, BUS& bus);
int CB_83(Registers& registers, BUS& bus);
int CB_84(Registers& registers, BUS& bus);
int CB_85(Registers& registers, BUS& bus);
int CB_86(Registers& registers, BUS& bus);
int CB_87(Registers& registers, BUS& bus);
int CB_88(Registers& registers, BUS& bus);
int CB_89(Registers& registers, BUS& bus);
int CB_8A(Registers& registers, BUS& bus);
int CB_8B(Registers& registers, BUS& bus);
int CB_8C(Registers& registers, BUS& bus);
int CB_8D(Registers& registers, BUS& bus);
int CB_8E(Registers& registers, BUS& bus);
int CB_8F(Registers& registers, BUS& bus);

//9x
int CB_90(Registers& registers, BUS& bus);
int CB_91(Registers& registers, BUS& bus);
int CB_92(Registers& registers, BUS& bus);
int CB_93(Registers& registers, BUS& bus);
int CB_94(Registers& registers, BUS& bus);
int CB_95(Registers& registers, BUS& bus);
int CB_96(Registers& registers, BUS& bus);
int CB_97(Registers& registers, BUS& bus);
int CB_98(Registers& registers, BUS& bus);
int CB_99(Registers& registers, BUS& bus);
int CB_9A(Registers& registers, BUS& bus);
int CB_9B(Registers& registers, BUS& bus);
int CB_9C(Registers& registers, BUS& bus);
int CB_9D(Registers& registers, BUS& bus);
int CB_9E(Registers& registers, BUS& bus);
int CB_9F(Registers& registers, BUS& bus);

//Ax
int CB_A0(Registers& registers, BUS& bus);
int CB_A1(Registers& registers, BUS& bus);
int CB_A2(Registers& registers, BUS& bus);
int CB_A3(Registers& registers, BUS& bus);
int CB_A4(Registers& registers, BUS& bus);
int CB_A5(Registers& registers, BUS& bus);
int CB_A6(Registers& registers, BUS& bus);
int CB_A7(Registers& registers, BUS& bus);
int CB_A8(Registers& registers, BUS& bus);
int CB_A9(Registers& registers, BUS& bus);
int CB_AA(Registers& registers, BUS& bus);
int CB_AB(Registers& registers, BUS& bus);
int CB_AC(Registers& registers, BUS& bus);
int CB_AD(Registers& registers, BUS& bus);
int CB_AE(Registers& registers, BUS& bus);
int CB_AF(Registers& registers, BUS& bus);

//Bx
int CB_B0(Registers& registers, BUS& bus);
int CB_B1(Registers& registers, BUS& bus);
int CB_B2(Registers& registers, BUS& bus);
int CB_B3(Registers& registers, BUS& bus);
int CB_B4(Registers& registers, BUS& bus);
int CB_B5(Registers& registers, BUS& bus);
int CB_B6(Registers& registers, BUS& bus);
int CB_B7(Registers& registers, BUS& bus);
int CB_B8(Registers& registers, BUS& bus);
int CB_B9(Registers& registers, BUS& bus);
int CB_BA(Registers& registers, BUS& bus);
int CB_BB(Registers& registers, BUS& bus);
int CB_BC(Registers& registers, BUS& bus);
int CB_BD(Registers& registers, BUS& bus);
int CB_BE(Registers& registers, BUS& bus);
int CB_BF(Registers& registers, BUS& bus);

//Cx
int CB_C0(Registers& registers, BUS& bus);
int CB_C1(Registers& registers, BUS& bus);
int CB_C2(Registers& registers, BUS& bus);
int CB_C3(Registers& registers, BUS& bus);
int CB_C4(Registers& registers, BUS& bus);
int CB_C5(Registers& registers, BUS& bus);
int CB_C6(Registers& registers, BUS& bus);
int CB_C7(Registers& registers, BUS& bus);
int CB_C8(Registers& registers, BUS& bus);
int CB_C9(Registers& registers, BUS& bus);
int CB_CA(Registers& registers, BUS& bus);
int CB_CB(Registers& registers, BUS& bus);
int CB_CC(Registers& registers, BUS& bus);
int CB_CD(Registers& registers, BUS& bus);
int CB_CE(Registers& registers, BUS& bus);
int CB_CF(Registers& registers, BUS& bus);

//Dx
int CB_D0(Registers& registers, BUS& bus);
int CB_D1(Registers& registers, BUS& bus);
int CB_D2(Registers& registers, BUS& bus);
int CB_D3(Registers& registers, BUS& bus);
int CB_D4(Registers& registers, BUS& bus);
int CB_D5(Registers& registers, BUS& bus);
int CB_D6(Registers& registers, BUS& bus);
int CB_D7(Registers& registers, BUS& bus);
int CB_D8(Registers& registers, BUS& bus);
int CB_D9(Registers& registers, BUS& bus);
int CB_DA(Registers& registers, BUS& bus);
int CB_DB(Registers& registers, BUS& bus);
int CB_DC(Registers& registers, BUS& bus);
int CB_DD(Registers& registers, BUS& bus);
int CB_DE(Registers& registers, BUS& bus);
int CB_DF(Registers& registers, BUS& bus);

//Ex
int CB_E0(Registers& registers, BUS& bus);
int CB_E1(Registers& registers, BUS& bus);
int CB_E2(Registers& registers, BUS& bus);
int CB_E3(Registers& registers, BUS& bus);
int CB_E4(Registers& registers, BUS& bus);
int CB_E5(Registers& registers, BUS& bus);
int CB_E6(Registers& registers, BUS& bus);
int CB_E7(Registers& registers, BUS& bus);
int CB_E8(Registers& registers, BUS& bus);
int CB_E9(Registers& registers, BUS& bus);
int CB_EA(Registers& registers, BUS& bus);
int CB_EB(Registers& registers, BUS& bus);
int CB_EC(Registers& registers, BUS& bus);
int CB_ED(Registers& registers, BUS& bus);
int CB_EE(Registers& registers, BUS& bus);
int CB_EF(Registers& registers, BUS& bus);

//Fx
int CB_F0(Registers& registers, BUS& bus);
int CB_F1(Registers& registers, BUS& bus);
int CB_F2(Registers& registers, BUS& bus);
int CB_F3(Registers& registers, BUS& bus);
int CB_F4(Registers& registers, BUS& bus);
int CB_F5(Registers& registers, BUS& bus);
int CB_F6(Registers& registers, BUS& bus);
int CB_F7(Registers& registers, BUS& bus);
int CB_F8(Registers& registers, BUS& bus);
int CB_F9(Registers& registers, BUS& bus);
int CB_FA(Registers& registers, BUS& bus);
int CB_FB(Registers& registers, BUS& bus);
int CB_FC(Registers& registers, BUS& bus);
int CB_FD(Registers& registers, BUS& bus);
int CB_FE(Registers& registers, BUS& bus);
int CB_FF(Registers& registers, BUS& bus);