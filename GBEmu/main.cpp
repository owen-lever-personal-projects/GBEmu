#include <iostream>

#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"
#include "PixelImage.h"

#include "hardware/CPU.h"
#include "hardware/BUS.h"
#include "hardware/CART.h"
#include "hardware/TestROMListener.h"
#include "hardware/WRAM.h"
#include "hardware/HRAM.h"
#include "hardware/VRAM.h"
#include "hardware/IORegisters.h"
#include "hardware/TIMER.h"
#include "CartLoader.h"
#include <memory>

#include <fstream>

#include <windows.h>
#include "GraphicsEngine.h"

int main() {

	bool log = false;

	SetConsoleTitle("GameBoy Emu");

	JoypadState joypad;

	BUS bus;
	GPU gpu(bus);
	Timer timer(bus);
	CPU cpu(bus, gpu, timer, log);
	//CartNoMBC cart(bus);
	WorkRAM ram(bus);
	HighRAM hram(bus);
	//VRAM vram(bus);
	IORegisters io(bus, joypad);

	GraphicsEngine engine(gpu);

	TestListener listener(bus);
	const char* rom = nullptr;

	
	//rom = "roms/gb-test-roms-master/cpu_instrs/individual/01-special.gb";
	rom = "roms/gb-test-roms-master/cpu_instrs/individual/02-interrupts.gb";
	//rom = "roms/gb-test-roms-master/cpu_instrs/individual/03-op sp,hl.gb";
	//rom = "roms/gb-test-roms-master/cpu_instrs/individual/04-op r,imm.gb";
	//rom = "roms/gb-test-roms-master/cpu_instrs/individual/05-op rp.gb";
	//rom = "roms/gb-test-roms-master/cpu_instrs/individual/06-ld r,r.gb";
	//rom = "roms/gb-test-roms-master/cpu_instrs/individual/07-jr,jp,call,ret,rst.gb";
	//rom = "roms/gb-test-roms-master/cpu_instrs/individual/08-misc instrs.gb";
	//rom = "roms/gb-test-roms-master/cpu_instrs/individual/09-op r,r.gb";
	//rom = "roms/gb-test-roms-master/cpu_instrs/individual/10-bit ops.gb";
	//rom = "roms/gb-test-roms-master/cpu_instrs/individual/11-op a,(hl).gb";

	//rom = "roms/Dr. Mario (World).gb";
	rom = "roms/Tetris.gb";

	Cart* cart = CartLoader::loadCart(bus, rom);

	std::ifstream s;
	long length;
	s.open("DMG_ROM.bin", std::ios::binary | std::ios::in);
	s.seekg(0, std::ios::end);
	length = s.tellg();
	s.seekg(0, std::ios::beg);
	char* buffer = new char[length];
	s.read(buffer, length);
	if (length > 0x100) length = 0x100;
	memcpy(&bus.bootRom, buffer, length);
	s.close();

	sf::RenderWindow window;
	window.create(sf::VideoMode(640, 576), "Window", sf::Style::Default);
	window.setFramerateLimit(60);

	sf::Texture texture;
	texture.create(160, 144);
	sf::Sprite sprite;
	
	sprite.setTexture(texture);
	sprite.setScale(4, 4);

	int frame = 0;
	while (window.isOpen()) {
		while (!engine.isReady()) {
			cpu.step();
		}


		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)window.close();
		}

		{
			joypad.up = sf::Keyboard::isKeyPressed(sf::Keyboard::Up);
			joypad.down = sf::Keyboard::isKeyPressed(sf::Keyboard::Down);
			joypad.left = sf::Keyboard::isKeyPressed(sf::Keyboard::Left);
			joypad.right = sf::Keyboard::isKeyPressed(sf::Keyboard::Right);
			joypad.a = sf::Keyboard::isKeyPressed(sf::Keyboard::X);
			joypad.b = sf::Keyboard::isKeyPressed(sf::Keyboard::Z);
			joypad.select = sf::Keyboard::isKeyPressed(sf::Keyboard::A);
			joypad.start = sf::Keyboard::isKeyPressed(sf::Keyboard::S);
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::L)) log = true;
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::K)) log = false;
		}

		PixelImage* image = engine.getImage();
		texture.update((sf::Uint8*)image->pixels);
		
		

		window.clear(sf::Color::Black);
		window.draw(sprite);
		window.display();
		frame++;
		//if (frame % 60 == 0) std::cout << "Second" << std::endl;
	}

}