#pragma once

//#include "SFML/Graphics.hpp"
#include "Types.h"
#include <vector>

struct Pixel
{
	UInt8 r;
	UInt8 g;
	UInt8 b;
	UInt8 a;

	Pixel() {
		r = 0;
		g = 0;
		b = 0;
		a = 0;
	}

	Pixel(UInt8 r, UInt8 g, UInt8 b, UInt8 a) {
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}
};

struct PixelImage {
	unsigned int height;
	unsigned int width;
	Pixel* pixels;

	Pixel getPixel(unsigned int x, unsigned int y) {
		if (x >= width || y >= height) return Pixel();

		return pixels[y * width + x];
	}

	inline void setPixel(unsigned int x, unsigned int y, Pixel pixel) {
		if (x >= width || y >= height) return;
		pixels[y * width + x] = pixel;

	}

	void init(unsigned int width, unsigned int height) {
		free(pixels);
		this->width = width;
		this->height = height;
		pixels = (Pixel*)calloc(height * width, sizeof(Pixel));
	}

	PixelImage() {
		width = 0;
		height = 0;
		pixels = nullptr;
	}

	PixelImage(unsigned int width, unsigned int height) {
		init(width, height);
	}

	~PixelImage() {
		free(pixels);
	}






};
