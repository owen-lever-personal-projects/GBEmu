#pragma once

#include <fstream>

#include "Types.h"
#include "hardware/CART.h"
#include <string>


struct BinaryFile {
	long length;
	char* data;

	BinaryFile(const std::string& filepath) {
		try {
			std::ifstream s;
			s.open(filepath, std::ios::binary | std::ios::in);
			s.seekg(0, std::ios::end);
			length = s.tellg();
			s.seekg(0, std::ios::beg);
			data = new char[length];
			s.read(data, length);
			s.close();
		}
		catch (const std::exception&) {
			data = nullptr;
			length = 0;
		}
	}

	~BinaryFile() {
		delete[] data;
	}

	char& operator[](int index) {
		return data[index];
	}



};

class CartLoader {

	public:

	static Cart* loadCart(BUS& bus, std::string romPath) {

		int index = romPath.find(".gb");
		std::string savePath;

		if (index != -1) {
			savePath = romPath.substr(0, index) + ".sav";
		}
		else {
			savePath = romPath + ".sav";
			romPath += ".gb";
		}


		BinaryFile rom(romPath);
		BinaryFile save(savePath);

		
		
		int cartType = rom[0x0147];
		int romSize = getRomSize(rom[0x0148]);
		int ramSize = getRamSize(rom[0x0149]);

		
		CartRom cartRom = CartRom(
			romPath,
			getRomSize(romSize),
			rom.data
		);

		CartRam cartRam = CartRam{
			savePath,
			getRamSize(ramSize),
			save.data
		};

		switch (cartType)
		{
			case 0x00: return new CartNoMBC(bus, cartRom, cartRam);


			default:	return nullptr;
		}
	}

	static int getRomSize(int value) {
		switch (value)
		{
			case 0x00:	return 2;
			case 0x01:	return 4;
			case 0x02:	return 8;
			case 0x03:	return 16;
			case 0x04:	return 32;
			case 0x05:	return 64;
			case 0x06:	return 128;
			case 0x07:	return 256;
			case 0x08:	return 512;
		
			case 0x52:	return 72;
			case 0x53:	return 80;
			case 0x54:	return 96;

			default: return 2;
		}
	}

	static int getRamSize(int value) {
		switch (value)
		{
		case 0x00:	return 0;
		case 0x01:	return 1;
		case 0x02:	return 1;
		case 0x03:	return 4;
		case 0x04:	return 16;
		case 0x05:	return 64;

		default: return 0;
		}
	}






};